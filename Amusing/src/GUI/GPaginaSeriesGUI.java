package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.MudarTelaController;

import javax.swing.JTextField;

import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTable;
import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GPaginaSeriesGUI extends JFrame {
	private JTextField txtBarraDePesquisa_1;
	private JTable table;
	private JTable table_1;
	private JTable table_2;
	private JTextField textField;
	private JTextField txtBarraDePesquisaPaginaSeries;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GPaginaSeriesGUI frame = new GPaginaSeriesGUI();
			        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
			        frame.setUndecorated(true);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GPaginaSeriesGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1600, 900);
		getContentPane().setLayout(null);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaCadastroDeSerieGUI();
								
			}
		});
		
		final JPanel pnlConfiguracoes = new JPanel();
		pnlConfiguracoes.setBounds(1415, 47, 119, 200);
		getContentPane().add(pnlConfiguracoes);
		pnlConfiguracoes.setLayout(null);
		pnlConfiguracoes.setVisible(false);
		
		JButton btnMinhasListasPaginaConfiguracoes = new JButton("Minhas listas");
		btnMinhasListasPaginaConfiguracoes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaListasGUI();
				
				dispose();
				
			}
		});
		btnMinhasListasPaginaConfiguracoes.setBounds(0, 114, 119, 30);
		pnlConfiguracoes.add(btnMinhasListasPaginaConfiguracoes);
		
		JButton btnConfiguracoesPaginaSeries = new JButton("Configura\u00E7\u00F5es");
		btnConfiguracoesPaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaConfiguracoesGUI();
				
				dispose();
				
			}
		});
		
		btnConfiguracoesPaginaSeries.setBounds(0, 142, 119, 30);
		pnlConfiguracoes.add(btnConfiguracoesPaginaSeries);
		
		JButton btnSairPaginaSeries = new JButton("Sair");	
		btnSairPaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaSairGUI();
								
			}
		});
		btnSairPaginaSeries.setBounds(0, 170, 119, 30);
		pnlConfiguracoes.add(btnSairPaginaSeries);
		
		JButton btnFoto = new JButton("foto");
		btnFoto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				pnlConfiguracoes.setVisible(false);
				
			}
		});
		
		btnFoto.setBounds(0, 0, 119, 115);
		pnlConfiguracoes.add(btnFoto);
		btnFoto.setFont(new Font("Tahoma", Font.ITALIC, 20));
		JButton btnFotoPaginaSeries = new JButton("foto");
		btnFotoPaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				pnlConfiguracoes.setVisible(true);
				
			}
		});
		btnFotoPaginaSeries.setFont(new Font("Tahoma", Font.ITALIC, 20));
		btnFotoPaginaSeries.setBounds(1415, 47, 119, 115);
		getContentPane().add(btnFotoPaginaSeries);
	
		btnCadastrar.setFont(new Font("Tahoma", Font.PLAIN, 25));
		btnCadastrar.setBounds(1314, 222, 217, 41);
		getContentPane().add(btnCadastrar);
		
		JLabel lblMelhorAvaliados = new JLabel("Melhor Avaliadas");
		lblMelhorAvaliados.setForeground(new Color(255, 255, 255));
		lblMelhorAvaliados.setBackground(new Color(255, 255, 255));
		lblMelhorAvaliados.setHorizontalAlignment(SwingConstants.CENTER);
		lblMelhorAvaliados.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblMelhorAvaliados.setBounds(895, 476, 649, 37);
		getContentPane().add(lblMelhorAvaliados);
		
		table_1 = new JTable();
		table_1.setBounds(895, 342, 649, 123);
		table_1.setBackground(new Color(0,0,0,180));
		getContentPane().add(table_1);
		
		table_2 = new JTable();
		table_2.setBounds(892, 524, 652, 326);
		table_2.setBackground(new Color(0,0,0,180));

		getContentPane().add(table_2);
		
		JLabel lblSeries = new JLabel("         S\u00C9RIES");
		lblSeries.setForeground(new Color(255, 255, 255));
		lblSeries.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblSeries.setBounds(30, 224, 813, 37);
		getContentPane().add(lblSeries);
		
		JLabel lblNewLabel = new JLabel("\u00DAltimas Cadastradas");
		lblNewLabel.setForeground(new Color(255, 255, 255));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblNewLabel.setBounds(895, 283, 649, 37);
		getContentPane().add(lblNewLabel);
		
		table = new JTable();
		table.setBounds(30, 342, 813, 508);
		table.setBackground(new Color(0,0,0,180));
		getContentPane().add(table);
		
		txtBarraDePesquisa_1 = new JTextField();
		txtBarraDePesquisa_1.setHorizontalAlignment(SwingConstants.LEFT);
		txtBarraDePesquisa_1.setText("       Barra de pesquisa");
		txtBarraDePesquisa_1.setFont(new Font("Tahoma", Font.ITALIC, 25));
		txtBarraDePesquisa_1.setColumns(10);
		txtBarraDePesquisa_1.setBounds(30, 283, 813, 37);
		getContentPane().add(txtBarraDePesquisa_1);
		
		JPanel pnlTransparencia1PaginaSeries = new JPanel();
		pnlTransparencia1PaginaSeries.setBounds(10, 220, 1564, 633);
		pnlTransparencia1PaginaSeries.setBackground(new Color(0, 0, 0, 150));
		
		getContentPane().add(pnlTransparencia1PaginaSeries);
		pnlTransparencia1PaginaSeries.setLayout(null);
		
		JButton btnIrParaPgina = new JButton("Ir para P\u00E1gina da S\u00E9rie (BOT\u00C3O PARA DEMONSTRA\u00C7\u00C3O)");
		btnIrParaPgina.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaDaSerieGUI();
				
				dispose();
				
			}
		});
		btnIrParaPgina.setBounds(871, 11, 365, 31);
		pnlTransparencia1PaginaSeries.add(btnIrParaPgina);
		
		textField = new JTextField();
		textField.setText("AMUSING");
		textField.setFont(new Font("Tahoma", Font.ITALIC, 85));
		textField.setEditable(false);
		textField.setColumns(10);
		textField.setBounds(30, 40, 396, 152);
		getContentPane().add(textField);
		
		JButton btnInicioPaginaSeries = new JButton("In\u00EDcio");
		btnInicioPaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaInicialGUI();
				
				dispose();
				
			}
		});
		btnInicioPaginaSeries.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnInicioPaginaSeries.setBounds(798, 51, 119, 41);
		getContentPane().add(btnInicioPaginaSeries);
		
		JButton btnVoltarPaginaSeries = new JButton("<-");
		btnVoltarPaginaSeries.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnVoltarPaginaSeries.setBounds(798, 121, 64, 37);
		getContentPane().add(btnVoltarPaginaSeries);
		
		txtBarraDePesquisaPaginaSeries = new JTextField();
		txtBarraDePesquisaPaginaSeries.setText("Barra de pesquisa");
		txtBarraDePesquisaPaginaSeries.setFont(new Font("Tahoma", Font.ITALIC, 25));
		txtBarraDePesquisaPaginaSeries.setColumns(10);
		txtBarraDePesquisaPaginaSeries.setBounds(1049, 121, 329, 37);
		getContentPane().add(txtBarraDePesquisaPaginaSeries);
		
		JButton btnMusicaPaginaSeries = new JButton("M\u00FAsica");
		btnMusicaPaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaMusicasGUI();
				
				dispose();
				
			}
		});
		btnMusicaPaginaSeries.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnMusicaPaginaSeries.setBounds(945, 51, 136, 41);
		getContentPane().add(btnMusicaPaginaSeries);
		
		JButton btnFilmePaginaSeries = new JButton("Filme");
		btnFilmePaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaFilmesGUI();
				
				dispose();
				
			}
		});
		btnFilmePaginaSeries.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnFilmePaginaSeries.setBounds(1108, 51, 119, 41);
		getContentPane().add(btnFilmePaginaSeries);
		
		JButton btnSeriePaginaSeries = new JButton("S\u00E9rie");
		btnSeriePaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		btnSeriePaginaSeries.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnSeriePaginaSeries.setBounds(1259, 51, 119, 41);
		getContentPane().add(btnSeriePaginaSeries);
		
		JButton btnManualPaginaSeries = new JButton("");
		btnManualPaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaManualGUI();
								
			}
		});
		btnManualPaginaSeries.setIcon(new ImageIcon(GPaginaSeriesGUI.class.getResource("/img/q-mark-icon_002070_64.png")));
		btnManualPaginaSeries.setBounds(871, 121, 46, 37);
		getContentPane().add(btnManualPaginaSeries);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(GPaginaSeriesGUI.class.getResource("/img/serie3.jpg")));
		label.setBounds(0, 0, 1600, 900);
		getContentPane().add(label);
		
		
		

	}
}
