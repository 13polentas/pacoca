package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;

import java.awt.Font;

import javax.swing.JButton;

import controller.MudarTelaController;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Color;

public class GPaginaInicialGUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JButton btnVoltarPaginaInicial;
	private JButton btnManualPaginaInicial;
	private JButton btnInicioPaginaInicial;
	private JButton btnMusicaPaginaInicial;
	private JButton btnFilmePaginaInicial;
	private JButton btnSeriePaginaInicial;
	private JTextField txtBarraDePesquisaPaginaInicial;
	private JButton btnFotoPaginaInicial;
	private JLabel lblMelhoresAvaliados;
	private JLabel lblLanamentos;
	private JLabel label;
	private JLabel lblMsicas;
	private JLabel lblFilmes;
	private JLabel lblSeries;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GPaginaInicialGUI frame = new GPaginaInicialGUI();
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					frame.setUndecorated(true);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GPaginaInicialGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1600, 900);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnIrParaPgina = new JButton("Ir para P\u00E1gina de Cadastro (BOT\u00C3O PARA DEMONSTRA\u00C7\u00C3O)");
		btnIrParaPgina.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaDeCadastro();
				
			}
		});
		btnIrParaPgina.setBounds(10, 169, 476, 40);
		contentPane.add(btnIrParaPgina);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(1195, 330, 326, 494);
		panel_1.setBackground(new Color(0,0,0,150));
		contentPane.add(panel_1);
		
		lblMelhoresAvaliados = new JLabel("MELHORES AVALIADOS");
		lblMelhoresAvaliados.setForeground(Color.WHITE);
		lblMelhoresAvaliados.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblMelhoresAvaliados.setHorizontalAlignment(SwingConstants.CENTER);
		lblMelhoresAvaliados.setBounds(438, 270, 273, 46);
		contentPane.add(lblMelhoresAvaliados);
		
		lblLanamentos = new JLabel("LAN\u00C7AMENTOS");
		lblLanamentos.setForeground(Color.WHITE);
		lblLanamentos.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblLanamentos.setBounds(1266, 266, 185, 46);
		contentPane.add(lblLanamentos);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(65, 517, 1010, 118);
		contentPane.add(panel_2);
		panel_2.setBackground(new Color(0,0,0,150));
		panel_2.setLayout(null);
		
		lblFilmes = new JLabel("Filmes");
		lblFilmes.setForeground(Color.WHITE);
		lblFilmes.setFont(new Font("Tahoma", Font.ITALIC, 20));
		lblFilmes.setBounds(20, 42, 91, 31);
		panel_2.add(lblFilmes);
		
		JPanel panel = new JPanel();
		panel.setBounds(65, 350, 1010, 118);
		contentPane.add(panel);
		panel.setBackground(new Color(0,0,0,150));
		panel.setLayout(null);
		
		lblMsicas = new JLabel("M\u00FAsicas");
		lblMsicas.setForeground(Color.WHITE);
		lblMsicas.setFont(new Font("Tahoma", Font.ITALIC, 20));
		lblMsicas.setBounds(22, 40, 91, 31);
		panel.add(lblMsicas);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBounds(65, 678, 1010, 118);
		contentPane.add(panel_3);
		panel_3.setBackground(new Color(0,0,0,150));
		panel_3.setLayout(null);
		
		lblSeries = new JLabel("S\u00E9ries");
		lblSeries.setForeground(Color.WHITE);
		lblSeries.setFont(new Font("Tahoma", Font.ITALIC, 20));
		lblSeries.setBounds(21, 45, 91, 31);
		panel_3.add(lblSeries);
		
		JPanel pnlTransparencia1PaginaSeries = new JPanel();
		pnlTransparencia1PaginaSeries.setBounds(10, 220, 1564, 633);
		pnlTransparencia1PaginaSeries.setBackground(new Color(0, 0, 0, 150));
		
		getContentPane().add(pnlTransparencia1PaginaSeries);
		pnlTransparencia1PaginaSeries.setLayout(null);
		
		textField = new JTextField();
		textField.setText("AMUSING");
		textField.setFont(new Font("Tahoma", Font.ITALIC, 85));
		textField.setEditable(false);
		textField.setColumns(10);
		textField.setBounds(30, 40, 396, 152);
		contentPane.add(textField);
		
		btnVoltarPaginaInicial = new JButton("\u2190");
		btnVoltarPaginaInicial.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnVoltarPaginaInicial.setBounds(798, 121, 64, 37);
		contentPane.add(btnVoltarPaginaInicial);
		
		btnManualPaginaInicial = new JButton("");
		btnManualPaginaInicial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaManualGUI();
								
			}
		});
		btnManualPaginaInicial.setIcon(new ImageIcon(GPaginaInicialGUI.class.getResource("/img/q-mark-icon_002070_64.png")));
		btnManualPaginaInicial.setBounds(871, 121, 46, 37);
		contentPane.add(btnManualPaginaInicial);
		
		btnInicioPaginaInicial = new JButton("In\u00EDcio");
		btnInicioPaginaInicial.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnInicioPaginaInicial.setBounds(798, 51, 119, 41);
		contentPane.add(btnInicioPaginaInicial);
		
		btnMusicaPaginaInicial = new JButton("M\u00FAsica");
		btnMusicaPaginaInicial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaMusicasGUI();
				
				dispose();
				
			}
		});
		btnMusicaPaginaInicial.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnMusicaPaginaInicial.setBounds(945, 51, 136, 41);
		contentPane.add(btnMusicaPaginaInicial);
		
		btnFilmePaginaInicial = new JButton("Filme");
		btnFilmePaginaInicial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaFilmesGUI();
				
				dispose();
				
			}
		});
		btnFilmePaginaInicial.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnFilmePaginaInicial.setBounds(1108, 51, 119, 41);
		contentPane.add(btnFilmePaginaInicial);
		
		btnSeriePaginaInicial = new JButton("S\u00E9rie");
		btnSeriePaginaInicial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaSeriesGUI();
				
				dispose();
				
			
			}
		});
		btnSeriePaginaInicial.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnSeriePaginaInicial.setBounds(1259, 51, 119, 41);
		contentPane.add(btnSeriePaginaInicial);
		
		txtBarraDePesquisaPaginaInicial = new JTextField();
		txtBarraDePesquisaPaginaInicial.setText("Barra de pesquisa");
		txtBarraDePesquisaPaginaInicial.setFont(new Font("Tahoma", Font.ITALIC, 25));
		txtBarraDePesquisaPaginaInicial.setColumns(10);
		txtBarraDePesquisaPaginaInicial.setBounds(1049, 121, 329, 37);
		contentPane.add(txtBarraDePesquisaPaginaInicial);
		
		final JPanel pnlConfiguracoes = new JPanel();
		pnlConfiguracoes.setBounds(1415, 47, 119, 200);
		getContentPane().add(pnlConfiguracoes);
		pnlConfiguracoes.setLayout(null);
		pnlConfiguracoes.setVisible(false);
		
		JButton btnMinhasListasPaginaConfiguracoes = new JButton("Minhas listas");
		btnMinhasListasPaginaConfiguracoes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaListasGUI();
				
				dispose();
				
			}
		});
		btnMinhasListasPaginaConfiguracoes.setBounds(0, 114, 119, 30);
		pnlConfiguracoes.add(btnMinhasListasPaginaConfiguracoes);
		
		JButton btnConfiguracoesPaginaSeries = new JButton("Configura\u00E7\u00F5es");
		btnConfiguracoesPaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaConfiguracoesGUI();
				
				dispose();
				
			}
		});
		btnConfiguracoesPaginaSeries.setBounds(0, 142, 119, 30);
		pnlConfiguracoes.add(btnConfiguracoesPaginaSeries);
		
		JButton btnSairPaginaSeries = new JButton("Sair");	
		btnSairPaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaSairGUI();
								
			}
		});
		btnSairPaginaSeries.setBounds(0, 170, 119, 30);
		pnlConfiguracoes.add(btnSairPaginaSeries);
		
		JButton btnFoto = new JButton("foto");
		btnFoto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				pnlConfiguracoes.setVisible(false);
				
			}
		});
		
		btnFoto.setBounds(0, 0, 119, 115);
		pnlConfiguracoes.add(btnFoto);
		btnFoto.setFont(new Font("Tahoma", Font.ITALIC, 20));
		
		JButton btnFotoPaginaSeries = new JButton("foto");
		btnFotoPaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				pnlConfiguracoes.setVisible(true);
				
			}
		});
		btnFotoPaginaSeries.setFont(new Font("Tahoma", Font.ITALIC, 20));
		btnFotoPaginaSeries.setBounds(1415, 47, 119, 115);
		getContentPane().add(btnFotoPaginaSeries);
		
		
		label = new JLabel("");
		label.setIcon(new ImageIcon(GPaginaInicialGUI.class.getResource("/img/musica4.jpg")));
		label.setBounds(0, 0, 1600, 900);
		contentPane.add(label);
	}
}
