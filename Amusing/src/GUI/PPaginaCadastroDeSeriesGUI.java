package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Color;

public class PPaginaCadastroDeSeriesGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtNomeDaFilmeCadastroDeSeriesGUI;
	private JLabel lblDuracaoCadastroDeSeriesGUI;
	private JLabel lblLancamentoCadastroDeSeriesGUI;
	private JTextField txtLancamentoCadastroDeSeriesGUI;
	private JTextField txtDuracaoCadastroDeSeriesUI;
	private JTextField txtDiretorCadastroDeSeriesGUI;
	private JTextField txtRoteiristaCadastroDeSeriesGUI;
	private JTextField txtGeneroCadastroDeSeriesGUI;
	private JTextField txtEstudioCadastroDeSeriesGUI;
	private JTextField txtAtoresCadastroDeSeriesGUI;
	private JButton btnAddTemporadaCadastroDeSeriesGUI;
	private JLabel label;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PPaginaCadastroDeFilmeGUI frame = new PPaginaCadastroDeFilmeGUI();
					frame.setUndecorated(true);
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PPaginaCadastroDeSeriesGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 778, 620);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(0, 0, 0, 150));
		panel_1.setBounds(401, 229, 371, 341);
		contentPane.add(panel_1);
		
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 229, 371, 341);
		panel.setBackground(new Color(0,0,0,150));
		contentPane.add(panel);
		
		JLabel lblNomeDaSerieCadastroDeSeriesGUI = new JLabel("Nome do S\u00E9rie:");
		lblNomeDaSerieCadastroDeSeriesGUI.setForeground(Color.WHITE);
		lblNomeDaSerieCadastroDeSeriesGUI.setHorizontalAlignment(SwingConstants.LEFT);
		lblNomeDaSerieCadastroDeSeriesGUI.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNomeDaSerieCadastroDeSeriesGUI.setBounds(150, 11, 141, 35);
		contentPane.add(lblNomeDaSerieCadastroDeSeriesGUI);
		
		txtNomeDaFilmeCadastroDeSeriesGUI = new JTextField();
		txtNomeDaFilmeCadastroDeSeriesGUI.setBounds(301, 17, 328, 25);
		contentPane.add(txtNomeDaFilmeCadastroDeSeriesGUI);
		txtNomeDaFilmeCadastroDeSeriesGUI.setColumns(10);
		
		JLabel lblDiretorCadastroDeSeriesGUI = new JLabel("Diretor:");
		lblDiretorCadastroDeSeriesGUI.setForeground(Color.WHITE);
		lblDiretorCadastroDeSeriesGUI.setHorizontalAlignment(SwingConstants.CENTER);
		lblDiretorCadastroDeSeriesGUI.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblDiretorCadastroDeSeriesGUI.setBounds(10, 53, 134, 25);
		contentPane.add(lblDiretorCadastroDeSeriesGUI);
		
		lblDuracaoCadastroDeSeriesGUI = new JLabel("Dura\u00E7\u00E3o:");
		lblDuracaoCadastroDeSeriesGUI.setForeground(Color.WHITE);
		lblDuracaoCadastroDeSeriesGUI.setHorizontalAlignment(SwingConstants.CENTER);
		lblDuracaoCadastroDeSeriesGUI.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblDuracaoCadastroDeSeriesGUI.setBounds(10, 94, 134, 25);
		contentPane.add(lblDuracaoCadastroDeSeriesGUI);
		
		lblLancamentoCadastroDeSeriesGUI = new JLabel("Lan\u00E7amento:");
		lblLancamentoCadastroDeSeriesGUI.setForeground(Color.WHITE);
		lblLancamentoCadastroDeSeriesGUI.setHorizontalAlignment(SwingConstants.CENTER);
		lblLancamentoCadastroDeSeriesGUI.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblLancamentoCadastroDeSeriesGUI.setBounds(10, 135, 134, 25);
		contentPane.add(lblLancamentoCadastroDeSeriesGUI);
		
		txtLancamentoCadastroDeSeriesGUI = new JTextField();
		txtLancamentoCadastroDeSeriesGUI.setColumns(10);
		txtLancamentoCadastroDeSeriesGUI.setBounds(150, 135, 231, 25);
		contentPane.add(txtLancamentoCadastroDeSeriesGUI);
		
		txtDuracaoCadastroDeSeriesUI = new JTextField();
		txtDuracaoCadastroDeSeriesUI.setColumns(10);
		txtDuracaoCadastroDeSeriesUI.setBounds(150, 94, 231, 25);
		contentPane.add(txtDuracaoCadastroDeSeriesUI);
		
		txtDiretorCadastroDeSeriesGUI = new JTextField();
		txtDiretorCadastroDeSeriesGUI.setColumns(10);
		txtDiretorCadastroDeSeriesGUI.setBounds(150, 53, 231, 25);
		contentPane.add(txtDiretorCadastroDeSeriesGUI);
		
		JLabel lblRoteiristaCadastroDeSeriesGUI = new JLabel("Roteirista:");
		lblRoteiristaCadastroDeSeriesGUI.setForeground(Color.WHITE);
		lblRoteiristaCadastroDeSeriesGUI.setHorizontalAlignment(SwingConstants.CENTER);
		lblRoteiristaCadastroDeSeriesGUI.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblRoteiristaCadastroDeSeriesGUI.setBounds(391, 53, 134, 25);
		contentPane.add(lblRoteiristaCadastroDeSeriesGUI);
		
		txtRoteiristaCadastroDeSeriesGUI = new JTextField();
		txtRoteiristaCadastroDeSeriesGUI.setColumns(10);
		txtRoteiristaCadastroDeSeriesGUI.setBounds(531, 53, 231, 25);
		contentPane.add(txtRoteiristaCadastroDeSeriesGUI);
		
		JLabel lblGeneroCadastroDeSeriesGUI = new JLabel("G\u00EAnero:");
		lblGeneroCadastroDeSeriesGUI.setForeground(Color.WHITE);
		lblGeneroCadastroDeSeriesGUI.setHorizontalAlignment(SwingConstants.CENTER);
		lblGeneroCadastroDeSeriesGUI.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblGeneroCadastroDeSeriesGUI.setBounds(391, 94, 134, 25);
		contentPane.add(lblGeneroCadastroDeSeriesGUI);
		
		txtGeneroCadastroDeSeriesGUI = new JTextField();
		txtGeneroCadastroDeSeriesGUI.setColumns(10);
		txtGeneroCadastroDeSeriesGUI.setBounds(531, 94, 231, 25);
		contentPane.add(txtGeneroCadastroDeSeriesGUI);
		
		JLabel lblEstdioCadastroDeSeriesGUI = new JLabel("Est\u00FAdio:");
		lblEstdioCadastroDeSeriesGUI.setForeground(Color.WHITE);
		lblEstdioCadastroDeSeriesGUI.setHorizontalAlignment(SwingConstants.CENTER);
		lblEstdioCadastroDeSeriesGUI.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblEstdioCadastroDeSeriesGUI.setBounds(391, 135, 134, 25);
		contentPane.add(lblEstdioCadastroDeSeriesGUI);
		
		txtEstudioCadastroDeSeriesGUI = new JTextField();
		txtEstudioCadastroDeSeriesGUI.setColumns(10);
		txtEstudioCadastroDeSeriesGUI.setBounds(531, 135, 231, 25);
		contentPane.add(txtEstudioCadastroDeSeriesGUI);
		
		JLabel lblAtoresCadastroDeSeriesGUI = new JLabel("Atores:");
		lblAtoresCadastroDeSeriesGUI.setForeground(Color.WHITE);
		lblAtoresCadastroDeSeriesGUI.setHorizontalAlignment(SwingConstants.CENTER);
		lblAtoresCadastroDeSeriesGUI.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblAtoresCadastroDeSeriesGUI.setBounds(10, 192, 134, 25);
		contentPane.add(lblAtoresCadastroDeSeriesGUI);
		
		txtAtoresCadastroDeSeriesGUI = new JTextField();
		txtAtoresCadastroDeSeriesGUI.setColumns(10);
		txtAtoresCadastroDeSeriesGUI.setBounds(150, 192, 231, 25);
		contentPane.add(txtAtoresCadastroDeSeriesGUI);
		
		JButton btnCancelarCadastroDeSeriesGUI = new JButton("CANCELAR");
		btnCancelarCadastroDeSeriesGUI.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				dispose();
				
			}
		});
		btnCancelarCadastroDeSeriesGUI.setBounds(267, 580, 114, 35);
		contentPane.add(btnCancelarCadastroDeSeriesGUI);
		
		JButton btnSalvarCadastroDeSeriesGUI = new JButton("SALVAR");
		btnSalvarCadastroDeSeriesGUI.setBounds(400, 580, 114, 35);
		contentPane.add(btnSalvarCadastroDeSeriesGUI);
		
		btnAddTemporadaCadastroDeSeriesGUI = new JButton("Add Temporada");
		btnAddTemporadaCadastroDeSeriesGUI.setBounds(621, 184, 141, 35);
		contentPane.add(btnAddTemporadaCadastroDeSeriesGUI);
		
		label = new JLabel("");
		label.setIcon(new ImageIcon(PPaginaCadastroDeSeriesGUI.class.getResource("/img/gray.jpg")));
		label.setBounds(0, 0, 800, 1000);
		contentPane.add(label);
	}
}
