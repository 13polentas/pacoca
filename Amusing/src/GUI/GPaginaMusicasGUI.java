package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.MudarTelaController;

import javax.swing.JTextField;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTable;
import javax.swing.ImageIcon;

public class GPaginaMusicasGUI extends JFrame {
	private JTextField txtBarraDePesquisa_1;
	private JTable table;
	private JTable table_1;
	private JTable table_2;
	private JTextField textField;
	private JTextField txtBarraDePesquisaPaginaMusicas;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GPaginaMusicasGUI frame = new GPaginaMusicasGUI();
					frame.setUndecorated(true);
			        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GPaginaMusicasGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1600, 900);
		getContentPane().setLayout(null);
		
		JLabel lblMusicas = new JLabel("         MUSICAS");
		lblMusicas.setForeground(Color.WHITE);
		lblMusicas.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblMusicas.setBounds(30, 224, 813, 37);
		getContentPane().add(lblMusicas);
		
		txtBarraDePesquisa_1 = new JTextField();
		txtBarraDePesquisa_1.setHorizontalAlignment(SwingConstants.LEFT);
		txtBarraDePesquisa_1.setText("       Barra de pesquisa");
		txtBarraDePesquisa_1.setFont(new Font("Tahoma", Font.ITALIC, 25));
		txtBarraDePesquisa_1.setColumns(10);
		txtBarraDePesquisa_1.setBounds(30, 283, 813, 37);
		getContentPane().add(txtBarraDePesquisa_1);
		
		JLabel lblNewLabel = new JLabel("\u00DAltimas Cadastradas");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblNewLabel.setBounds(895, 283, 649, 37);
		getContentPane().add(lblNewLabel);
		
		JLabel lblMelhorAvaliados = new JLabel("Melhor Avaliados");
		lblMelhorAvaliados.setForeground(Color.WHITE);
		lblMelhorAvaliados.setHorizontalAlignment(SwingConstants.CENTER);
		lblMelhorAvaliados.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblMelhorAvaliados.setBounds(895, 476, 649, 37);
		getContentPane().add(lblMelhorAvaliados);
		
		
		JButton button = new JButton("Cadastrar");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaCadastroDeMusicaGUI();
								
			}
		});
		button.setFont(new Font("Tahoma", Font.PLAIN, 25));
		button.setBounds(1352, 224, 192, 37);
		getContentPane().add(button);
		
		textField = new JTextField();
		textField.setText("AMUSING");
		textField.setFont(new Font("Tahoma", Font.ITALIC, 85));
		textField.setEditable(false);
		textField.setColumns(10);
		textField.setBounds(30, 40, 396, 152);
		getContentPane().add(textField);
		
		JButton btnInicioPaginaMusicas = new JButton("In\u00EDcio");
		btnInicioPaginaMusicas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaInicialGUI();
				
				dispose();
				
			}
		});
		btnInicioPaginaMusicas.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnInicioPaginaMusicas.setBounds(798, 51, 119, 41);
		getContentPane().add(btnInicioPaginaMusicas);
		
		JButton btnVoltarPaginaMusicas = new JButton("\u2190");
		btnVoltarPaginaMusicas.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnVoltarPaginaMusicas.setBounds(798, 121, 64, 37);
		getContentPane().add(btnVoltarPaginaMusicas);
		
		JButton btnManualPaginaMusicas = new JButton("");
		btnManualPaginaMusicas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaManualGUI();
								
			}
		});
		btnManualPaginaMusicas.setIcon(new ImageIcon(GPaginaMusicasGUI.class.getResource("/img/q-mark-icon_002070_64.png")));
		btnManualPaginaMusicas.setBounds(871, 121, 46, 37);
		getContentPane().add(btnManualPaginaMusicas);
		
		txtBarraDePesquisaPaginaMusicas = new JTextField();
		txtBarraDePesquisaPaginaMusicas.setText("Barra de pesquisa");
		txtBarraDePesquisaPaginaMusicas.setFont(new Font("Tahoma", Font.ITALIC, 25));
		txtBarraDePesquisaPaginaMusicas.setColumns(10);
		txtBarraDePesquisaPaginaMusicas.setBounds(1049, 121, 329, 37);
		getContentPane().add(txtBarraDePesquisaPaginaMusicas);
		
		JButton btnMusicaPaginaMusicas = new JButton("M\u00FAsica");
		btnMusicaPaginaMusicas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		btnMusicaPaginaMusicas.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnMusicaPaginaMusicas.setBounds(945, 51, 136, 41);
		getContentPane().add(btnMusicaPaginaMusicas);
		
		JButton btnFilmePaginaMusicas = new JButton("Filme");
		btnFilmePaginaMusicas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaFilmesGUI();
				
				dispose();
				
			}
		});
		btnFilmePaginaMusicas.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnFilmePaginaMusicas.setBounds(1108, 51, 119, 41);
		getContentPane().add(btnFilmePaginaMusicas);
		
		JButton btnSeriePaginaMusicas = new JButton("S\u00E9rie");
		btnSeriePaginaMusicas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaSeriesGUI();
				
				dispose();
				
			}
		});
		btnSeriePaginaMusicas.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnSeriePaginaMusicas.setBounds(1259, 51, 119, 41);
		getContentPane().add(btnSeriePaginaMusicas);
		
		final JPanel pnlConfiguracoes = new JPanel();
		pnlConfiguracoes.setBounds(1415, 47, 119, 200);
		getContentPane().add(pnlConfiguracoes);
		pnlConfiguracoes.setLayout(null);
		pnlConfiguracoes.setVisible(false);
		
		JButton btnMinhasListasPaginaSeries = new JButton("Minhas listas");
		btnMinhasListasPaginaSeries.setBounds(0, 114, 119, 30);
		pnlConfiguracoes.add(btnMinhasListasPaginaSeries);
		
		JButton btnConfiguracoesPaginaSeries = new JButton("Configura\u00E7\u00F5es");
		btnConfiguracoesPaginaSeries.setBounds(0, 142, 119, 30);
		pnlConfiguracoes.add(btnConfiguracoesPaginaSeries);
		
		JButton btnSairPaginaSeries = new JButton("Sair");
		btnSairPaginaSeries.setBounds(0, 170, 119, 30);
		pnlConfiguracoes.add(btnSairPaginaSeries);
		
		JButton btnFoto = new JButton("foto");
		btnFoto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				pnlConfiguracoes.setVisible(false);
				
			}
		});
		
		btnFoto.setBounds(0, 0, 119, 115);
		pnlConfiguracoes.add(btnFoto);
		btnFoto.setFont(new Font("Tahoma", Font.ITALIC, 20));
		
		JButton btnFotoPaginaSeries = new JButton("foto");
		btnFotoPaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				pnlConfiguracoes.setVisible(true);
				
			}
		});
		btnFotoPaginaSeries.setFont(new Font("Tahoma", Font.ITALIC, 20));
		btnFotoPaginaSeries.setBounds(1415, 47, 119, 115);
		getContentPane().add(btnFotoPaginaSeries);
		
		JPanel pnlTransparencia1PaginaSeries = new JPanel();
		pnlTransparencia1PaginaSeries.setBounds(10, 220, 1564, 633);
		pnlTransparencia1PaginaSeries.setBackground(new Color(0, 0, 0, 150));
		
		getContentPane().add(pnlTransparencia1PaginaSeries);
		pnlTransparencia1PaginaSeries.setLayout(null);
		
		JButton btnIrParaPgina = new JButton("Ir para P\u00E1gina da M\u00FAsica (BOT\u00C3O PARA DEMONSTRA\u00C7\u00C3O)");
		btnIrParaPgina.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaDaMusicaGUI();
				
				dispose();
				
			}
		});
		btnIrParaPgina.setBounds(968, 11, 348, 31);
		pnlTransparencia1PaginaSeries.add(btnIrParaPgina);
		
		table = new JTable();
		table.setBounds(30, 342, 813, 508);
		table.setBackground(new Color(0,0,0,180));
		getContentPane().add(table);
		
		table_1 = new JTable();
		table_1.setBounds(895, 342, 649, 123);
		table_1.setBackground(new Color(0,0,0,180));
		getContentPane().add(table_1);
		
		table_2 = new JTable();
		table_2.setBounds(892, 524, 652, 326);
		table_2.setBackground(new Color(0,0,0,180));

		getContentPane().add(table_2);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(GPaginaMusicasGUI.class.getResource("/img/musica1.jpg")));
		label.setBounds(0, 0, 1600, 900);
		getContentPane().add(label);
	}
}
