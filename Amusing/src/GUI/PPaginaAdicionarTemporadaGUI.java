package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Color;

public class PPaginaAdicionarTemporadaGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtTituloAdicionarTemporada;
	private JTextField txtNumeroAdicionarTemporada;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PPaginaAdicionarListaGUI frame = new PPaginaAdicionarListaGUI();
					frame.setUndecorated(true);
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PPaginaAdicionarTemporadaGUI() {
		setTitle("Adicionar temporada");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 436, 537);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblTituloAdicionarTemporada = new JLabel("T\u00EDtulo:");
		lblTituloAdicionarTemporada.setForeground(Color.WHITE);
		lblTituloAdicionarTemporada.setHorizontalAlignment(SwingConstants.CENTER);
		lblTituloAdicionarTemporada.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblTituloAdicionarTemporada.setBounds(24, 28, 108, 39);
		contentPane.add(lblTituloAdicionarTemporada);
		
		txtTituloAdicionarTemporada = new JTextField();
		txtTituloAdicionarTemporada.setBounds(120, 35, 292, 33);
		contentPane.add(txtTituloAdicionarTemporada);
		txtTituloAdicionarTemporada.setColumns(10);
		
		JLabel lblNumeroAdicionarTemporada = new JLabel("N\u00FAmero:");
		lblNumeroAdicionarTemporada.setForeground(Color.WHITE);
		lblNumeroAdicionarTemporada.setHorizontalAlignment(SwingConstants.CENTER);
		lblNumeroAdicionarTemporada.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNumeroAdicionarTemporada.setBounds(24, 90, 108, 39);
		contentPane.add(lblNumeroAdicionarTemporada);
		
		txtNumeroAdicionarTemporada = new JTextField();
		txtNumeroAdicionarTemporada.setBounds(120, 97, 292, 33);
		contentPane.add(txtNumeroAdicionarTemporada);
		txtNumeroAdicionarTemporada.setColumns(10);
		
		table = new JTable();
		table.setBounds(24, 198, 388, 271);
		table.setBackground(new Color(0,0,0,150));
		contentPane.add(table);
		
		JButton btnSalvarAdicionarTemporada = new JButton("Salvar");
		btnSalvarAdicionarTemporada.setBounds(314, 491, 98, 34);
		contentPane.add(btnSalvarAdicionarTemporada);
		
		JButton btnCancelarAdicionarTemporada = new JButton("Cancelar");
		btnCancelarAdicionarTemporada.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				dispose();
			}
		});
		btnCancelarAdicionarTemporada.setBounds(191, 491, 98, 34);
		contentPane.add(btnCancelarAdicionarTemporada);
		
		JLabel lblEpisdiosAdicionarTemporada = new JLabel("Epis\u00F3dios:");
		lblEpisdiosAdicionarTemporada.setForeground(Color.WHITE);
		lblEpisdiosAdicionarTemporada.setHorizontalAlignment(SwingConstants.CENTER);
		lblEpisdiosAdicionarTemporada.setFont(new Font("Tahoma", Font.ITALIC, 20));
		lblEpisdiosAdicionarTemporada.setBounds(24, 154, 108, 33);
		contentPane.add(lblEpisdiosAdicionarTemporada);
		
		JButton btnAddEpisodioAdicionarTemporada = new JButton("Add epis\u00F3dio");
		btnAddEpisodioAdicionarTemporada.setBounds(286, 154, 126, 32);
		contentPane.add(btnAddEpisodioAdicionarTemporada);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(PPaginaAdicionarTemporadaGUI.class.getResource("/img/gray.jpg")));
		label.setBounds(0, 0, 792, 558);
		contentPane.add(label);
	}
}
