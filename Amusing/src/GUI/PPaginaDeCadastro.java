package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class PPaginaDeCadastro extends JFrame {

	private JPanel contentPane;
	private JTextField txtConfirmarSenhaPaginaDeCadastro;
	private JTextField txtSenhaPaginaDeCadastro;
	private JTextField txtEmailPaginaDeCadastro;
	private JTextField txtNomePaginaDeCadastro;
	private JTextField txtNomeDeUsuarioPaginaDeCadastro;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PPaginaDeCadastro frame = new PPaginaDeCadastro();
					frame.setUndecorated(true);
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PPaginaDeCadastro() {
		setTitle("P\u00E1gina de Cadastro");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 763, 335);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNomeDeUsuarioPaginaDeCadastro = new JLabel("Nome de usu\u00E1rio:");
		lblNomeDeUsuarioPaginaDeCadastro.setForeground(Color.WHITE);
		lblNomeDeUsuarioPaginaDeCadastro.setHorizontalAlignment(SwingConstants.CENTER);
		lblNomeDeUsuarioPaginaDeCadastro.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNomeDeUsuarioPaginaDeCadastro.setBounds(281, 53, 140, 14);
		contentPane.add(lblNomeDeUsuarioPaginaDeCadastro);
		
		JLabel lblNomePaginaDeCadastro = new JLabel("Nome:");
		lblNomePaginaDeCadastro.setForeground(Color.WHITE);
		lblNomePaginaDeCadastro.setHorizontalAlignment(SwingConstants.CENTER);
		lblNomePaginaDeCadastro.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNomePaginaDeCadastro.setBounds(281, 101, 140, 14);
		contentPane.add(lblNomePaginaDeCadastro);
		
		JLabel lblSenhaPaginaDeCadastro = new JLabel("Senha:");
		lblSenhaPaginaDeCadastro.setForeground(Color.WHITE);
		lblSenhaPaginaDeCadastro.setHorizontalAlignment(SwingConstants.CENTER);
		lblSenhaPaginaDeCadastro.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblSenhaPaginaDeCadastro.setBounds(281, 197, 140, 14);
		contentPane.add(lblSenhaPaginaDeCadastro);
		
		JLabel lblEmailPaginaDeCadastro = new JLabel("Email:");
		lblEmailPaginaDeCadastro.setForeground(Color.WHITE);
		lblEmailPaginaDeCadastro.setHorizontalAlignment(SwingConstants.CENTER);
		lblEmailPaginaDeCadastro.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblEmailPaginaDeCadastro.setBounds(281, 149, 140, 14);
		contentPane.add(lblEmailPaginaDeCadastro);
		
		JLabel lblConfirmarSenhaPaginaDeCadastro = new JLabel("Confirmar senha:");
		lblConfirmarSenhaPaginaDeCadastro.setForeground(Color.WHITE);
		lblConfirmarSenhaPaginaDeCadastro.setHorizontalAlignment(SwingConstants.CENTER);
		lblConfirmarSenhaPaginaDeCadastro.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblConfirmarSenhaPaginaDeCadastro.setBounds(281, 245, 140, 14);
		contentPane.add(lblConfirmarSenhaPaginaDeCadastro);
		
		txtConfirmarSenhaPaginaDeCadastro = new JTextField();
		txtConfirmarSenhaPaginaDeCadastro.setBounds(431, 245, 306, 20);
		contentPane.add(txtConfirmarSenhaPaginaDeCadastro);
		txtConfirmarSenhaPaginaDeCadastro.setColumns(10);
		
		txtSenhaPaginaDeCadastro = new JTextField();
		txtSenhaPaginaDeCadastro.setColumns(10);
		txtSenhaPaginaDeCadastro.setBounds(431, 197, 306, 20);
		contentPane.add(txtSenhaPaginaDeCadastro);
		
		txtEmailPaginaDeCadastro = new JTextField();
		txtEmailPaginaDeCadastro.setColumns(10);
		txtEmailPaginaDeCadastro.setBounds(431, 148, 306, 20);
		contentPane.add(txtEmailPaginaDeCadastro);
		
		txtNomePaginaDeCadastro = new JTextField();
		txtNomePaginaDeCadastro.setColumns(10);
		txtNomePaginaDeCadastro.setBounds(431, 101, 306, 20);
		contentPane.add(txtNomePaginaDeCadastro);
		
		txtNomeDeUsuarioPaginaDeCadastro = new JTextField();
		txtNomeDeUsuarioPaginaDeCadastro.setColumns(10);
		txtNomeDeUsuarioPaginaDeCadastro.setBounds(431, 53, 306, 20);
		contentPane.add(txtNomeDeUsuarioPaginaDeCadastro);
		
		JButton btnConfirmar = new JButton("CANCELAR");
		btnConfirmar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				dispose();
				
			}
		});
		btnConfirmar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnConfirmar.setBounds(589, 283, 148, 39);
		contentPane.add(btnConfirmar);
		
		JButton button = new JButton("CONFIRMAR");
		button.setFont(new Font("Tahoma", Font.PLAIN, 15));
		button.setBounds(431, 283, 148, 39);
		contentPane.add(button);
		
		JPanel panel = new JPanel();
		panel.setBounds(31, 38, 240, 240);
		panel.setBackground(new Color(0,0,0,150));
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblFoto = new JLabel("FOTO");
		lblFoto.setForeground(Color.LIGHT_GRAY);
		lblFoto.setHorizontalAlignment(SwingConstants.CENTER);
		lblFoto.setFont(new Font("Tahoma", Font.ITALIC, 20));
		lblFoto.setBounds(0, 0, 240, 240);
		panel.add(lblFoto);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(PPaginaDeCadastro.class.getResource("/img/gray.jpg")));
		label.setBounds(0, 0, 1071, 461);
		contentPane.add(label);
	}
}
