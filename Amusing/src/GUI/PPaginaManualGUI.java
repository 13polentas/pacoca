package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JComboBox;

import java.awt.TextArea;
import java.awt.List;

import javax.swing.JTable;
import javax.swing.JScrollBar;
import javax.swing.ImageIcon;

import controller.MudarTelaController;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PPaginaManualGUI extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PPaginaManualGUI frame = new PPaginaManualGUI();
					frame.setUndecorated(true);
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PPaginaManualGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 895);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnFechar = new JButton("FECHAR");
		btnFechar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnFechar.setBounds(30, 802, 113, 34);
		contentPane.add(btnFechar);
		
		JLabel lblNewLabel = new JLabel("MANUAL");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblNewLabel.setBounds(348, 25, 136, 55);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Para criar uma conta, deve-se clicar em cima de \"criar conta\".");
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel_1.setBounds(64, 126, 653, 26);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Para salvar sua conta clique em \"salvar\".");
		lblNewLabel_2.setForeground(Color.WHITE);
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel_2.setBounds(64, 163, 653, 26);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Para cancelar clique em \"cancelar\".");
		lblNewLabel_3.setForeground(Color.WHITE);
		lblNewLabel_3.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel_3.setBounds(64, 200, 653, 26);
		contentPane.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Para ir \u00E0 pagina In\u00EDcio clique em \"In\u00EDcio\".");
		lblNewLabel_4.setForeground(Color.WHITE);
		lblNewLabel_4.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel_4.setBounds(64, 237, 653, 26);
		contentPane.add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Para ir \u00E0 pagina M\u00FAsica clique em \"M\u00FAsica\".");
		lblNewLabel_5.setForeground(Color.WHITE);
		lblNewLabel_5.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel_5.setBounds(64, 274, 653, 26);
		contentPane.add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("Para ir \u00E0 pagina Filmes clique em \"Filmes\".");
		lblNewLabel_6.setForeground(Color.WHITE);
		lblNewLabel_6.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel_6.setBounds(64, 311, 653, 26);
		contentPane.add(lblNewLabel_6);
		
		JLabel lblNewLabel_7 = new JLabel("Para ir \u00E0 pagina S\u00E9ries clique em \"S\u00E9ries\".");
		lblNewLabel_7.setForeground(Color.WHITE);
		lblNewLabel_7.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel_7.setBounds(64, 348, 653, 26);
		contentPane.add(lblNewLabel_7);
		
		JLabel lblNewLabel_8 = new JLabel("Para adicionar uma lista, clique em \"adicionar lista\".");
		lblNewLabel_8.setForeground(Color.WHITE);
		lblNewLabel_8.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel_8.setBounds(64, 562, 653, 26);
		contentPane.add(lblNewLabel_8);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(PPaginaManualGUI.class.getResource("/img/barra principal.png")));
		label.setBounds(30, 406, 669, 125);
		contentPane.add(label);
		
		JLabel lblParaEditarUma = new JLabel("Para editar uma s\u00E9rie, clique em \"editar s\u00E9rie\".");
		lblParaEditarUma.setForeground(Color.WHITE);
		lblParaEditarUma.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblParaEditarUma.setBounds(64, 599, 653, 26);
		contentPane.add(lblParaEditarUma);
		
		JLabel lblParaEditarUm = new JLabel("Para editar um episodio, clique em \"editar episodio\".");
		lblParaEditarUm.setForeground(Color.WHITE);
		lblParaEditarUm.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblParaEditarUm.setBounds(64, 636, 653, 26);
		contentPane.add(lblParaEditarUm);
		
		JLabel lblParaVisualizarOutra = new JLabel("Para visualizar outra p\u00E1gina do manual, clique em \"continua\u00E7\u00E3o\".");
		lblParaVisualizarOutra.setForeground(Color.WHITE);
		lblParaVisualizarOutra.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblParaVisualizarOutra.setBounds(64, 747, 653, 26);
		contentPane.add(lblParaVisualizarOutra);
		
		JButton btnNewButton = new JButton("Continu\u00E7\u00E3o");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaContinuacaoManualGUI();
				
				dispose();
			}
		});
		btnNewButton.setBounds(603, 801, 126, 36);
		contentPane.add(btnNewButton);
		
		JLabel lblParaAdicionarUma = new JLabel("Para adicionar uma m\u00FAsica, clique em \"adicionar m\u00FAsica\".");
		lblParaAdicionarUma.setForeground(Color.WHITE);
		lblParaAdicionarUma.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblParaAdicionarUma.setBounds(64, 673, 653, 26);
		contentPane.add(lblParaAdicionarUma);
		
		JLabel lblParaAdicionarUma_1 = new JLabel("Para adicionar uma episodio, clique em \"adicionar episodio\".");
		lblParaAdicionarUma_1.setForeground(Color.WHITE);
		lblParaAdicionarUma_1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblParaAdicionarUma_1.setBounds(64, 710, 653, 26);
		contentPane.add(lblParaAdicionarUma_1);
		
		JLabel label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon(PPaginaManualGUI.class.getResource("/img/gray.jpg")));
		label_1.setBounds(0, 0, 800, 1000);
		contentPane.add(label_1);
	}
}
