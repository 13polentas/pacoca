package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.MudarTelaController;

import javax.swing.JTextField;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

public class GPaginaDaSerieGUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField;
	private JTextField txtBarraDePesquisaPaginaDaSerie;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GPaginaDaSerieGUI frame = new GPaginaDaSerieGUI();
					frame.setUndecorated(true);
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GPaginaDaSerieGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1600, 900);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnManualPaginaDaSerie = new JButton("");
		btnManualPaginaDaSerie.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaManualGUI();
								
			}
		});
		btnManualPaginaDaSerie.setIcon(new ImageIcon(GPaginaDaSerieGUI.class.getResource("/img/q-mark-icon_002070_64.png")));
		btnManualPaginaDaSerie.setBounds(871, 121, 46, 37);
		contentPane.add(btnManualPaginaDaSerie);
		
		JButton btnEditarSerie = new JButton("Editar S\u00E9rie");
		btnEditarSerie.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaCadastroDeSerieGUI();
								
			}
		});
		btnEditarSerie.setFont(new Font("Tahoma", Font.PLAIN, 25));
		btnEditarSerie.setBounds(1314, 222, 217, 41);
		contentPane.add(btnEditarSerie);
		
		JPanel panel_2 = new JPanel();
		panel_2.setLayout(null);
		panel_2.setBounds(1223, 315, 318, 514);
		panel_2.setBackground(new Color(0,0,0,150));
		contentPane.add(panel_2);
		
		JLabel lblAtores = new JLabel("Atores");
		lblAtores.setForeground(Color.WHITE);
		lblAtores.setBounds(1345, 254, 105, 66);
		contentPane.add(lblAtores);
		lblAtores.setFont(new Font("Tahoma", Font.PLAIN, 20));
		
		JLabel lblTrilhaSonora = new JLabel("Temporadas");
		lblTrilhaSonora.setForeground(Color.WHITE);
		lblTrilhaSonora.setBounds(774, 254, 196, 66);
		contentPane.add(lblTrilhaSonora);
		lblTrilhaSonora.setFont(new Font("Tahoma", Font.PLAIN, 20));
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBounds(76, 315, 291, 267);
		panel.setBackground(new Color(0, 0, 0, 150));
		contentPane.add(panel);
		
		JLabel lblCapa = new JLabel("Sem capa");
		lblCapa.setForeground(Color.GRAY);
		lblCapa.setHorizontalAlignment(SwingConstants.CENTER);
		lblCapa.setFont(new Font("Tahoma", Font.ITALIC, 20));
		lblCapa.setBounds(10, 76, 271, 79);
		panel.add(lblCapa);
		
		JLabel label_3 = new JLabel("G\u00EAnero:");
		label_3.setForeground(Color.WHITE);
		label_3.setFont(new Font("Tahoma", Font.PLAIN, 18));
		label_3.setBounds(30, 691, 135, 37);
		contentPane.add(label_3);
		
		JLabel lblLanamento = new JLabel("Lan\u00E7amento:");
		lblLanamento.setForeground(Color.WHITE);
		lblLanamento.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblLanamento.setBounds(30, 728, 135, 37);
		contentPane.add(lblLanamento);
		
		JLabel lblEstdio = new JLabel("Est\u00FAdio:");
		lblEstdio.setForeground(Color.WHITE);
		lblEstdio.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblEstdio.setBounds(30, 764, 135, 37);
		contentPane.add(lblEstdio);
		
		JLabel lblRoteirista = new JLabel("Roteirista:");
		lblRoteirista.setForeground(Color.WHITE);
		lblRoteirista.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblRoteirista.setBounds(30, 655, 135, 37);
		contentPane.add(lblRoteirista);
		
		JLabel lblDiretor = new JLabel("Diretor:");
		lblDiretor.setForeground(Color.WHITE);
		lblDiretor.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblDiretor.setBounds(30, 620, 135, 37);
		contentPane.add(lblDiretor);
		
		textField_7 = new JTextField();
		textField_7.setEditable(false);
		textField_7.setColumns(10);
		textField_7.setBounds(161, 772, 233, 26);
		contentPane.add(textField_7);
		
		textField_4 = new JTextField();
		textField_4.setEditable(false);
		textField_4.setColumns(10);
		textField_4.setBounds(161, 663, 233, 26);
		contentPane.add(textField_4);
		
		textField_5 = new JTextField();
		textField_5.setEditable(false);
		textField_5.setColumns(10);
		textField_5.setBounds(161, 699, 233, 26);
		contentPane.add(textField_5);
		
		textField_6 = new JTextField();
		textField_6.setEditable(false);
		textField_6.setColumns(10);
		textField_6.setBounds(161, 736, 233, 26);
		contentPane.add(textField_6);
		
		textField_3 = new JTextField();
		textField_3.setEditable(false);
		textField_3.setColumns(10);
		textField_3.setBounds(161, 628, 233, 26);
		contentPane.add(textField_3);
		
		JPanel panel_1 = new JPanel();
		panel_1.setLayout(null);
		panel_1.setBounds(501, 315, 672, 514);
		panel_1.setBackground(new Color(0,0,0,150));
		contentPane.add(panel_1);
		
		JButton button_7 = new JButton("Estrelas de Avalia\u00E7\u00E3o");
		button_7.setBounds(165, 462, 364, 41);
		panel_1.add(button_7);
		button_7.setFont(new Font("Tahoma", Font.ITALIC, 15));
		
		JButton btnIrParaEpisdio = new JButton("Ir para Epis\u00F3dio (BOT\u00C3O PARA DEMONSTRA\u00C7\u00C3O)");
		btnIrParaEpisdio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaDoEpisodioGUI();
				
				dispose();
				
			}
		});
		btnIrParaEpisdio.setBounds(20, 158, 364, 41);
		panel_1.add(btnIrParaEpisdio);
		
		JButton btnAdicionarTemporadaboto = new JButton("Adicionar temporada (BOT\u00C3O PARA DEMONSTRA\u00C7\u00C3O)");
		btnAdicionarTemporadaboto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaAdicionarTemporadaGUI();
								
			}
		});
		btnAdicionarTemporadaboto.setBounds(20, 106, 364, 41);
		panel_1.add(btnAdicionarTemporadaboto);
		
		JLabel lblNomeDaSerie = new JLabel("       Nome da S\u00E9rie");
		lblNomeDaSerie.setForeground(Color.WHITE);
		lblNomeDaSerie.setFont(new Font("Tahoma", Font.ITALIC, 25));
		lblNomeDaSerie.setBounds(30, 224, 1246, 37);
		contentPane.add(lblNomeDaSerie);
		
		JPanel pnlTransparencia1PaginaDaSerie = new JPanel();
		pnlTransparencia1PaginaDaSerie.setBounds(10, 220, 1564, 633);
		pnlTransparencia1PaginaDaSerie.setBackground(new Color(0, 0, 0, 150));
		
		getContentPane().add(pnlTransparencia1PaginaDaSerie);
		pnlTransparencia1PaginaDaSerie.setLayout(null);
		
		textField = new JTextField();
		textField.setText("AMUSING");
		textField.setFont(new Font("Tahoma", Font.ITALIC, 85));
		textField.setEditable(false);
		textField.setColumns(10);
		textField.setBounds(30, 40, 396, 152);
		contentPane.add(textField);
		
		JButton btnInicioPaginaDaSerie = new JButton("In\u00EDcio");
		btnInicioPaginaDaSerie.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaInicialGUI();
				
				dispose();
				
			}
		});
		btnInicioPaginaDaSerie.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnInicioPaginaDaSerie.setBounds(798, 51, 119, 41);
		contentPane.add(btnInicioPaginaDaSerie);
		
		JButton btnVoltarPaginaDaSerie = new JButton("\u2190");
		btnVoltarPaginaDaSerie.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnVoltarPaginaDaSerie.setBounds(798, 121, 64, 37);
		contentPane.add(btnVoltarPaginaDaSerie);
		
		txtBarraDePesquisaPaginaDaSerie = new JTextField();
		txtBarraDePesquisaPaginaDaSerie.setText("Barra de pesquisa");
		txtBarraDePesquisaPaginaDaSerie.setFont(new Font("Tahoma", Font.ITALIC, 25));
		txtBarraDePesquisaPaginaDaSerie.setColumns(10);
		txtBarraDePesquisaPaginaDaSerie.setBounds(1049, 121, 329, 37);
		contentPane.add(txtBarraDePesquisaPaginaDaSerie);
		
		final JPanel pnlConfiguracoes = new JPanel();
		pnlConfiguracoes.setBounds(1415, 47, 119, 200);
		getContentPane().add(pnlConfiguracoes);
		pnlConfiguracoes.setLayout(null);
		pnlConfiguracoes.setVisible(false);
		
		JButton btnMinhasListasPaginaConfiguracoes = new JButton("Minhas listas");
		btnMinhasListasPaginaConfiguracoes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaListasGUI();
				
				dispose();
				
			}
		});
		btnMinhasListasPaginaConfiguracoes.setBounds(0, 114, 119, 30);
		pnlConfiguracoes.add(btnMinhasListasPaginaConfiguracoes);
		
		JButton btnConfiguracoesPaginaSeries = new JButton("Configura\u00E7\u00F5es");
		btnConfiguracoesPaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaConfiguracoesGUI();
				
				dispose();
				
			}
		});
		btnConfiguracoesPaginaSeries.setBounds(0, 142, 119, 30);
		pnlConfiguracoes.add(btnConfiguracoesPaginaSeries);
		
		JButton btnSairPaginaSeries = new JButton("Sair");	
		btnSairPaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaSairGUI();
								
			}
		});
		btnSairPaginaSeries.setBounds(0, 170, 119, 30);
		pnlConfiguracoes.add(btnSairPaginaSeries);
		
		JButton btnFoto = new JButton("foto");
		btnFoto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				pnlConfiguracoes.setVisible(false);
				
			}
		});
		
		btnFoto.setBounds(0, 0, 119, 115);
		pnlConfiguracoes.add(btnFoto);
		btnFoto.setFont(new Font("Tahoma", Font.ITALIC, 20));
		
		JButton btnFotoPaginaSeries = new JButton("foto");
		btnFotoPaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				pnlConfiguracoes.setVisible(true);
				
			}
		});
		btnFotoPaginaSeries.setFont(new Font("Tahoma", Font.ITALIC, 20));
		btnFotoPaginaSeries.setBounds(1415, 47, 119, 115);
		getContentPane().add(btnFotoPaginaSeries);
		
		JButton btnMusicaPaginaDaSerie = new JButton("M\u00FAsica");
		btnMusicaPaginaDaSerie.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaInicialGUI();
				
				dispose();
				
			}
		});
		btnMusicaPaginaDaSerie.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnMusicaPaginaDaSerie.setBounds(945, 51, 136, 41);
		contentPane.add(btnMusicaPaginaDaSerie);
		
		JButton btnFilmePaginaDaSerie = new JButton("Filme");
		btnFilmePaginaDaSerie.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaFilmesGUI();
				
				dispose();
				
			}
		});
		btnFilmePaginaDaSerie.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnFilmePaginaDaSerie.setBounds(1108, 51, 119, 41);
		contentPane.add(btnFilmePaginaDaSerie);
		
		JButton btnSeriePaginaDaSerie = new JButton("S\u00E9rie");
		btnSeriePaginaDaSerie.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaSeriesGUI();
				
				dispose();
				
			}
		});
		btnSeriePaginaDaSerie.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnSeriePaginaDaSerie.setBounds(1259, 51, 119, 41);
		contentPane.add(btnSeriePaginaDaSerie);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(GPaginaDaSerieGUI.class.getResource("/img/serie1.jpg")));
		label.setBounds(0, 0, 1600, 900);
		contentPane.add(label);
	}
}
