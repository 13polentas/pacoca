package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Color;

public class PPaginaCadastroDeFilmeGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtNomeDaFilmeCadastroDeFilmeGUI;
	private JLabel lblDuracaoCadastroDeFilmeGUI;
	private JLabel lblLancamentoCadastroDeFilmeGUI;
	private JLabel lblTrilhaSonoraCadastroDeFilmeGUI;
	private JTextField txtTrilhaSonoraDeFilmeGUI;
	private JTextField txtLancamentoCadastroDeFilmeGUI;
	private JTextField txtDuracaoCadastroDeFilmeGUI;
	private JTextField txtDiretorCadastroDeFilmeGUI;
	private JTextField txtRoteiristaCadastroDeFilmeGUI;
	private JTextField txtGeneroCadastroDeFilmeGUI;
	private JTextField txtEstudioCadastroDeFilmeGUI;
	private JTextField txtAtoresCadastroDeFilmeGUI;
	private JLabel label;
	private JPanel panel_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PPaginaCadastroDeFilmeGUI frame = new PPaginaCadastroDeFilmeGUI();
					frame.setUndecorated(true);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PPaginaCadastroDeFilmeGUI() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 778, 620);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		panel_1 = new JPanel();
		panel_1.setBackground(new Color(0, 0, 0, 150));
		panel_1.setBounds(401, 229, 371, 341);
		contentPane.add(panel_1);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 229, 371, 341);
		panel.setBackground(new Color(0,0,0,150));
		contentPane.add(panel);
		
		JLabel lblNomeDeFilmeCadastroDeFilmeGUI = new JLabel("Nome do Filme:");
		lblNomeDeFilmeCadastroDeFilmeGUI.setBounds(150, 11, 141, 35);
		lblNomeDeFilmeCadastroDeFilmeGUI.setForeground(Color.WHITE);
		lblNomeDeFilmeCadastroDeFilmeGUI.setHorizontalAlignment(SwingConstants.LEFT);
		lblNomeDeFilmeCadastroDeFilmeGUI.setFont(new Font("Tahoma", Font.PLAIN, 18));
		contentPane.add(lblNomeDeFilmeCadastroDeFilmeGUI);
		
		txtNomeDaFilmeCadastroDeFilmeGUI = new JTextField();
		txtNomeDaFilmeCadastroDeFilmeGUI.setBounds(301, 17, 328, 25);
		contentPane.add(txtNomeDaFilmeCadastroDeFilmeGUI);
		txtNomeDaFilmeCadastroDeFilmeGUI.setColumns(10);
		
		JLabel lblDiretorCadastroDeFilmeGUI = new JLabel("Diretor:");
		lblDiretorCadastroDeFilmeGUI.setBounds(10, 53, 134, 25);
		lblDiretorCadastroDeFilmeGUI.setForeground(Color.WHITE);
		lblDiretorCadastroDeFilmeGUI.setHorizontalAlignment(SwingConstants.CENTER);
		lblDiretorCadastroDeFilmeGUI.setFont(new Font("Tahoma", Font.PLAIN, 18));
		contentPane.add(lblDiretorCadastroDeFilmeGUI);
		
		lblDuracaoCadastroDeFilmeGUI = new JLabel("Dura\u00E7\u00E3o:");
		lblDuracaoCadastroDeFilmeGUI.setBounds(10, 94, 134, 25);
		lblDuracaoCadastroDeFilmeGUI.setForeground(Color.WHITE);
		lblDuracaoCadastroDeFilmeGUI.setHorizontalAlignment(SwingConstants.CENTER);
		lblDuracaoCadastroDeFilmeGUI.setFont(new Font("Tahoma", Font.PLAIN, 18));
		contentPane.add(lblDuracaoCadastroDeFilmeGUI);
		
		lblLancamentoCadastroDeFilmeGUI = new JLabel("Lan\u00E7amento:");
		lblLancamentoCadastroDeFilmeGUI.setBounds(10, 135, 134, 25);
		lblLancamentoCadastroDeFilmeGUI.setForeground(Color.WHITE);
		lblLancamentoCadastroDeFilmeGUI.setHorizontalAlignment(SwingConstants.CENTER);
		lblLancamentoCadastroDeFilmeGUI.setFont(new Font("Tahoma", Font.PLAIN, 18));
		contentPane.add(lblLancamentoCadastroDeFilmeGUI);
		
		lblTrilhaSonoraCadastroDeFilmeGUI = new JLabel("Trilha Sonora:");
		lblTrilhaSonoraCadastroDeFilmeGUI.setBounds(10, 192, 134, 25);
		lblTrilhaSonoraCadastroDeFilmeGUI.setForeground(Color.WHITE);
		lblTrilhaSonoraCadastroDeFilmeGUI.setHorizontalAlignment(SwingConstants.CENTER);
		lblTrilhaSonoraCadastroDeFilmeGUI.setFont(new Font("Tahoma", Font.PLAIN, 18));
		contentPane.add(lblTrilhaSonoraCadastroDeFilmeGUI);
		
		txtTrilhaSonoraDeFilmeGUI = new JTextField();
		txtTrilhaSonoraDeFilmeGUI.setBounds(150, 192, 231, 25);
		contentPane.add(txtTrilhaSonoraDeFilmeGUI);
		txtTrilhaSonoraDeFilmeGUI.setColumns(10);
		
		txtLancamentoCadastroDeFilmeGUI = new JTextField();
		txtLancamentoCadastroDeFilmeGUI.setBounds(150, 135, 231, 25);
		txtLancamentoCadastroDeFilmeGUI.setColumns(10);
		contentPane.add(txtLancamentoCadastroDeFilmeGUI);
		
		txtDuracaoCadastroDeFilmeGUI = new JTextField();
		txtDuracaoCadastroDeFilmeGUI.setBounds(150, 94, 231, 25);
		txtDuracaoCadastroDeFilmeGUI.setColumns(10);
		contentPane.add(txtDuracaoCadastroDeFilmeGUI);
		
		txtDiretorCadastroDeFilmeGUI = new JTextField();
		txtDiretorCadastroDeFilmeGUI.setBounds(150, 53, 231, 25);
		txtDiretorCadastroDeFilmeGUI.setColumns(10);
		contentPane.add(txtDiretorCadastroDeFilmeGUI);
		
		JLabel lblRoteiristaCadastroDeFilmeGUI = new JLabel("Roteirista:");
		lblRoteiristaCadastroDeFilmeGUI.setBounds(391, 53, 134, 25);
		lblRoteiristaCadastroDeFilmeGUI.setForeground(Color.WHITE);
		lblRoteiristaCadastroDeFilmeGUI.setHorizontalAlignment(SwingConstants.CENTER);
		lblRoteiristaCadastroDeFilmeGUI.setFont(new Font("Tahoma", Font.PLAIN, 18));
		contentPane.add(lblRoteiristaCadastroDeFilmeGUI);
		
		txtRoteiristaCadastroDeFilmeGUI = new JTextField();
		txtRoteiristaCadastroDeFilmeGUI.setBounds(531, 53, 231, 25);
		txtRoteiristaCadastroDeFilmeGUI.setColumns(10);
		contentPane.add(txtRoteiristaCadastroDeFilmeGUI);
		
		JLabel lblGeneroCadastroDeFilmeGUI = new JLabel("G\u00EAnero:");
		lblGeneroCadastroDeFilmeGUI.setBounds(391, 94, 134, 25);
		lblGeneroCadastroDeFilmeGUI.setForeground(Color.WHITE);
		lblGeneroCadastroDeFilmeGUI.setHorizontalAlignment(SwingConstants.CENTER);
		lblGeneroCadastroDeFilmeGUI.setFont(new Font("Tahoma", Font.PLAIN, 18));
		contentPane.add(lblGeneroCadastroDeFilmeGUI);
		
		txtGeneroCadastroDeFilmeGUI = new JTextField();
		txtGeneroCadastroDeFilmeGUI.setBounds(531, 94, 231, 25);
		txtGeneroCadastroDeFilmeGUI.setColumns(10);
		contentPane.add(txtGeneroCadastroDeFilmeGUI);
		
		JLabel lblEstdioCadastroDeFilmeGUI = new JLabel("Est\u00FAdio:");
		lblEstdioCadastroDeFilmeGUI.setBounds(391, 135, 134, 25);
		lblEstdioCadastroDeFilmeGUI.setForeground(Color.WHITE);
		lblEstdioCadastroDeFilmeGUI.setHorizontalAlignment(SwingConstants.CENTER);
		lblEstdioCadastroDeFilmeGUI.setFont(new Font("Tahoma", Font.PLAIN, 18));
		contentPane.add(lblEstdioCadastroDeFilmeGUI);
		
		txtEstudioCadastroDeFilmeGUI = new JTextField();
		txtEstudioCadastroDeFilmeGUI.setBounds(531, 135, 231, 25);
		txtEstudioCadastroDeFilmeGUI.setColumns(10);
		contentPane.add(txtEstudioCadastroDeFilmeGUI);
		
		JLabel lblAtoresCadastroDeFilmeGUI = new JLabel("Atores:");
		lblAtoresCadastroDeFilmeGUI.setBounds(391, 192, 134, 25);
		lblAtoresCadastroDeFilmeGUI.setForeground(Color.WHITE);
		lblAtoresCadastroDeFilmeGUI.setHorizontalAlignment(SwingConstants.CENTER);
		lblAtoresCadastroDeFilmeGUI.setFont(new Font("Tahoma", Font.PLAIN, 18));
		contentPane.add(lblAtoresCadastroDeFilmeGUI);
		
		txtAtoresCadastroDeFilmeGUI = new JTextField();
		txtAtoresCadastroDeFilmeGUI.setBounds(531, 192, 231, 25);
		txtAtoresCadastroDeFilmeGUI.setColumns(10);
		contentPane.add(txtAtoresCadastroDeFilmeGUI);
		
		JButton btnCancelarCadastroDeFilmeGUI = new JButton("CANCELAR");
		btnCancelarCadastroDeFilmeGUI.setBounds(267, 580, 114, 35);
		btnCancelarCadastroDeFilmeGUI.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				dispose();
			}
		});
		contentPane.add(btnCancelarCadastroDeFilmeGUI);
		
		JButton btnSalvarCadastroDeFilmeGUI = new JButton("SALVAR");
		btnSalvarCadastroDeFilmeGUI.setBounds(400, 580, 114, 35);
		contentPane.add(btnSalvarCadastroDeFilmeGUI);
		
		label = new JLabel("");
		label.setBounds(0, 0, 878, 704);
		label.setIcon(new ImageIcon(PPaginaCadastroDeFilmeGUI.class.getResource("/img/gray.jpg")));
		contentPane.add(label);
	}
}
