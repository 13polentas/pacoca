package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class PPaginaEditarEpisodioGUI extends JFrame {

	private JPanel contentPane;
	private JLabel lblNumeroEditarEpisodioGUI;
	private JTextField txtNumeroEditarEpisodioGUI;
	private JTextField txtDiretorEditarEpisodioGUI;
	private JLabel lblTrilhaSonoraEditarEpisodioGUI;
	private JLabel lblAtoresEditarEpisodioGUI;
	private JButton btnAddMusicaEditarEpisodioGUI;
	private JButton btnAddAtorEditarEpisodioGUI;
	private JPanel panel;
	private JLabel label;
	private JPanel panel_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PPaginaEditarEpisodioGUI frame = new PPaginaEditarEpisodioGUI();
					frame.setUndecorated(true);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PPaginaEditarEpisodioGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 627, 569);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		panel_1 = new JPanel();
		panel_1.setBounds(336, 171, 281, 341);
		panel_1.setBackground(new Color (0,0,0,150));
		contentPane.add(panel_1);
		
		JLabel lblDiretorEditarEpisodioGUI = new JLabel("Diretor:");
		lblDiretorEditarEpisodioGUI.setForeground(Color.WHITE);
		lblDiretorEditarEpisodioGUI.setHorizontalAlignment(SwingConstants.CENTER);
		lblDiretorEditarEpisodioGUI.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblDiretorEditarEpisodioGUI.setBounds(10, 22, 93, 25);
		contentPane.add(lblDiretorEditarEpisodioGUI);
		
		lblNumeroEditarEpisodioGUI = new JLabel("N\u00FAmero:");
		lblNumeroEditarEpisodioGUI.setForeground(Color.WHITE);
		lblNumeroEditarEpisodioGUI.setHorizontalAlignment(SwingConstants.CENTER);
		lblNumeroEditarEpisodioGUI.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNumeroEditarEpisodioGUI.setBounds(10, 66, 93, 25);
		contentPane.add(lblNumeroEditarEpisodioGUI);
		
		txtNumeroEditarEpisodioGUI = new JTextField();
		txtNumeroEditarEpisodioGUI.setColumns(10);
		txtNumeroEditarEpisodioGUI.setBounds(100, 69, 517, 25);
		contentPane.add(txtNumeroEditarEpisodioGUI);
		
		txtDiretorEditarEpisodioGUI = new JTextField();
		txtDiretorEditarEpisodioGUI.setColumns(10);
		txtDiretorEditarEpisodioGUI.setBounds(100, 25, 517, 25);
		contentPane.add(txtDiretorEditarEpisodioGUI);
		
		JButton btnCancelarEditarEpisodioGUI = new JButton("CANCELAR");
		btnCancelarEditarEpisodioGUI.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				dispose();
				
			}
		});
		btnCancelarEditarEpisodioGUI.setBounds(193, 523, 114, 35);
		contentPane.add(btnCancelarEditarEpisodioGUI);
		
		JButton btnSalvarEditarEpisodioGUI = new JButton("SALVAR");
		btnSalvarEditarEpisodioGUI.setBounds(326, 523, 114, 35);
		contentPane.add(btnSalvarEditarEpisodioGUI);
		
		lblTrilhaSonoraEditarEpisodioGUI = new JLabel("Trilha Sonora:");
		lblTrilhaSonoraEditarEpisodioGUI.setForeground(Color.WHITE);
		lblTrilhaSonoraEditarEpisodioGUI.setHorizontalAlignment(SwingConstants.CENTER);
		lblTrilhaSonoraEditarEpisodioGUI.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblTrilhaSonoraEditarEpisodioGUI.setBounds(10, 135, 134, 25);
		contentPane.add(lblTrilhaSonoraEditarEpisodioGUI);
		
		lblAtoresEditarEpisodioGUI = new JLabel("Atores:");
		lblAtoresEditarEpisodioGUI.setForeground(Color.WHITE);
		lblAtoresEditarEpisodioGUI.setHorizontalAlignment(SwingConstants.CENTER);
		lblAtoresEditarEpisodioGUI.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblAtoresEditarEpisodioGUI.setBounds(336, 134, 80, 25);
		contentPane.add(lblAtoresEditarEpisodioGUI);
		
		btnAddMusicaEditarEpisodioGUI = new JButton("Adicionar M\u00FAsica");
		btnAddMusicaEditarEpisodioGUI.setBounds(142, 133, 149, 35);
		contentPane.add(btnAddMusicaEditarEpisodioGUI);
		
		btnAddAtorEditarEpisodioGUI = new JButton("Adicionar Ator");
		btnAddAtorEditarEpisodioGUI.setBounds(468, 133, 149, 35);
		contentPane.add(btnAddAtorEditarEpisodioGUI);
		
		panel = new JPanel();
		panel.setBounds(10, 170, 281, 341);
		panel.setBackground(new Color (0,0,0,150));
		contentPane.add(panel);
		
		label = new JLabel("");
		label.setIcon(new ImageIcon(PPaginaEditarEpisodioGUI.class.getResource("/img/gray.jpg")));
		label.setBounds(0, 0, 897, 674);
		label.setBackground(new Color(0,0,0,150));
		contentPane.add(label);
	}
}
