package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.MudarTelaController;

import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class GPaginaDaListaGUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField txtBarraDePesquisaPaginaDaLista;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GPaginaDaListaGUI frame = new GPaginaDaListaGUI();
					frame.setUndecorated(true);
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GPaginaDaListaGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1600, 900);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textField = new JTextField();
		textField.setEditable(false);
		textField.setText("AMUSING");
		textField.setFont(new Font("Tahoma", Font.ITALIC, 85));
		textField.setColumns(10);
		textField.setBounds(30, 40, 396, 152);
		contentPane.add(textField);
		
		JPanel pnlTransparencia1PaginaDaLista = new JPanel();
		pnlTransparencia1PaginaDaLista.setBounds(10, 220, 1564, 633);
		pnlTransparencia1PaginaDaLista.setBackground(new Color(0, 0, 0, 150));
		
		getContentPane().add(pnlTransparencia1PaginaDaLista);
		pnlTransparencia1PaginaDaLista.setLayout(null);
		
				
		JPanel panel = new JPanel();
		panel.setBounds(73, 80, 1454, 508);
		panel.setBackground(new Color(0,0,0,150));
		pnlTransparencia1PaginaDaLista.add(panel);
		
		table = new JTable();
		panel.add(table);
		
		JLabel lblNewLabel = new JLabel("Nome da Lista");
		lblNewLabel.setBounds(73, 24, 239, 37);
		pnlTransparencia1PaginaDaLista.add(lblNewLabel);
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Tahoma", Font.ITALIC, 25));
		
		JButton btnEditarListaPaginaDaLista = new JButton("Editar Lista");
		btnEditarListaPaginaDaLista.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaAdicionarListaGUI();
								
			}
		});
		btnEditarListaPaginaDaLista.setBounds(1304, 20, 217, 41);
		pnlTransparencia1PaginaDaLista.add(btnEditarListaPaginaDaLista);
		btnEditarListaPaginaDaLista.setFont(new Font("Tahoma", Font.PLAIN, 25));
		
		JButton btnInicioPaginaDaLista = new JButton("In\u00EDcio");
		btnInicioPaginaDaLista.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaInicialGUI();
				
				dispose();
				
			}
		});
		btnInicioPaginaDaLista.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnInicioPaginaDaLista.setBounds(798, 51, 119, 41);
		contentPane.add(btnInicioPaginaDaLista);
		
		JButton btnVoltarPaginaDaLista = new JButton("\u2190");
		btnVoltarPaginaDaLista.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnVoltarPaginaDaLista.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnVoltarPaginaDaLista.setBounds(798, 121, 64, 37);
		contentPane.add(btnVoltarPaginaDaLista);
		
		txtBarraDePesquisaPaginaDaLista = new JTextField();
		txtBarraDePesquisaPaginaDaLista.setText("Barra de pesquisa");
		txtBarraDePesquisaPaginaDaLista.setFont(new Font("Tahoma", Font.ITALIC, 25));
		txtBarraDePesquisaPaginaDaLista.setColumns(10);
		txtBarraDePesquisaPaginaDaLista.setBounds(1049, 121, 329, 37);
		contentPane.add(txtBarraDePesquisaPaginaDaLista);
		
		final JPanel pnlConfiguracoes = new JPanel();
		pnlConfiguracoes.setBounds(1415, 47, 119, 200);
		getContentPane().add(pnlConfiguracoes);
		pnlConfiguracoes.setLayout(null);
		pnlConfiguracoes.setVisible(false);
		
		JButton btnMinhasListasPaginaConfiguracoes = new JButton("Minhas listas");
		btnMinhasListasPaginaConfiguracoes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaListasGUI();
				
				dispose();
				
			}
		});
		btnMinhasListasPaginaConfiguracoes.setBounds(0, 114, 119, 30);
		pnlConfiguracoes.add(btnMinhasListasPaginaConfiguracoes);
		
		JButton btnConfiguracoesPaginaSeries = new JButton("Configura\u00E7\u00F5es");
		btnConfiguracoesPaginaSeries.setBounds(0, 142, 119, 30);
		pnlConfiguracoes.add(btnConfiguracoesPaginaSeries);
		
		JButton btnSairPaginaSeries = new JButton("Sair");	
		btnSairPaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaSairGUI();
				
				dispose();
				
			}
		});
		btnSairPaginaSeries.setBounds(0, 170, 119, 30);
		pnlConfiguracoes.add(btnSairPaginaSeries);
		
		JButton btnFoto = new JButton("foto");
		btnFoto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				pnlConfiguracoes.setVisible(false);
				
			}
		});
		
		btnFoto.setBounds(0, 0, 119, 115);
		pnlConfiguracoes.add(btnFoto);
		btnFoto.setFont(new Font("Tahoma", Font.ITALIC, 20));
		
		JButton btnFotoPaginaSeries = new JButton("foto");
		btnFotoPaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				pnlConfiguracoes.setVisible(true);
				
			}
		});
		btnFotoPaginaSeries.setFont(new Font("Tahoma", Font.ITALIC, 20));
		btnFotoPaginaSeries.setBounds(1415, 47, 119, 115);
		getContentPane().add(btnFotoPaginaSeries);
		
		JButton btnMsica = new JButton("M\u00FAsica");
		btnMsica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaMusicasGUI();
				
				dispose();
				
			}
		});
		btnMsica.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnMsica.setBounds(945, 51, 136, 41);
		contentPane.add(btnMsica);
		
		JButton btnFilme = new JButton("Filme");
		btnFilme.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaFilmesGUI();
				
				dispose();
				
			}
		});
		btnFilme.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnFilme.setBounds(1108, 51, 119, 41);
		contentPane.add(btnFilme);
		
		JButton btnSrie = new JButton("S\u00E9rie");
		btnSrie.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaSairGUI();
				
				dispose();
				
			}
		});
		btnSrie.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnSrie.setBounds(1259, 51, 119, 41);
		contentPane.add(btnSrie);
		
		JButton btnManualPaginaDaLista = new JButton("");
		btnManualPaginaDaLista.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaManualGUI();
								
			}
		});
		btnManualPaginaDaLista.setIcon(new ImageIcon(GPaginaDaListaGUI.class.getResource("/img/q-mark-icon_002070_64.png")));
		btnManualPaginaDaLista.setBounds(871, 121, 46, 37);
		contentPane.add(btnManualPaginaDaLista);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(GPaginaDaListaGUI.class.getResource("/img/filme3.png")));
		label.setBounds(0, 0, 1600, 900);
		contentPane.add(label);
	}
}
