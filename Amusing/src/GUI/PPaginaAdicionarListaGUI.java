package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Color;

public class PPaginaAdicionarListaGUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField txtPesquisa;
	private JTable table;
	private JLabel label;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PPaginaAdicionarListaGUI frame = new PPaginaAdicionarListaGUI();
					frame.setUndecorated(true);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PPaginaAdicionarListaGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 435, 540);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNomeDaLista = new JLabel("Nome da Lista:");
		lblNomeDaLista.setForeground(Color.WHITE);
		lblNomeDaLista.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNomeDaLista.setBounds(24, 28, 147, 39);
		contentPane.add(lblNomeDaLista);
		
		textField = new JTextField();
		textField.setBounds(177, 35, 235, 33);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblTipoDeLista = new JLabel("Tipo de Lista:");
		lblTipoDeLista.setForeground(Color.WHITE);
		lblTipoDeLista.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblTipoDeLista.setBounds(24, 90, 130, 39);
		contentPane.add(lblTipoDeLista);
		
		textField_1 = new JTextField();
		textField_1.setBounds(177, 97, 235, 33);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		txtPesquisa = new JTextField();
		txtPesquisa.setFont(new Font("Tahoma", Font.ITALIC, 20));
		txtPesquisa.setText("pesquisa");
		txtPesquisa.setBounds(24, 149, 272, 25);
		contentPane.add(txtPesquisa);
		txtPesquisa.setColumns(10);
		
		table = new JTable();
		table.setBounds(24, 198, 388, 271);
		table.setBackground(new Color(0,0,0,150));
		contentPane.add(table);
		
		JButton btnSalvarPaginaAdicionarLista = new JButton("Salvar");
		btnSalvarPaginaAdicionarLista.setBounds(314, 491, 98, 34);
		contentPane.add(btnSalvarPaginaAdicionarLista);
		
		JButton btnCancelarPaginaAdicionarLista = new JButton("Cancelar");
		btnCancelarPaginaAdicionarLista.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				dispose();
			}
		});
		btnCancelarPaginaAdicionarLista.setBounds(191, 491, 98, 34);
		contentPane.add(btnCancelarPaginaAdicionarLista);
		
		label = new JLabel("");
		label.setIcon(new ImageIcon(PPaginaAdicionarListaGUI.class.getResource("/img/gray.jpg")));
		label.setBounds(0, 0, 637, 590);
		contentPane.add(label);
	}
}
