package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class PPaginaCadastroDeMusicaGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtNomeDaMusicaCadastroDeMusicaGUI;
	private JLabel lblDuracaoCadastroDeMusicaGUI;
	private JLabel lblLancamentoCadastroDeMusicaGUI;
	private JLabel lblLetraCadastroDeMusicaGUI;
	private JTextField txtLetraCadastroDeMusicaGUI;
	private JTextField txtLancamentoCadastroDeMusicaGUI;
	private JTextField txtDuracaoCadastroDeMusicaGUI;
	private JTextField txtCompositorCadastroDeMusicaGUI;
	private JTextField CantorBandaCadastroDeMusicaGUI;
	private JTextField GeneroCadastroDeMusicaGUI;
	private JTextField LinkDeVideoCadastroDeMusicaGUI;
	private JTextField AparicoesCadastroDeMusicaGUI;
	private JLabel label;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PPaginaCadastroDeFilmeGUI frame = new PPaginaCadastroDeFilmeGUI();
					frame.setUndecorated(true);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PPaginaCadastroDeMusicaGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 778, 620);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNomeDaMusicaCadastroDeMusicaGUI = new JLabel("Nome da M\u00FAsica:");
		lblNomeDaMusicaCadastroDeMusicaGUI.setForeground(Color.WHITE);
		lblNomeDaMusicaCadastroDeMusicaGUI.setHorizontalAlignment(SwingConstants.LEFT);
		lblNomeDaMusicaCadastroDeMusicaGUI.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNomeDaMusicaCadastroDeMusicaGUI.setBounds(150, 11, 141, 35);
		contentPane.add(lblNomeDaMusicaCadastroDeMusicaGUI);
		
		txtNomeDaMusicaCadastroDeMusicaGUI = new JTextField();
		txtNomeDaMusicaCadastroDeMusicaGUI.setBounds(301, 17, 328, 25);
		contentPane.add(txtNomeDaMusicaCadastroDeMusicaGUI);
		txtNomeDaMusicaCadastroDeMusicaGUI.setColumns(10);
		
		JLabel lblCompositorCadastroDeMusicaGUI = new JLabel("Compositor");
		lblCompositorCadastroDeMusicaGUI.setForeground(Color.WHITE);
		lblCompositorCadastroDeMusicaGUI.setHorizontalAlignment(SwingConstants.CENTER);
		lblCompositorCadastroDeMusicaGUI.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblCompositorCadastroDeMusicaGUI.setBounds(10, 53, 134, 25);
		contentPane.add(lblCompositorCadastroDeMusicaGUI);
		
		lblDuracaoCadastroDeMusicaGUI = new JLabel("Dura\u00E7\u00E3o:");
		lblDuracaoCadastroDeMusicaGUI.setForeground(Color.WHITE);
		lblDuracaoCadastroDeMusicaGUI.setHorizontalAlignment(SwingConstants.CENTER);
		lblDuracaoCadastroDeMusicaGUI.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblDuracaoCadastroDeMusicaGUI.setBounds(10, 94, 134, 25);
		contentPane.add(lblDuracaoCadastroDeMusicaGUI);
		
		lblLancamentoCadastroDeMusicaGUI = new JLabel("Lan\u00E7amento:");
		lblLancamentoCadastroDeMusicaGUI.setForeground(Color.WHITE);
		lblLancamentoCadastroDeMusicaGUI.setHorizontalAlignment(SwingConstants.CENTER);
		lblLancamentoCadastroDeMusicaGUI.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblLancamentoCadastroDeMusicaGUI.setBounds(10, 135, 134, 25);
		contentPane.add(lblLancamentoCadastroDeMusicaGUI);
		
		lblLetraCadastroDeMusicaGUI = new JLabel("Letra:");
		lblLetraCadastroDeMusicaGUI.setForeground(Color.WHITE);
		lblLetraCadastroDeMusicaGUI.setHorizontalAlignment(SwingConstants.CENTER);
		lblLetraCadastroDeMusicaGUI.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblLetraCadastroDeMusicaGUI.setBounds(10, 192, 134, 25);
		contentPane.add(lblLetraCadastroDeMusicaGUI);
		
		txtLetraCadastroDeMusicaGUI = new JTextField();
		txtLetraCadastroDeMusicaGUI.setBounds(150, 192, 231, 25);
		contentPane.add(txtLetraCadastroDeMusicaGUI);
		txtLetraCadastroDeMusicaGUI.setColumns(10);
		
		txtLancamentoCadastroDeMusicaGUI = new JTextField();
		txtLancamentoCadastroDeMusicaGUI.setColumns(10);
		txtLancamentoCadastroDeMusicaGUI.setBounds(150, 135, 231, 25);
		contentPane.add(txtLancamentoCadastroDeMusicaGUI);
		
		txtDuracaoCadastroDeMusicaGUI = new JTextField();
		txtDuracaoCadastroDeMusicaGUI.setColumns(10);
		txtDuracaoCadastroDeMusicaGUI.setBounds(150, 94, 231, 25);
		contentPane.add(txtDuracaoCadastroDeMusicaGUI);
		
		txtCompositorCadastroDeMusicaGUI = new JTextField();
		txtCompositorCadastroDeMusicaGUI.setColumns(10);
		txtCompositorCadastroDeMusicaGUI.setBounds(150, 53, 231, 25);
		contentPane.add(txtCompositorCadastroDeMusicaGUI);
		
		JLabel lblCantorBandaCadastroDeMusicaGUI = new JLabel("Cantor/banda:");
		lblCantorBandaCadastroDeMusicaGUI.setForeground(Color.WHITE);
		lblCantorBandaCadastroDeMusicaGUI.setHorizontalAlignment(SwingConstants.CENTER);
		lblCantorBandaCadastroDeMusicaGUI.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblCantorBandaCadastroDeMusicaGUI.setBounds(391, 53, 134, 25);
		contentPane.add(lblCantorBandaCadastroDeMusicaGUI);
		
		CantorBandaCadastroDeMusicaGUI = new JTextField();
		CantorBandaCadastroDeMusicaGUI.setColumns(10);
		CantorBandaCadastroDeMusicaGUI.setBounds(531, 53, 231, 25);
		contentPane.add(CantorBandaCadastroDeMusicaGUI);
		
		JLabel lblGeneroCadastroDeMusica = new JLabel("G\u00EAnero:");
		lblGeneroCadastroDeMusica.setForeground(Color.WHITE);
		lblGeneroCadastroDeMusica.setHorizontalAlignment(SwingConstants.CENTER);
		lblGeneroCadastroDeMusica.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblGeneroCadastroDeMusica.setBounds(391, 94, 134, 25);
		contentPane.add(lblGeneroCadastroDeMusica);
		
		GeneroCadastroDeMusicaGUI = new JTextField();
		GeneroCadastroDeMusicaGUI.setColumns(10);
		GeneroCadastroDeMusicaGUI.setBounds(531, 94, 231, 25);
		contentPane.add(GeneroCadastroDeMusicaGUI);
		
		JLabel lblLinkDeVideoCadastroDeMusicaGUI = new JLabel("Link de v\u00EDdeo:");
		lblLinkDeVideoCadastroDeMusicaGUI.setForeground(Color.WHITE);
		lblLinkDeVideoCadastroDeMusicaGUI.setHorizontalAlignment(SwingConstants.CENTER);
		lblLinkDeVideoCadastroDeMusicaGUI.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblLinkDeVideoCadastroDeMusicaGUI.setBounds(391, 135, 134, 25);
		contentPane.add(lblLinkDeVideoCadastroDeMusicaGUI);
		
		LinkDeVideoCadastroDeMusicaGUI = new JTextField();
		LinkDeVideoCadastroDeMusicaGUI.setColumns(10);
		LinkDeVideoCadastroDeMusicaGUI.setBounds(531, 135, 231, 25);
		contentPane.add(LinkDeVideoCadastroDeMusicaGUI);
		
		JLabel lblAparicoesCadastroDeMusica = new JLabel("Apari\u00E7\u00F5es:");
		lblAparicoesCadastroDeMusica.setForeground(Color.WHITE);
		lblAparicoesCadastroDeMusica.setHorizontalAlignment(SwingConstants.CENTER);
		lblAparicoesCadastroDeMusica.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblAparicoesCadastroDeMusica.setBounds(391, 192, 134, 25);
		contentPane.add(lblAparicoesCadastroDeMusica);
		
		AparicoesCadastroDeMusicaGUI = new JTextField();
		AparicoesCadastroDeMusicaGUI.setColumns(10);
		AparicoesCadastroDeMusicaGUI.setBounds(531, 192, 231, 25);
		contentPane.add(AparicoesCadastroDeMusicaGUI);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 229, 371, 341);
		panel.setBackground(new Color(0,0,0,150));
		contentPane.add(panel);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(0, 0, 0, 150));
		panel_1.setBounds(401, 229, 371, 341);
		contentPane.add(panel_1);
		
		JButton btnCancelarCadastroDeMusicaGUI = new JButton("CANCELAR");
		btnCancelarCadastroDeMusicaGUI.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				dispose();
			}
		});
		btnCancelarCadastroDeMusicaGUI.setBounds(267, 580, 114, 35);
		contentPane.add(btnCancelarCadastroDeMusicaGUI);
		
		JButton btnSalvarCadastroDeMusicaGUI = new JButton("SALVAR");
		btnSalvarCadastroDeMusicaGUI.setBounds(400, 580, 114, 35);
		contentPane.add(btnSalvarCadastroDeMusicaGUI);
		
		label = new JLabel("");
		label.setIcon(new ImageIcon(PPaginaCadastroDeMusicaGUI.class.getResource("/img/gray.jpg")));
		label.setBounds(0, 0, 1071, 748);
		contentPane.add(label);
	}
}
