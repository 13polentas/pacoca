package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.MudarTelaController;

import javax.swing.JTextField;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class GPaginaPesquisasGUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField_2;
	private JTable table;
	private JTextField textField;
	private JTextField txtBarraDePesquisaPaginaPesquisas;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GPaginaPesquisasGUI frame = new GPaginaPesquisasGUI();
					frame.setUndecorated(true);
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GPaginaPesquisasGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1600, 900);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(40, 316, 1494, 508);
		panel.setBackground(new Color (0,0,0,150));
		contentPane.add(panel);
		
		table = new JTable();
		panel.add(table);
		
		txtBarraDePesquisaPaginaPesquisas = new JTextField();
		txtBarraDePesquisaPaginaPesquisas.setText("Barra de pesquisa");
		txtBarraDePesquisaPaginaPesquisas.setFont(new Font("Tahoma", Font.ITALIC, 25));
		txtBarraDePesquisaPaginaPesquisas.setColumns(10);
		txtBarraDePesquisaPaginaPesquisas.setBounds(1049, 121, 329, 37);
		contentPane.add(txtBarraDePesquisaPaginaPesquisas);
		
		textField_2 = new JTextField();
		textField_2.setText("Barra de pesquisa");
		textField_2.setFont(new Font("Tahoma", Font.PLAIN, 25));
		textField_2.setColumns(10);
		textField_2.setBounds(40, 245, 1494, 37);
		contentPane.add(textField_2);
		
		textField = new JTextField();
		textField.setText("AMUSING");
		textField.setFont(new Font("Tahoma", Font.ITALIC, 85));
		textField.setEditable(false);
		textField.setColumns(10);
		textField.setBounds(30, 40, 396, 152);
		contentPane.add(textField);
		
		JButton btnVoltarPaginaPesquisas = new JButton("\u2190");
		btnVoltarPaginaPesquisas.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnVoltarPaginaPesquisas.setBounds(798, 121, 64, 37);
		contentPane.add(btnVoltarPaginaPesquisas);
		
		JButton btnManualPaginaPesquisas = new JButton("");
		btnManualPaginaPesquisas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaManualGUI();
								
			}
		});
		btnManualPaginaPesquisas.setIcon(new ImageIcon(GPaginaPesquisasGUI.class.getResource("/img/q-mark-icon_002070_64.png")));
		btnManualPaginaPesquisas.setBounds(871, 121, 46, 37);
		contentPane.add(btnManualPaginaPesquisas);
		
		JButton btnInicioPaginaPesquisas = new JButton("In\u00EDcio");
		btnInicioPaginaPesquisas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaInicialGUI();
				
				dispose();
				
			}
		});
		btnInicioPaginaPesquisas.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnInicioPaginaPesquisas.setBounds(798, 51, 119, 41);
		contentPane.add(btnInicioPaginaPesquisas);
		
		JButton btnMusicaPaginaPesquisas = new JButton("M\u00FAsica");
		btnMusicaPaginaPesquisas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaMusicasGUI();
				
				dispose();
				
			}
		});
		btnMusicaPaginaPesquisas.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnMusicaPaginaPesquisas.setBounds(945, 51, 136, 41);
		contentPane.add(btnMusicaPaginaPesquisas);
		
		JButton btnFilmePaginaPesquisas = new JButton("Filme");
		btnFilmePaginaPesquisas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaFilmesGUI();
				
				dispose();
				
			}
		});
		btnFilmePaginaPesquisas.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnFilmePaginaPesquisas.setBounds(1108, 51, 119, 41);
		contentPane.add(btnFilmePaginaPesquisas);
		
		JButton btnSeriePaginaPesquisas = new JButton("S\u00E9rie");
		btnSeriePaginaPesquisas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaSeriesGUI();
				
				dispose();
				
			}
		});
		btnSeriePaginaPesquisas.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnSeriePaginaPesquisas.setBounds(1259, 51, 119, 41);
		contentPane.add(btnSeriePaginaPesquisas);
		
		final JPanel pnlConfiguracoes = new JPanel();
		pnlConfiguracoes.setBounds(1415, 47, 119, 200);
		getContentPane().add(pnlConfiguracoes);
		pnlConfiguracoes.setLayout(null);
		pnlConfiguracoes.setVisible(false);
		
		JButton btnMinhasListasPaginaConfiguracoes = new JButton("Minhas listas");
		btnMinhasListasPaginaConfiguracoes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaListasGUI();
				
				dispose();
				
			}
		});
		btnMinhasListasPaginaConfiguracoes.setBounds(0, 114, 119, 30);
		pnlConfiguracoes.add(btnMinhasListasPaginaConfiguracoes);
		
		JButton btnConfiguracoesPaginaSeries = new JButton("Configura\u00E7\u00F5es");
		btnConfiguracoesPaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaConfiguracoesGUI();
				
				dispose();
				
			}
		});
		btnConfiguracoesPaginaSeries.setBounds(0, 142, 119, 30);
		pnlConfiguracoes.add(btnConfiguracoesPaginaSeries);
		
		JButton btnSairPaginaSeries = new JButton("Sair");	
		btnSairPaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaSairGUI();
								
			}
		});
		btnSairPaginaSeries.setBounds(0, 170, 119, 30);
		pnlConfiguracoes.add(btnSairPaginaSeries);
		
		JButton btnFoto = new JButton("foto");
		btnFoto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				pnlConfiguracoes.setVisible(false);
				
			}
		});
		
		btnFoto.setBounds(0, 0, 119, 115);
		pnlConfiguracoes.add(btnFoto);
		btnFoto.setFont(new Font("Tahoma", Font.ITALIC, 20));
		
		JPanel pnlTransparencia1PaginaSeries = new JPanel();
		pnlTransparencia1PaginaSeries.setBounds(10, 220, 1564, 633);
		pnlTransparencia1PaginaSeries.setBackground(new Color(0, 0, 0, 150));
		
		getContentPane().add(pnlTransparencia1PaginaSeries);
		pnlTransparencia1PaginaSeries.setLayout(null);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(GPaginaPesquisasGUI.class.getResource("/img/filme1.jpg")));
		label.setBounds(0, 0, 1600, 900);
		contentPane.add(label);
	}
}
