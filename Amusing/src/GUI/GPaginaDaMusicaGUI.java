
package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.MudarTelaController;

import javax.swing.JTextField;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

public class GPaginaDaMusicaGUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField;
	private JTextField txtBarraDePesquisaPaginaDaMusica;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GPaginaDaMusicaGUI frame = new GPaginaDaMusicaGUI();
					frame.setUndecorated(true);
			        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GPaginaDaMusicaGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1600, 900);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDurao = new JLabel("Dura\u00E7\u00E3o:");
		lblDurao.setForeground(Color.WHITE);
		lblDurao.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblDurao.setBounds(30, 691, 135, 37);
		contentPane.add(lblDurao);
		
		JLabel lblGnero = new JLabel("G\u00EAnero:");
		lblGnero.setForeground(Color.WHITE);
		lblGnero.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblGnero.setBounds(30, 654, 135, 37);
		contentPane.add(lblGnero);
		
		textField_4 = new JTextField();
		textField_4.setEditable(false);
		textField_4.setColumns(10);
		textField_4.setBounds(161, 662, 233, 26);
		contentPane.add(textField_4);
		
		textField_2 = new JTextField();
		textField_2.setEditable(false);
		textField_2.setBounds(161, 591, 233, 26);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		textField_5 = new JTextField();
		textField_5.setEditable(false);
		textField_5.setColumns(10);
		textField_5.setBounds(161, 699, 233, 26);
		contentPane.add(textField_5);
		
		JButton btnEstrelasDeAvaliacaoPaginaDaMusica = new JButton("Estrelas de Avalia\u00E7\u00E3o");
		btnEstrelasDeAvaliacaoPaginaDaMusica.setFont(new Font("Tahoma", Font.ITALIC, 15));
		btnEstrelasDeAvaliacaoPaginaDaMusica.setBounds(30, 788, 364, 41);
		contentPane.add(btnEstrelasDeAvaliacaoPaginaDaMusica);
		
		JPanel panel = new JPanel();
		panel.setBounds(30, 270, 364, 295);
		contentPane.add(panel);
		panel.setBackground(new Color(0, 0, 0, 150));
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Capa ");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setForeground(Color.LIGHT_GRAY);
		lblNewLabel.setFont(new Font("Tahoma", Font.ITALIC, 20));
		lblNewLabel.setBounds(10, 34, 344, 67);
		panel.add(lblNewLabel);
		
		textField_6 = new JTextField();
		textField_6.setEditable(false);
		textField_6.setColumns(10);
		textField_6.setBounds(161, 735, 233, 26);
		contentPane.add(textField_6);
		
		JLabel lblLanamento = new JLabel("Lan\u00E7amento:");
		lblLanamento.setForeground(Color.WHITE);
		lblLanamento.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblLanamento.setBounds(30, 727, 135, 37);
		contentPane.add(lblLanamento);
		
		textField_3 = new JTextField();
		textField_3.setEditable(false);
		textField_3.setColumns(10);
		textField_3.setBounds(161, 626, 233, 26);
		contentPane.add(textField_3);
		
		JLabel lblCantorbanda = new JLabel("Cantor/Banda:");
		lblCantorbanda.setForeground(Color.WHITE);
		lblCantorbanda.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblCantorbanda.setBounds(30, 618, 135, 37);
		contentPane.add(lblCantorbanda);
		
		JLabel lblCompositorNome = new JLabel("Compositor: ");
		lblCompositorNome.setForeground(Color.WHITE);
		lblCompositorNome.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblCompositorNome.setBounds(30, 583, 135, 37);
		contentPane.add(lblCompositorNome);
		
		JLabel lblNomeDaMsica = new JLabel("         Nome da M\u00FAsica");
		lblNomeDaMsica.setFont(new Font("Tahoma", Font.ITALIC, 25));
		lblNomeDaMsica.setForeground(Color.WHITE);
		lblNomeDaMsica.setBounds(30, 224, 1307, 37);
		contentPane.add(lblNomeDaMsica);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(498, 270, 672, 559);
		contentPane.add(panel_1);
		panel_1.setBackground(new Color(0, 0, 0, 150));

		panel_1.setLayout(null);
		
		JLabel lblLetraDaMusica = new JLabel("Letra da M\u00FAsica");
		lblLetraDaMusica.setForeground(Color.LIGHT_GRAY);
		lblLetraDaMusica.setFont(new Font("Tahoma", Font.ITALIC, 20));
		lblLetraDaMusica.setBounds(249, 24, 196, 66);
		panel_1.add(lblLetraDaMusica);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(1223, 270, 318, 559);
		contentPane.add(panel_2);
		panel_2.setBackground(new Color(0, 0, 0, 150));
		panel_2.setLayout(null);
		
		JLabel lblFilmesESeries = new JLabel("Filmes e series (em que aparece data lancamento inverso)");
		lblFilmesESeries.setForeground(Color.LIGHT_GRAY);
		lblFilmesESeries.setFont(new Font("Tahoma", Font.ITALIC, 11));
		lblFilmesESeries.setBounds(10, 11, 281, 84);
		panel_2.add(lblFilmesESeries);
		
		textField = new JTextField();
		textField.setText("AMUSING");
		textField.setFont(new Font("Tahoma", Font.ITALIC, 85));
		textField.setEditable(false);
		textField.setColumns(10);
		textField.setBounds(30, 40, 396, 152);
		contentPane.add(textField);
		
		JButton button = new JButton("\u2190");
		button.setFont(new Font("Tahoma", Font.PLAIN, 20));
		button.setBounds(798, 121, 64, 37);
		contentPane.add(button);
		
		JButton btnManualPaginaDaMusica = new JButton("");
		btnManualPaginaDaMusica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaManualGUI();
								
			}
		});
		btnManualPaginaDaMusica.setIcon(new ImageIcon(GPaginaDaMusicaGUI.class.getResource("/img/q-mark-icon_002070_64.png")));
		btnManualPaginaDaMusica.setBounds(871, 121, 46, 37);
		contentPane.add(btnManualPaginaDaMusica);
		
		JButton btnInicioPaginaDaMusica = new JButton("In\u00EDcio");
		btnInicioPaginaDaMusica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaInicialGUI();
				
				dispose();
				
			}
		});
		btnInicioPaginaDaMusica.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnInicioPaginaDaMusica.setBounds(798, 51, 119, 41);
		contentPane.add(btnInicioPaginaDaMusica);
		
		JButton btnMusicaPaginaDaMusica = new JButton("M\u00FAsica");
		btnMusicaPaginaDaMusica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaMusicasGUI();
				
				dispose();
				
			}
		});
		btnMusicaPaginaDaMusica.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnMusicaPaginaDaMusica.setBounds(945, 51, 136, 41);
		contentPane.add(btnMusicaPaginaDaMusica);
		
		txtBarraDePesquisaPaginaDaMusica = new JTextField();
		txtBarraDePesquisaPaginaDaMusica.setText("Barra de pesquisa");
		txtBarraDePesquisaPaginaDaMusica.setFont(new Font("Tahoma", Font.ITALIC, 25));
		txtBarraDePesquisaPaginaDaMusica.setColumns(10);
		txtBarraDePesquisaPaginaDaMusica.setBounds(1049, 121, 329, 37);
		contentPane.add(txtBarraDePesquisaPaginaDaMusica);
		
		JButton btnFilmePaginaDaMusica = new JButton("Filme");
		btnFilmePaginaDaMusica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaFilmesGUI();
				
				dispose();
				
			}
		});
		btnFilmePaginaDaMusica.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnFilmePaginaDaMusica.setBounds(1108, 51, 119, 41);
		contentPane.add(btnFilmePaginaDaMusica);
		
		JButton btnSeriePaginaDaMusica = new JButton("S\u00E9rie");
		btnSeriePaginaDaMusica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaSeriesGUI();
				
				dispose();
				
			}
		});
		btnSeriePaginaDaMusica.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnSeriePaginaDaMusica.setBounds(1259, 51, 119, 41);
		contentPane.add(btnSeriePaginaDaMusica);
		
		final JPanel pnlConfiguracoes = new JPanel();
		pnlConfiguracoes.setBounds(1415, 47, 119, 200);
		getContentPane().add(pnlConfiguracoes);
		pnlConfiguracoes.setLayout(null);
		pnlConfiguracoes.setVisible(false);
		
		JButton btnMinhasListasPaginaConfiguracoes = new JButton("Minhas listas");
		btnMinhasListasPaginaConfiguracoes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaListasGUI();
				
				dispose();
				
			}
		});
		btnMinhasListasPaginaConfiguracoes.setBounds(0, 114, 119, 30);
		pnlConfiguracoes.add(btnMinhasListasPaginaConfiguracoes);
		
		JButton btnConfiguracoesPaginaSeries = new JButton("Configura\u00E7\u00F5es");
		btnConfiguracoesPaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaConfiguracoesGUI();
				
				dispose();
				
			}
		});
		btnConfiguracoesPaginaSeries.setBounds(0, 142, 119, 30);
		pnlConfiguracoes.add(btnConfiguracoesPaginaSeries);
		
		JButton btnSairPaginaSeries = new JButton("Sair");	
		btnSairPaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaSairGUI();
								
			}
		});
		btnSairPaginaSeries.setBounds(0, 170, 119, 30);
		pnlConfiguracoes.add(btnSairPaginaSeries);
		
		JButton btnFoto = new JButton("foto");
		btnFoto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				pnlConfiguracoes.setVisible(false);
				
			}
		});
		
		btnFoto.setBounds(0, 0, 119, 115);
		pnlConfiguracoes.add(btnFoto);
		btnFoto.setFont(new Font("Tahoma", Font.ITALIC, 20));
		
		JPanel pnlTransparencia1PaginaDaSerie = new JPanel();
		pnlTransparencia1PaginaDaSerie.setBounds(10, 220, 1564, 633);
		pnlTransparencia1PaginaDaSerie.setBackground(new Color(0, 0, 0, 150));
		
		getContentPane().add(pnlTransparencia1PaginaDaSerie);
		pnlTransparencia1PaginaDaSerie.setLayout(null);
		
		JButton btnEditarMusicaPaginaDaMusica = new JButton("Editar M\u00FAsica");
		btnEditarMusicaPaginaDaMusica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaCadastroDeMusicaGUI();
								
			}
		});
		btnEditarMusicaPaginaDaMusica.setBounds(1307, 0, 217, 41);
		pnlTransparencia1PaginaDaSerie.add(btnEditarMusicaPaginaDaMusica);
		btnEditarMusicaPaginaDaMusica.setFont(new Font("Tahoma", Font.PLAIN, 25));
		
		JButton btnFotoPaginaSeries = new JButton("foto");
		btnFotoPaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				pnlConfiguracoes.setVisible(true);
				
			}
		});
		btnFotoPaginaSeries.setFont(new Font("Tahoma", Font.ITALIC, 20));
		btnFotoPaginaSeries.setBounds(1415, 47, 119, 115);
		getContentPane().add(btnFotoPaginaSeries);
		
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(GPaginaDaMusicaGUI.class.getResource("/img/musica7.jpg")));
		label.setBounds(0, 0, 1600, 900);
		contentPane.add(label);
	}
}
