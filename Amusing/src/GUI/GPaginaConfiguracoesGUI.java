package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.MudarTelaController;

import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class GPaginaConfiguracoesGUI extends JFrame {
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField textField;
	private JTextField txtBarraDePesquisaPaginaConfiguracoes;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GPaginaConfiguracoesGUI frame = new GPaginaConfiguracoesGUI();
					frame.setUndecorated(true);
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GPaginaConfiguracoesGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1600, 900);
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblConfiguracoes = new JLabel("Configura\u00E7\u00F5es");
		lblConfiguracoes.setForeground(Color.WHITE);
		lblConfiguracoes.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblConfiguracoes.setBounds(106, 228, 174, 65);
		contentPane.add(lblConfiguracoes);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setForeground(Color.WHITE);
		lblNome.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNome.setBounds(143, 299, 75, 41);
		contentPane.add(lblNome);
		
		textField_3 = new JTextField();
		textField_3.setBounds(273, 309, 542, 29);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		JLabel lblIdade = new JLabel("Idade:");
		lblIdade.setForeground(Color.WHITE);
		lblIdade.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblIdade.setBounds(143, 361, 75, 41);
		contentPane.add(lblIdade);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(273, 371, 542, 29);
		contentPane.add(textField_4);
		
		JLabel lblNovaSenha = new JLabel("Nova Senha:");
		lblNovaSenha.setForeground(Color.WHITE);
		lblNovaSenha.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNovaSenha.setBounds(143, 431, 120, 41);
		contentPane.add(lblNovaSenha);
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(273, 441, 542, 29);
		contentPane.add(textField_5);
		
		JLabel lblRepitaSenha = new JLabel("Repita senha:");
		lblRepitaSenha.setForeground(Color.WHITE);
		lblRepitaSenha.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblRepitaSenha.setBounds(340, 495, 147, 41);
		contentPane.add(lblRepitaSenha);
		
		textField_6 = new JTextField();
		textField_6.setColumns(10);
		textField_6.setBounds(497, 505, 318, 29);
		contentPane.add(textField_6);
		
		JLabel lblSenhaAntiga = new JLabel("Senha antiga:");
		lblSenhaAntiga.setForeground(Color.WHITE);
		lblSenhaAntiga.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblSenhaAntiga.setBounds(340, 560, 147, 41);
		contentPane.add(lblSenhaAntiga);
		
		textField_7 = new JTextField();
		textField_7.setColumns(10);
		textField_7.setBounds(497, 570, 318, 29);
		contentPane.add(textField_7);
		
		JLabel lblEmail = new JLabel("E-mail:");
		lblEmail.setForeground(Color.WHITE);
		lblEmail.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblEmail.setBounds(143, 628, 75, 41);
		contentPane.add(lblEmail);
		
		textField_8 = new JTextField();
		textField_8.setColumns(10);
		textField_8.setBounds(273, 638, 542, 29);
		contentPane.add(textField_8);
		
		JButton btnCancelarPaginaConfiguracoes = new JButton("Cancelar");
		btnCancelarPaginaConfiguracoes.setBounds(143, 731, 98, 34);
		contentPane.add(btnCancelarPaginaConfiguracoes);
		
		JButton btnSalvarPaginaConfiguracoes = new JButton("Salvar");
		btnSalvarPaginaConfiguracoes.setBounds(266, 731, 98, 34);
		contentPane.add(btnSalvarPaginaConfiguracoes);
		
		textField = new JTextField();
		textField.setText("AMUSING");
		textField.setFont(new Font("Tahoma", Font.ITALIC, 85));
		textField.setEditable(false);
		textField.setColumns(10);
		textField.setBounds(30, 40, 396, 152);
		contentPane.add(textField);
		
		JButton btnInicioPaginaConfiguracoes = new JButton("In\u00EDcio");
		btnInicioPaginaConfiguracoes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaInicialGUI();
				
				dispose();
				
			}
		});
		btnInicioPaginaConfiguracoes.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnInicioPaginaConfiguracoes.setBounds(798, 51, 119, 41);
		contentPane.add(btnInicioPaginaConfiguracoes);
		
		JButton btnVoltarPaginaConfiguracoes = new JButton("\u2190");
		btnVoltarPaginaConfiguracoes.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnVoltarPaginaConfiguracoes.setBounds(798, 121, 64, 37);
		contentPane.add(btnVoltarPaginaConfiguracoes);
		
		txtBarraDePesquisaPaginaConfiguracoes = new JTextField();
		txtBarraDePesquisaPaginaConfiguracoes.setText("Barra de pesquisa");
		txtBarraDePesquisaPaginaConfiguracoes.setFont(new Font("Tahoma", Font.ITALIC, 25));
		txtBarraDePesquisaPaginaConfiguracoes.setColumns(10);
		txtBarraDePesquisaPaginaConfiguracoes.setBounds(1049, 121, 329, 37);
		contentPane.add(txtBarraDePesquisaPaginaConfiguracoes);
		
		final JPanel pnlConfiguracoes = new JPanel();
		pnlConfiguracoes.setBounds(1415, 47, 119, 200);
		getContentPane().add(pnlConfiguracoes);
		pnlConfiguracoes.setLayout(null);
		pnlConfiguracoes.setVisible(false);
		
		JButton btnMinhasListasPaginaConfiguracoes = new JButton("Minhas listas");
		btnMinhasListasPaginaConfiguracoes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaListasGUI();
				
				dispose();
				
			}
		});
		btnMinhasListasPaginaConfiguracoes.setBounds(0, 114, 119, 30);
		pnlConfiguracoes.add(btnMinhasListasPaginaConfiguracoes);
		
		JButton btnConfiguracoesPaginaSeries = new JButton("Configura\u00E7\u00F5es");
		btnConfiguracoesPaginaSeries.setBounds(0, 142, 119, 30);
		pnlConfiguracoes.add(btnConfiguracoesPaginaSeries);
		
		JButton btnSairPaginaSeries = new JButton("Sair");	
		btnSairPaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaSairGUI();
								
			}
		});
		btnSairPaginaSeries.setBounds(0, 170, 119, 30);
		pnlConfiguracoes.add(btnSairPaginaSeries);
		
		JButton btnFoto = new JButton("foto");
		btnFoto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				pnlConfiguracoes.setVisible(false);
				
			}
		});
		btnFoto.setBounds(0, 0, 119, 115);
		pnlConfiguracoes.add(btnFoto);
		btnFoto.setFont(new Font("Tahoma", Font.ITALIC, 20));
		
		
		JButton btnFotoPaginaSeries = new JButton("foto");
		btnFotoPaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				pnlConfiguracoes.setVisible(true);
				
			}
		});
		btnFotoPaginaSeries.setFont(new Font("Tahoma", Font.ITALIC, 20));
		btnFotoPaginaSeries.setBounds(1415, 47, 119, 115);
		getContentPane().add(btnFotoPaginaSeries);
		
		JButton btnMusicaPaginaConfiguracoes = new JButton("M\u00FAsica");
		btnMusicaPaginaConfiguracoes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaMusicasGUI();
				
				dispose();
				
			}
		});
		btnMusicaPaginaConfiguracoes.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnMusicaPaginaConfiguracoes.setBounds(945, 51, 136, 41);
		contentPane.add(btnMusicaPaginaConfiguracoes);
		
		JButton btnFilmePaginaConfiguracoes = new JButton("Filme");
		btnFilmePaginaConfiguracoes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaFilmesGUI();
				
				dispose();
				
			}
		});
		btnFilmePaginaConfiguracoes.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnFilmePaginaConfiguracoes.setBounds(1108, 51, 119, 41);
		contentPane.add(btnFilmePaginaConfiguracoes);
		
		JButton btnSeriePaginaConfiguracoes = new JButton("S\u00E9rie");
		btnSeriePaginaConfiguracoes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaSeriesGUI();
				
				dispose();
				
			}
		});
		btnSeriePaginaConfiguracoes.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnSeriePaginaConfiguracoes.setBounds(1259, 51, 119, 41);
		contentPane.add(btnSeriePaginaConfiguracoes);
		
		JButton btnManualPaginaConfiguracoes = new JButton("");
		btnManualPaginaConfiguracoes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaManualGUI();
								
			}
		});
		btnManualPaginaConfiguracoes.setIcon(new ImageIcon(GPaginaConfiguracoesGUI.class.getResource("/img/q-mark-icon_002070_64.png")));
		btnManualPaginaConfiguracoes.setBounds(871, 121, 46, 37);
		contentPane.add(btnManualPaginaConfiguracoes);
		
		JPanel pnlTransparencia1PaginaSeries = new JPanel();
		pnlTransparencia1PaginaSeries.setBounds(10, 220, 1564, 633);
		pnlTransparencia1PaginaSeries.setBackground(new Color(0, 0, 0, 150));
		
		getContentPane().add(pnlTransparencia1PaginaSeries);
		pnlTransparencia1PaginaSeries.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(GPaginaConfiguracoesGUI.class.getResource("/img/lorax.jpg")));
		lblNewLabel.setBounds(0, 0, 1600, 900);
		contentPane.add(lblNewLabel);
	}
}
