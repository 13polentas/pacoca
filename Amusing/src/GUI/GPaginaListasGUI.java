package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.MudarTelaController;

import javax.swing.JTextField;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

public class GPaginaListasGUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField txtBarraDePesquisaPaginaListas;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GPaginaListasGUI frame = new GPaginaListasGUI();
					frame.setUndecorated(true);
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GPaginaListasGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1600, 900);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel pnlTransparencia1PaginaDaSerie = new JPanel();
		pnlTransparencia1PaginaDaSerie.setBounds(10, 220, 1564, 633);
		pnlTransparencia1PaginaDaSerie.setBackground(new Color(0, 0, 0, 150));
		
		getContentPane().add(pnlTransparencia1PaginaDaSerie);
		pnlTransparencia1PaginaDaSerie.setLayout(null);
		
		JButton btnIrParaPgina = new JButton("Ir para P\u00E1gina da Lista (BOT\u00C3O DE DEMONSTRA\u00C7\u00C3O)");
		btnIrParaPgina.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaDaListaGUI();
				
				dispose();
				
			}
		});
		btnIrParaPgina.setBounds(233, 26, 379, 43);
		pnlTransparencia1PaginaDaSerie.add(btnIrParaPgina);
		
		JPanel panel = new JPanel();
		panel.setBounds(73, 80, 1454, 508);
		panel.setBackground(new Color(0,0,0,150));
		pnlTransparencia1PaginaDaSerie.add(panel);
		
		table = new JTable();
		panel.add(table);
		table.setBackground(new Color(0, 0, 0, 180));
		
		JLabel lblListas = new JLabel("Listas");
		lblListas.setBounds(73, 13, 117, 56);
		pnlTransparencia1PaginaDaSerie.add(lblListas);
		lblListas.setForeground(Color.WHITE);
		lblListas.setFont(new Font("Tahoma", Font.PLAIN, 40));
		
		JButton btnAdicionarListaPaginaListas = new JButton("Adicionar Lista");
		btnAdicionarListaPaginaListas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaAdicionarListaGUI();
				
			}
		});
		btnAdicionarListaPaginaListas.setBounds(1296, 11, 217, 41);
		pnlTransparencia1PaginaDaSerie.add(btnAdicionarListaPaginaListas);
		btnAdicionarListaPaginaListas.setFont(new Font("Tahoma", Font.PLAIN, 25));
		
		textField = new JTextField();
		textField.setText("AMUSING");
		textField.setFont(new Font("Tahoma", Font.ITALIC, 85));
		textField.setEditable(false);
		textField.setColumns(10);
		textField.setBounds(30, 40, 396, 152);
		contentPane.add(textField);
		
		JButton btnInicioPaginaListas = new JButton("In\u00EDcio");
		btnInicioPaginaListas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaInicialGUI();
				
				dispose();
				
			}
		});
		btnInicioPaginaListas.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnInicioPaginaListas.setBounds(798, 51, 119, 41);
		contentPane.add(btnInicioPaginaListas);
		
		JButton btnVoltarPaginaListas = new JButton("\u2190");
		btnVoltarPaginaListas.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnVoltarPaginaListas.setBounds(798, 121, 64, 37);
		contentPane.add(btnVoltarPaginaListas);
		
		JButton btnManualPaginaListas = new JButton("");
		btnManualPaginaListas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaManualGUI();
								
			}
		});
		btnManualPaginaListas.setBounds(871, 121, 46, 37);
		contentPane.add(btnManualPaginaListas);
		
		JButton btnMusicaPaginaListas = new JButton("M\u00FAsica");
		btnMusicaPaginaListas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaMusicasGUI();
				
				dispose();
				
			}
		});
		btnMusicaPaginaListas.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnMusicaPaginaListas.setBounds(945, 51, 136, 41);
		contentPane.add(btnMusicaPaginaListas);
		
		txtBarraDePesquisaPaginaListas = new JTextField();
		txtBarraDePesquisaPaginaListas.setText("Barra de pesquisa");
		txtBarraDePesquisaPaginaListas.setFont(new Font("Tahoma", Font.ITALIC, 25));
		txtBarraDePesquisaPaginaListas.setColumns(10);
		txtBarraDePesquisaPaginaListas.setBounds(1049, 121, 329, 37);
		contentPane.add(txtBarraDePesquisaPaginaListas);
		
		JButton btnFilmePaginaListas = new JButton("Filme");
		btnFilmePaginaListas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaFilmesGUI();
				
				dispose();
				
			}
		});
		btnFilmePaginaListas.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnFilmePaginaListas.setBounds(1108, 51, 119, 41);
		contentPane.add(btnFilmePaginaListas);
		
		JButton btnSeriePaginaListas = new JButton("S\u00E9rie");
		btnSeriePaginaListas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaSeriesGUI();
				
				dispose();
				
			}
		});
		btnSeriePaginaListas.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnSeriePaginaListas.setBounds(1259, 51, 119, 41);
		contentPane.add(btnSeriePaginaListas);
		
		final JPanel pnlConfiguracoes = new JPanel();
		pnlConfiguracoes.setBounds(1415, 47, 119, 200);
		getContentPane().add(pnlConfiguracoes);
		pnlConfiguracoes.setLayout(null);
		pnlConfiguracoes.setVisible(false);
		
		JButton btnMinhasListasPaginaConfiguracoes = new JButton("Minhas listas");
		btnMinhasListasPaginaConfiguracoes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaConfiguracoesGUI();
				
				dispose();
				
			}
		});
		btnMinhasListasPaginaConfiguracoes.setBounds(0, 114, 119, 30);
		pnlConfiguracoes.add(btnMinhasListasPaginaConfiguracoes);
		
		JButton btnConfiguracoesPaginaSeries = new JButton("Configura\u00E7\u00F5es");
		btnConfiguracoesPaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaConfiguracoesGUI();
				
				dispose();
				
			}
		});
		btnConfiguracoesPaginaSeries.setBounds(0, 142, 119, 30);
		pnlConfiguracoes.add(btnConfiguracoesPaginaSeries);
		
		JButton btnSairPaginaSeries = new JButton("Sair");	
		btnSairPaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaSairGUI();
								
			}
		});
		btnSairPaginaSeries.setBounds(0, 170, 119, 30);
		pnlConfiguracoes.add(btnSairPaginaSeries);
		
		JButton btnFoto = new JButton("foto");
		btnFoto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				pnlConfiguracoes.setVisible(false);
				
			}
		});
		
		btnFoto.setBounds(0, 0, 119, 115);
		pnlConfiguracoes.add(btnFoto);
		btnFoto.setFont(new Font("Tahoma", Font.ITALIC, 20));
		
		JButton btnFotoPaginaSeries = new JButton("foto");
		btnFotoPaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				pnlConfiguracoes.setVisible(true);
				
			}
		});
		btnFotoPaginaSeries.setFont(new Font("Tahoma", Font.ITALIC, 20));
		btnFotoPaginaSeries.setBounds(1415, 47, 119, 115);
		getContentPane().add(btnFotoPaginaSeries);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(GPaginaListasGUI.class.getResource("/img/detona_ralph.jpg")));
		label.setBounds(0, 0, 1600, 900);
		contentPane.add(label);
	}
}
