package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Color;

public class PPaginaAdicionarEpisodioGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtTituloAdicionarEpisodioGUI;
	private JTextField txtNumeroAdicionarEpisodioGUI;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PPaginaAdicionarEpisodioGUI frame = new PPaginaAdicionarEpisodioGUI();
					frame.setUndecorated(true);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PPaginaAdicionarEpisodioGUI() {
		setTitle("Adicionar epis\u00F3dio");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 436, 537);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(24, 198, 388, 271);
		panel.setBackground(new Color(0,0,0,150));
		contentPane.add(panel);
		
		JLabel lblTituloAdicionarEpisodioGUI = new JLabel("T\u00EDtulo:");
		lblTituloAdicionarEpisodioGUI.setForeground(Color.WHITE);
		lblTituloAdicionarEpisodioGUI.setHorizontalAlignment(SwingConstants.CENTER);
		lblTituloAdicionarEpisodioGUI.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblTituloAdicionarEpisodioGUI.setBounds(24, 28, 108, 39);
		contentPane.add(lblTituloAdicionarEpisodioGUI);
		
		txtTituloAdicionarEpisodioGUI = new JTextField();
		txtTituloAdicionarEpisodioGUI.setBounds(120, 35, 292, 33);
		contentPane.add(txtTituloAdicionarEpisodioGUI);
		txtTituloAdicionarEpisodioGUI.setColumns(10);
		
		JLabel lblNumeroAdicionarEpisodioGUI = new JLabel("N\u00FAmero:");
		lblNumeroAdicionarEpisodioGUI.setForeground(Color.WHITE);
		lblNumeroAdicionarEpisodioGUI.setHorizontalAlignment(SwingConstants.CENTER);
		lblNumeroAdicionarEpisodioGUI.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNumeroAdicionarEpisodioGUI.setBounds(24, 90, 108, 39);
		contentPane.add(lblNumeroAdicionarEpisodioGUI);
		
		txtNumeroAdicionarEpisodioGUI = new JTextField();
		txtNumeroAdicionarEpisodioGUI.setBounds(120, 97, 292, 33);
		contentPane.add(txtNumeroAdicionarEpisodioGUI);
		txtNumeroAdicionarEpisodioGUI.setColumns(10);
		
		JButton btnSalvarAdicionarEpisodioGUI = new JButton("Salvar");
		btnSalvarAdicionarEpisodioGUI.setBounds(314, 491, 98, 34);
		contentPane.add(btnSalvarAdicionarEpisodioGUI);
		
		JButton btnCancelarAdicionarEpisodioGUI = new JButton("Cancelar");
		btnCancelarAdicionarEpisodioGUI.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				dispose();
			}
		});
		btnCancelarAdicionarEpisodioGUI.setBounds(191, 491, 98, 34);
		contentPane.add(btnCancelarAdicionarEpisodioGUI);
		
		JLabel lblTrilhaSonoraAdicionarEpisodioGUI = new JLabel("Trilha Sonora:");
		lblTrilhaSonoraAdicionarEpisodioGUI.setForeground(Color.WHITE);
		lblTrilhaSonoraAdicionarEpisodioGUI.setHorizontalAlignment(SwingConstants.CENTER);
		lblTrilhaSonoraAdicionarEpisodioGUI.setFont(new Font("Tahoma", Font.ITALIC, 20));
		lblTrilhaSonoraAdicionarEpisodioGUI.setBounds(24, 154, 132, 33);
		contentPane.add(lblTrilhaSonoraAdicionarEpisodioGUI);
		
		JButton btnAddMusicaAdicionarEpisodioGUI = new JButton("Add m\u00FAsica");
		btnAddMusicaAdicionarEpisodioGUI.setBounds(286, 154, 126, 32);
		contentPane.add(btnAddMusicaAdicionarEpisodioGUI);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(PPaginaAdicionarEpisodioGUI.class.getResource("/img/gray.jpg")));
		label.setBounds(0, 0, 590, 634);
		contentPane.add(label);
	}
}
