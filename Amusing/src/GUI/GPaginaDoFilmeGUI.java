package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.MudarTelaController;

import javax.swing.JTextField;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

public class GPaginaDoFilmeGUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField;
	private JTextField txtBarraDePesquisaPaginaDoFilme;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GPaginaDoFilmeGUI frame = new GPaginaDoFilmeGUI();
					frame.setUndecorated(true);
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GPaginaDoFilmeGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1600, 900);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblLanamento = new JLabel("Lan\u00E7amento:");
		lblLanamento.setForeground(Color.WHITE);
		lblLanamento.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblLanamento.setBounds(30, 728, 135, 37);
		contentPane.add(lblLanamento);
		
		JLabel lblNomeDaMsica = new JLabel("         Nome do Filme");
		lblNomeDaMsica.setFont(new Font("Tahoma", Font.ITALIC, 25));
		lblNomeDaMsica.setForeground(Color.WHITE);
		lblNomeDaMsica.setBounds(30, 224, 1307, 37);
		contentPane.add(lblNomeDaMsica);
		
		JPanel panel_2 = new JPanel();
		panel_2.setLayout(null);
		panel_2.setBounds(1223, 315, 318, 514);
		panel_2.setBackground(new Color(0,0,0,150));
		contentPane.add(panel_2);
		
		JPanel panel_1 = new JPanel();
		panel_1.setLayout(null);
		panel_1.setBounds(498, 315, 672, 514);
		panel_1.setBackground(new Color(0,0,0,150));
		contentPane.add(panel_1);
		
		JButton button_7 = new JButton("Estrelas de Avalia\u00E7\u00E3o");
		button_7.setBounds(165, 462, 364, 41);
		panel_1.add(button_7);
		button_7.setFont(new Font("Tahoma", Font.ITALIC, 15));
		
		JLabel lblTrilhaSonora = new JLabel("Trilha Sonora");
		lblTrilhaSonora.setForeground(Color.WHITE);
		lblTrilhaSonora.setBounds(774, 254, 196, 66);
		contentPane.add(lblTrilhaSonora);
		lblTrilhaSonora.setFont(new Font("Tahoma", Font.PLAIN, 20));
		
		textField_5 = new JTextField();
		textField_5.setEditable(false);
		textField_5.setColumns(10);
		textField_5.setBounds(161, 699, 233, 26);
		contentPane.add(textField_5);
		
		textField_7 = new JTextField();
		textField_7.setEditable(false);
		textField_7.setColumns(10);
		textField_7.setBounds(161, 772, 233, 26);
		contentPane.add(textField_7);
		
		JLabel lblRoteirista = new JLabel("Roteirista:");
		lblRoteirista.setForeground(Color.WHITE);
		lblRoteirista.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblRoteirista.setBounds(30, 655, 135, 37);
		contentPane.add(lblRoteirista);
		
		JLabel label_3 = new JLabel("G\u00EAnero:");
		label_3.setForeground(Color.WHITE);
		label_3.setFont(new Font("Tahoma", Font.PLAIN, 18));
		label_3.setBounds(30, 691, 135, 37);
		contentPane.add(label_3);
		
		textField_3 = new JTextField();
		textField_3.setEditable(false);
		textField_3.setColumns(10);
		textField_3.setBounds(161, 628, 233, 26);
		contentPane.add(textField_3);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBounds(76, 315, 291, 267);
		panel.setBackground(new Color(0,0,0,150));
		contentPane.add(panel);
		
		JLabel lblCapaFilme = new JLabel("Capa Filme");
		lblCapaFilme.setFont(new Font("Tahoma", Font.ITALIC, 20));
		lblCapaFilme.setBounds(84, 76, 177, 67);
		panel.add(lblCapaFilme);
		
		textField_6 = new JTextField();
		textField_6.setEditable(false);
		textField_6.setColumns(10);
		textField_6.setBounds(161, 736, 233, 26);
		contentPane.add(textField_6);
		
		JLabel lblAtores = new JLabel("Atores");
		lblAtores.setForeground(Color.WHITE);
		lblAtores.setBounds(1345, 254, 105, 66);
		contentPane.add(lblAtores);
		lblAtores.setFont(new Font("Tahoma", Font.PLAIN, 20));
		
		textField_4 = new JTextField();
		textField_4.setEditable(false);
		textField_4.setColumns(10);
		textField_4.setBounds(161, 663, 233, 26);
		contentPane.add(textField_4);
		
		JLabel lblEstdio = new JLabel("Est\u00FAdio:");
		lblEstdio.setForeground(Color.WHITE);
		lblEstdio.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblEstdio.setBounds(30, 764, 135, 37);
		contentPane.add(lblEstdio);
		
		JLabel lblDiretor = new JLabel("Diretor:");
		lblDiretor.setForeground(Color.WHITE);
		lblDiretor.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblDiretor.setBounds(30, 620, 135, 37);
		contentPane.add(lblDiretor);
		
		
		JPanel pnlTransparencia1PaginaDoFilme = new JPanel();
		pnlTransparencia1PaginaDoFilme.setBounds(10, 220, 1564, 633);
		pnlTransparencia1PaginaDoFilme.setBackground(new Color(0, 0, 0, 150));
		
		getContentPane().add(pnlTransparencia1PaginaDoFilme);
		pnlTransparencia1PaginaDoFilme.setLayout(null);
		JButton btnEditarFilmePaginaDoFilme = new JButton("Editar Filme");
		btnEditarFilmePaginaDoFilme.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaCadastroDeFilmeGUI();
								
			}
		});
		
		btnEditarFilmePaginaDoFilme.setBounds(1307, 0, 217, 41);
		pnlTransparencia1PaginaDoFilme.add(btnEditarFilmePaginaDoFilme);
		btnEditarFilmePaginaDoFilme.setFont(new Font("Tahoma", Font.PLAIN, 25));
		
		
		JButton btnManualPaginaDoFilme = new JButton("");
		btnManualPaginaDoFilme.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaManualGUI();
								
			}
		});
		btnManualPaginaDoFilme.setIcon(new ImageIcon(GPaginaDoFilmeGUI.class.getResource("/img/q-mark-icon_002070_64.png")));
		btnManualPaginaDoFilme.setBounds(871, 121, 46, 37);
		contentPane.add(btnManualPaginaDoFilme);
		
		textField = new JTextField();
		textField.setText("AMUSING");
		textField.setFont(new Font("Tahoma", Font.ITALIC, 85));
		textField.setEditable(false);
		textField.setColumns(10);
		textField.setBounds(30, 40, 396, 152);
		contentPane.add(textField);
		
		JButton btnVoltarPaginaDoFilme = new JButton("\u2190");
		btnVoltarPaginaDoFilme.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnVoltarPaginaDoFilme.setBounds(798, 121, 64, 37);
		contentPane.add(btnVoltarPaginaDoFilme);
		
		JButton btnInicioPaginaDoFilme = new JButton("In\u00EDcio");
		btnInicioPaginaDoFilme.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaInicialGUI();
				
				dispose();
				
			}
		});
		btnInicioPaginaDoFilme.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnInicioPaginaDoFilme.setBounds(798, 51, 119, 41);
		contentPane.add(btnInicioPaginaDoFilme);
		
		txtBarraDePesquisaPaginaDoFilme = new JTextField();
		txtBarraDePesquisaPaginaDoFilme.setText("Barra de pesquisa");
		txtBarraDePesquisaPaginaDoFilme.setFont(new Font("Tahoma", Font.ITALIC, 25));
		txtBarraDePesquisaPaginaDoFilme.setColumns(10);
		txtBarraDePesquisaPaginaDoFilme.setBounds(1049, 121, 329, 37);
		contentPane.add(txtBarraDePesquisaPaginaDoFilme);
		
		JButton btnMusicaPaginaDoFilme = new JButton("M\u00FAsica");
		btnMusicaPaginaDoFilme.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaMusicasGUI();
				
				dispose();
				
			}
		});
		btnMusicaPaginaDoFilme.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnMusicaPaginaDoFilme.setBounds(945, 51, 136, 41);
		contentPane.add(btnMusicaPaginaDoFilme);
		
		JButton btnFilmePaginaDoFilme = new JButton("Filme");
		btnFilmePaginaDoFilme.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaFilmesGUI();
				
				dispose();
				
			}
		});
		btnFilmePaginaDoFilme.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnFilmePaginaDoFilme.setBounds(1108, 51, 119, 41);
		contentPane.add(btnFilmePaginaDoFilme);
		
		JButton btnSeriePaginaDoFilme = new JButton("S\u00E9rie");
		btnSeriePaginaDoFilme.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaSeriesGUI();
				
				dispose();
				
			}
		});
		btnSeriePaginaDoFilme.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnSeriePaginaDoFilme.setBounds(1259, 51, 119, 41);
		contentPane.add(btnSeriePaginaDoFilme);
		
		final JPanel pnlConfiguracoes = new JPanel();
		pnlConfiguracoes.setBounds(1415, 47, 119, 200);
		getContentPane().add(pnlConfiguracoes);
		pnlConfiguracoes.setLayout(null);
		pnlConfiguracoes.setVisible(false);
		
		JButton btnMinhasListasPaginaConfiguracoes = new JButton("Minhas listas");
		btnMinhasListasPaginaConfiguracoes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaListasGUI();
				
				dispose();
				
			}
		});
		btnMinhasListasPaginaConfiguracoes.setBounds(0, 114, 119, 30);
		pnlConfiguracoes.add(btnMinhasListasPaginaConfiguracoes);
		
		JButton btnConfiguracoesPaginaSeries = new JButton("Configura\u00E7\u00F5es");
		btnConfiguracoesPaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaConfiguracoesGUI();
				
				dispose();
				
			}
		});
		btnConfiguracoesPaginaSeries.setBounds(0, 142, 119, 30);
		pnlConfiguracoes.add(btnConfiguracoesPaginaSeries);
		
		JButton btnSairPaginaSeries = new JButton("Sair");	
		btnSairPaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaSairGUI();
								
			}
		});
		btnSairPaginaSeries.setBounds(0, 170, 119, 30);
		pnlConfiguracoes.add(btnSairPaginaSeries);
		
		JButton btnFoto = new JButton("foto");
		btnFoto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				pnlConfiguracoes.setVisible(false);
				
			}
		});
		
		btnFoto.setBounds(0, 0, 119, 115);
		pnlConfiguracoes.add(btnFoto);
		btnFoto.setFont(new Font("Tahoma", Font.ITALIC, 20));
		
		JButton btnFotoPaginaSeries = new JButton("foto");
		btnFotoPaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				pnlConfiguracoes.setVisible(true);
				
			}
		});
		btnFotoPaginaSeries.setFont(new Font("Tahoma", Font.ITALIC, 20));
		btnFotoPaginaSeries.setBounds(1415, 47, 119, 115);
		getContentPane().add(btnFotoPaginaSeries);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(GPaginaDoFilmeGUI.class.getResource("/img/filme2.jpg")));
		label.setBounds(0, 0, 1600, 900);
		contentPane.add(label);
		
	}

}
