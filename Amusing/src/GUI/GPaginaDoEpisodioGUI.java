package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.MudarTelaController;

import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class GPaginaDoEpisodioGUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField;
	private JTextField txtBarraDePesquisa;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GPaginaDoEpisodioGUI frame = new GPaginaDoEpisodioGUI();
					frame.setUndecorated(true);
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GPaginaDoEpisodioGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1600, 900);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnEditarMsica = new JButton("Editar Episodio");
		btnEditarMsica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaEditarEpisodioGUI();
								
			}
		});
		btnEditarMsica.setFont(new Font("Tahoma", Font.PLAIN, 25));
		btnEditarMsica.setBounds(1314, 222, 217, 41);
		contentPane.add(btnEditarMsica);
		
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBounds(76, 315, 291, 267);
		panel.setBackground(new Color(0,0,0,150));
		contentPane.add(panel);
		
		JLabel lblCapaFilme = new JLabel("Capa Serie");
		lblCapaFilme.setForeground(Color.LIGHT_GRAY);
		lblCapaFilme.setFont(new Font("Tahoma", Font.ITALIC, 20));
		lblCapaFilme.setBounds(84, 76, 177, 67);
		panel.add(lblCapaFilme);
		
		textField_4 = new JTextField();
		textField_4.setEditable(false);
		textField_4.setColumns(10);
		textField_4.setBounds(161, 663, 233, 26);
		contentPane.add(textField_4);
		
		JPanel panel_1 = new JPanel();
		panel_1.setLayout(null);
		panel_1.setBounds(498, 315, 672, 514);
		panel_1.setBackground(new Color(0,0,0,150));
		contentPane.add(panel_1);
		
		JButton button_7 = new JButton("Estrelas de Avalia\u00E7\u00E3o");
		button_7.setBounds(165, 462, 364, 41);
		panel_1.add(button_7);
		button_7.setFont(new Font("Tahoma", Font.ITALIC, 15));
		
		textField_5 = new JTextField();
		textField_5.setEditable(false);
		textField_5.setColumns(10);
		textField_5.setBounds(161, 699, 233, 26);
		contentPane.add(textField_5);
		
		JLabel lblRoteirista = new JLabel("Dura\u00E7\u00E3o:");
		lblRoteirista.setForeground(Color.WHITE);
		lblRoteirista.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblRoteirista.setBounds(30, 655, 135, 37);
		contentPane.add(lblRoteirista);
		
		JLabel lblLanamento_1 = new JLabel("Lan\u00E7amento");
		lblLanamento_1.setForeground(Color.WHITE);
		lblLanamento_1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblLanamento_1.setBounds(30, 691, 135, 37);
		contentPane.add(lblLanamento_1);
		
		JLabel lblTrilhaSonora = new JLabel("Trilha Sonora");
		lblTrilhaSonora.setForeground(Color.WHITE);
		lblTrilhaSonora.setBounds(774, 254, 196, 66);
		contentPane.add(lblTrilhaSonora);
		lblTrilhaSonora.setFont(new Font("Tahoma", Font.PLAIN, 20));
		
		JLabel lblAtores = new JLabel("Atores do Episodio");
		lblAtores.setForeground(Color.WHITE);
		lblAtores.setBounds(1290, 254, 227, 66);
		contentPane.add(lblAtores);
		lblAtores.setFont(new Font("Tahoma", Font.PLAIN, 20));
		
		JPanel panel_2 = new JPanel();
		panel_2.setLayout(null);
		panel_2.setBounds(1223, 315, 318, 514);
		panel_2.setBackground(new Color(0,0,0,150));
		contentPane.add(panel_2);
		
		JLabel lblDiretor = new JLabel("T\u00ECtulo:");
		lblDiretor.setForeground(Color.WHITE);
		lblDiretor.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblDiretor.setBounds(30, 620, 135, 37);
		contentPane.add(lblDiretor);
		
		textField_3 = new JTextField();
		textField_3.setEditable(false);
		textField_3.setColumns(10);
		textField_3.setBounds(161, 628, 233, 26);
		contentPane.add(textField_3);
		
		JLabel lblNewLabel = new JLabel("           Nome da Serie  >  Temporada X   >  Episodio  Y");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Tahoma", Font.ITALIC, 25));
		lblNewLabel.setBounds(30, 220, 1246, 37);
		contentPane.add(lblNewLabel);
		
		
		JPanel pnlTransparencia1PaginaDaSerie = new JPanel();
		pnlTransparencia1PaginaDaSerie.setBounds(10, 220, 1564, 633);
		pnlTransparencia1PaginaDaSerie.setBackground(new Color(0, 0, 0, 150));
		
		getContentPane().add(pnlTransparencia1PaginaDaSerie);
		pnlTransparencia1PaginaDaSerie.setLayout(null);
		
		textField = new JTextField();
		textField.setText("AMUSING");
		textField.setFont(new Font("Tahoma", Font.ITALIC, 85));
		textField.setEditable(false);
		textField.setColumns(10);
		textField.setBounds(30, 40, 396, 152);
		contentPane.add(textField);
		
		JButton btnInicioPaginaDoEpisodio = new JButton("In\u00EDcio");
		btnInicioPaginaDoEpisodio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaInicialGUI();
				
				dispose();
				
			}
		});
		btnInicioPaginaDoEpisodio.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnInicioPaginaDoEpisodio.setBounds(798, 51, 119, 41);
		contentPane.add(btnInicioPaginaDoEpisodio);
		
		JButton btnVoltarPaginaDoEpisodio = new JButton("\u2190");
		btnVoltarPaginaDoEpisodio.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnVoltarPaginaDoEpisodio.setBounds(798, 121, 64, 37);
		contentPane.add(btnVoltarPaginaDoEpisodio);
		
		txtBarraDePesquisa = new JTextField();
		txtBarraDePesquisa.setText("Barra de pesquisa");
		txtBarraDePesquisa.setFont(new Font("Tahoma", Font.ITALIC, 25));
		txtBarraDePesquisa.setColumns(10);
		txtBarraDePesquisa.setBounds(1049, 121, 329, 37);
		contentPane.add(txtBarraDePesquisa);
		
		final JPanel pnlConfiguracoes = new JPanel();
		pnlConfiguracoes.setBounds(1415, 47, 119, 200);
		getContentPane().add(pnlConfiguracoes);
		pnlConfiguracoes.setLayout(null);
		pnlConfiguracoes.setVisible(false);
		
		JButton btnMinhasListasPaginaConfiguracoes = new JButton("Minhas listas");
		btnMinhasListasPaginaConfiguracoes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaListasGUI();
				
				dispose();
				
			}
		});
		btnMinhasListasPaginaConfiguracoes.setBounds(0, 114, 119, 30);
		pnlConfiguracoes.add(btnMinhasListasPaginaConfiguracoes);
		
		JButton btnConfiguracoesPaginaSeries = new JButton("Configura\u00E7\u00F5es");
		btnConfiguracoesPaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaConfiguracoesGUI();
				
				dispose();
				
			}
		});
		btnConfiguracoesPaginaSeries.setBounds(0, 142, 119, 30);
		pnlConfiguracoes.add(btnConfiguracoesPaginaSeries);
		
		JButton btnSairPaginaSeries = new JButton("Sair");	
		btnSairPaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaSairGUI();
								
			}
		});
		btnSairPaginaSeries.setBounds(0, 170, 119, 30);
		pnlConfiguracoes.add(btnSairPaginaSeries);
		
		JButton btnFoto = new JButton("foto");
		btnFoto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				pnlConfiguracoes.setVisible(false);
				
			}
		});
		
		btnFoto.setBounds(0, 0, 119, 115);
		pnlConfiguracoes.add(btnFoto);
		btnFoto.setFont(new Font("Tahoma", Font.ITALIC, 20));
		
		JButton btnFotoPaginaSeries = new JButton("foto");
		btnFotoPaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				pnlConfiguracoes.setVisible(true);
				
			}
		});
		btnFotoPaginaSeries.setFont(new Font("Tahoma", Font.ITALIC, 20));
		btnFotoPaginaSeries.setBounds(1415, 47, 119, 115);
		getContentPane().add(btnFotoPaginaSeries);
		
		JButton btnMusicaPaginaDoEpisodio = new JButton("M\u00FAsica");
		btnMusicaPaginaDoEpisodio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaMusicasGUI();
				
				dispose();
				
			}
		});
		btnMusicaPaginaDoEpisodio.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnMusicaPaginaDoEpisodio.setBounds(945, 51, 136, 41);
		contentPane.add(btnMusicaPaginaDoEpisodio);
		
		JButton btnFilmePaginaDoEpisodio = new JButton("Filme");
		btnFilmePaginaDoEpisodio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaFilmesGUI();
				
				dispose();
				
			}
		});
		btnFilmePaginaDoEpisodio.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnFilmePaginaDoEpisodio.setBounds(1108, 51, 119, 41);
		contentPane.add(btnFilmePaginaDoEpisodio);
		
		JButton button_6 = new JButton("S\u00E9rie");
		button_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaSeriesGUI();
				
				dispose();
				
			}
		});
		button_6.setFont(new Font("Tahoma", Font.PLAIN, 30));
		button_6.setBounds(1259, 51, 119, 41);
		contentPane.add(button_6);
		
		JButton btnManualPaginaDoEpisodio = new JButton("");
		btnManualPaginaDoEpisodio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaManualGUI();
								
			}
		});
		btnManualPaginaDoEpisodio.setIcon(new ImageIcon(GPaginaDoEpisodioGUI.class.getResource("/img/q-mark-icon_002070_64.png")));
		btnManualPaginaDoEpisodio.setBounds(871, 121, 46, 37);
		contentPane.add(btnManualPaginaDoEpisodio);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(GPaginaDoEpisodioGUI.class.getResource("/img/serie2.jpg")));
		label.setBounds(0, 0, 1600, 900);
		contentPane.add(label);
	}

}
