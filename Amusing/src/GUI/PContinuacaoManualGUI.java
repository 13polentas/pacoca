package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JScrollBar;
import javax.swing.ImageIcon;

import controller.MudarTelaController;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PContinuacaoManualGUI extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PContinuacaoManualGUI frame = new PContinuacaoManualGUI();
					frame.setUndecorated(true);
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PContinuacaoManualGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 597);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnFechar = new JButton("FECHAR");
		btnFechar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnFechar.setBounds(59, 508, 113, 34);
		contentPane.add(btnFechar);
		
		JLabel label = new JLabel("MANUAL");
		label.setForeground(Color.WHITE);
		label.setFont(new Font("Tahoma", Font.PLAIN, 25));
		label.setBounds(313, 22, 136, 55);
		contentPane.add(label);
		
		JButton button = new JButton(" <-");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				MudarTelaController mudar = new MudarTelaController ();
				
				mudar.AbrirPaginaManualGUI();
				
				dispose();
			}
		});
		button.setBounds(30, 37, 56, 36);
		contentPane.add(button);
		
		JLabel lblParaAdicionarTemporada = new JLabel("Para adicionar temporada, clique em \"adicionar temporada\".");
		lblParaAdicionarTemporada.setForeground(Color.WHITE);
		lblParaAdicionarTemporada.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblParaAdicionarTemporada.setBounds(59, 123, 653, 26);
		contentPane.add(lblParaAdicionarTemporada);
		
		JLabel lblParaConfirmarSeu = new JLabel("Para confirmar seu cadastro, clique em \"confirmar\".");
		lblParaConfirmarSeu.setForeground(Color.WHITE);
		lblParaConfirmarSeu.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblParaConfirmarSeu.setBounds(59, 160, 653, 26);
		contentPane.add(lblParaConfirmarSeu);
		
		JLabel lblParaAdicionarUm = new JLabel("Para adicionar um ator, clique em \"adicionar ator\".");
		lblParaAdicionarUm.setForeground(Color.WHITE);
		lblParaAdicionarUm.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblParaAdicionarUm.setBounds(59, 197, 653, 26);
		contentPane.add(lblParaAdicionarUm);
		
		JLabel lblSeDesejarPesquisar = new JLabel("Se desejar pesquisar algo, digite na barra de pesquisas.");
		lblSeDesejarPesquisar.setForeground(Color.WHITE);
		lblSeDesejarPesquisar.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblSeDesejarPesquisar.setBounds(59, 230, 519, 26);
		contentPane.add(lblSeDesejarPesquisar);
		
		JLabel lblSeQuiserVoltar = new JLabel("Se quiser voltar na tela anterior, clique na seta.");
		lblSeQuiserVoltar.setForeground(Color.WHITE);
		lblSeQuiserVoltar.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblSeQuiserVoltar.setBounds(59, 261, 519, 26);
		contentPane.add(lblSeQuiserVoltar);
		
		JLabel lblParaEntrarNo = new JLabel("Para entrar no manual, clique na interroga\u00E7\u00E3o.");
		lblParaEntrarNo.setForeground(Color.WHITE);
		lblParaEntrarNo.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblParaEntrarNo.setBounds(59, 294, 519, 26);
		contentPane.add(lblParaEntrarNo);
		
		JLabel lblParaConfigurarSeu = new JLabel("Para configurar seu perfil, clique em \"configura\u00E7\u00F5es\".");
		lblParaConfigurarSeu.setForeground(Color.WHITE);
		lblParaConfigurarSeu.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblParaConfigurarSeu.setBounds(59, 324, 499, 26);
		contentPane.add(lblParaConfigurarSeu);
		
		JLabel lblParaEditarUma = new JLabel("Para editar uma lista, clique em \"editar lista\".");
		lblParaEditarUma.setForeground(Color.WHITE);
		lblParaEditarUma.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblParaEditarUma.setBounds(59, 352, 481, 26);
		contentPane.add(lblParaEditarUma);
		
		JLabel lblParaEditarUma_1 = new JLabel("Para editar uma m\u00FAsica, clique em \"editar m\u00FAsica\".");
		lblParaEditarUma_1.setForeground(Color.WHITE);
		lblParaEditarUma_1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblParaEditarUma_1.setBounds(59, 389, 481, 26);
		contentPane.add(lblParaEditarUma_1);
		
		JLabel lblParaEditarUm = new JLabel("Para editar um filme, clique em \"editar filme\".");
		lblParaEditarUm.setForeground(Color.WHITE);
		lblParaEditarUm.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblParaEditarUm.setBounds(59, 419, 454, 26);
		contentPane.add(lblParaEditarUm);
		
		JLabel lblParaFecharO = new JLabel("Para fechar o programa aperte \"alt\" + \"F4\".");
		lblParaFecharO.setForeground(Color.WHITE);
		lblParaFecharO.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblParaFecharO.setBounds(59, 451, 653, 26);
		contentPane.add(lblParaFecharO);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(PContinuacaoManualGUI.class.getResource("/img/botao foto.png")));
		lblNewLabel.setBounds(596, 324, 128, 218);
		contentPane.add(lblNewLabel);
		
		JLabel label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon(PContinuacaoManualGUI.class.getResource("/img/botao.png")));
		label_1.setBounds(588, 264, 147, 49);
		contentPane.add(label_1);
		
		JLabel label_2 = new JLabel("");
		label_2.setIcon(new ImageIcon(PContinuacaoManualGUI.class.getResource("/img/gray.jpg")));
		label_2.setBounds(0, 0, 800, 1000);
		contentPane.add(label_2);
	}
}
