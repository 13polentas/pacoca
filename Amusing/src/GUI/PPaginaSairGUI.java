package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Color;

public class PPaginaSairGUI extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PPaginaSairGUI frame = new PPaginaSairGUI();
					frame.setUndecorated(true);
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PPaginaSairGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 428, 200);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDesejaRealmenteSair = new JLabel("Deseja realmente sair?");
		lblDesejaRealmenteSair.setForeground(Color.WHITE);
		lblDesejaRealmenteSair.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblDesejaRealmenteSair.setBounds(84, 28, 251, 44);
		contentPane.add(lblDesejaRealmenteSair);
		
		JButton btnSimPaginaSair = new JButton("Sim");
		btnSimPaginaSair.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnSimPaginaSair.setBounds(237, 101, 108, 44);
		contentPane.add(btnSimPaginaSair);
		
		JButton btnNaoPaginaSair = new JButton("N\u00E3o");
		btnNaoPaginaSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				dispose();
			}
		});
		btnNaoPaginaSair.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnNaoPaginaSair.setBounds(77, 101, 108, 44);
		contentPane.add(btnNaoPaginaSair);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(PPaginaSairGUI.class.getResource("/img/gray.jpg")));
		label.setBounds(0, 0, 806, 456);
		contentPane.add(label);
	}

}
