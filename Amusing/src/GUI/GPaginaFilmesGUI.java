package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.MudarTelaController;

import javax.swing.JTextField;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTable;
import javax.swing.ImageIcon;

public class GPaginaFilmesGUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField_2;
	private JTable table;
	private JTable table_1;
	private JTable table_2;
	private JTextField textField;
	private JTextField txtBarraDePesquisaPaginaFilmes;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GPaginaFilmesGUI frame = new GPaginaFilmesGUI();
					frame.setUndecorated(true);
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GPaginaFilmesGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1600, 900);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblFilmes = new JLabel("         FILMES");
		lblFilmes.setForeground(Color.WHITE);
		lblFilmes.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblFilmes.setBounds(30, 224, 763, 37);
		contentPane.add(lblFilmes);
		
		textField_2 = new JTextField();
		textField_2.setText("       Barra de pesquisa");
		textField_2.setHorizontalAlignment(SwingConstants.LEFT);
		textField_2.setFont(new Font("Tahoma", Font.ITALIC, 25));
		textField_2.setColumns(10);
		textField_2.setBounds(30, 283, 813, 37);
		contentPane.add(textField_2);
		
		JLabel lblltimosCadastrados = new JLabel("\u00DAltimos Cadastrados");
		lblltimosCadastrados.setForeground(Color.WHITE);
		lblltimosCadastrados.setHorizontalAlignment(SwingConstants.CENTER);
		lblltimosCadastrados.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblltimosCadastrados.setBounds(895, 283, 649, 37);
		contentPane.add(lblltimosCadastrados);
		
		table = new JTable();
		table.setBounds(30, 342, 813, 508);
		table.setBackground(new Color (0,0,0,150));
		contentPane.add(table);
		
		table_1 = new JTable();
		table_1.setBounds(895, 342, 649, 123);
		table_1.setBackground(new Color (0,0,0,150));
		contentPane.add(table_1);
		
		JLabel lblTrilhasSonorasMelhor = new JLabel("Trilhas Sonoras Melhor Avaliadas");
		lblTrilhasSonorasMelhor.setForeground(Color.WHITE);
		lblTrilhasSonorasMelhor.setHorizontalAlignment(SwingConstants.CENTER);
		lblTrilhasSonorasMelhor.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblTrilhasSonorasMelhor.setBounds(895, 476, 649, 37);
		contentPane.add(lblTrilhasSonorasMelhor);
		
		table_2 = new JTable();
		table_2.setBounds(892, 524, 652, 326);
		table_2.setBackground(new Color (0,0,0,150));
		contentPane.add(table_2);
		
		JButton btnCadastrarPaginaFilmes = new JButton("Cadastrar");
		btnCadastrarPaginaFilmes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaCadastroDeFilmeGUI();
								
			}
		});
		btnCadastrarPaginaFilmes.setFont(new Font("Tahoma", Font.PLAIN, 25));
		btnCadastrarPaginaFilmes.setBounds(1346, 224, 192, 37);
		contentPane.add(btnCadastrarPaginaFilmes);
		
		textField = new JTextField();
		textField.setText("AMUSING");
		textField.setFont(new Font("Tahoma", Font.ITALIC, 85));
		textField.setEditable(false);
		textField.setColumns(10);
		textField.setBounds(30, 40, 396, 152);
		contentPane.add(textField);
		
		JButton btnInicioPaginaFilmes = new JButton("In\u00EDcio");
		btnInicioPaginaFilmes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaInicialGUI();
				
				dispose();
				
			}
		});
		btnInicioPaginaFilmes.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnInicioPaginaFilmes.setBounds(798, 51, 119, 41);
		contentPane.add(btnInicioPaginaFilmes);
		
		JButton btnVoltarPaginaFilmes = new JButton("\u2190");
		btnVoltarPaginaFilmes.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnVoltarPaginaFilmes.setBounds(798, 121, 64, 37);
		contentPane.add(btnVoltarPaginaFilmes);
		
		JButton btnManualPaginaFilmes = new JButton("");
		btnManualPaginaFilmes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaManualGUI();
								
			}
		});
		btnManualPaginaFilmes.setIcon(new ImageIcon(GPaginaFilmesGUI.class.getResource("/img/q-mark-icon_002070_64.png")));
		btnManualPaginaFilmes.setBounds(871, 121, 46, 37);
		contentPane.add(btnManualPaginaFilmes);
		
		txtBarraDePesquisaPaginaFilmes = new JTextField();
		txtBarraDePesquisaPaginaFilmes.setText("Barra de pesquisa");
		txtBarraDePesquisaPaginaFilmes.setFont(new Font("Tahoma", Font.ITALIC, 25));
		txtBarraDePesquisaPaginaFilmes.setColumns(10);
		txtBarraDePesquisaPaginaFilmes.setBounds(1049, 121, 329, 37);
		contentPane.add(txtBarraDePesquisaPaginaFilmes);
		
		JButton btnMusicaPaginaFilmes = new JButton("M\u00FAsica");
		btnMusicaPaginaFilmes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaMusicasGUI();
				
				dispose();
				
			}
		});
		btnMusicaPaginaFilmes.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnMusicaPaginaFilmes.setBounds(945, 51, 136, 41);
		contentPane.add(btnMusicaPaginaFilmes);
		
		JButton btnFilmePaginaFilmes = new JButton("Filme");
		btnFilmePaginaFilmes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		btnFilmePaginaFilmes.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnFilmePaginaFilmes.setBounds(1108, 51, 119, 41);
		contentPane.add(btnFilmePaginaFilmes);
		
		JButton btnSeriePaginaFilmes = new JButton("S\u00E9rie");
		btnSeriePaginaFilmes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaSeriesGUI();
				
				dispose();
				
			}
		});
		btnSeriePaginaFilmes.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnSeriePaginaFilmes.setBounds(1259, 51, 119, 41);
		contentPane.add(btnSeriePaginaFilmes);
		
		final JPanel pnlConfiguracoes = new JPanel();
		pnlConfiguracoes.setBounds(1415, 47, 119, 200);
		getContentPane().add(pnlConfiguracoes);
		pnlConfiguracoes.setLayout(null);
		pnlConfiguracoes.setVisible(false);
		
		JButton btnMinhasListasPaginaConfiguracoes = new JButton("Minhas listas");
		btnMinhasListasPaginaConfiguracoes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaListasGUI();
				
				dispose();
				
			}
		});
		btnMinhasListasPaginaConfiguracoes.setBounds(0, 114, 119, 30);
		pnlConfiguracoes.add(btnMinhasListasPaginaConfiguracoes);
		
		JButton btnConfiguracoesPaginaSeries = new JButton("Configura\u00E7\u00F5es");
		btnConfiguracoesPaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaConfiguracoesGUI();
				
				dispose();
				
			}
		});
		btnConfiguracoesPaginaSeries.setBounds(0, 142, 119, 30);
		pnlConfiguracoes.add(btnConfiguracoesPaginaSeries);
		
		JButton btnSairPaginaSeries = new JButton("Sair");	
		btnSairPaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.AbrirPaginaSairGUI();
								
			}
		});
		btnSairPaginaSeries.setBounds(0, 170, 119, 30);
		pnlConfiguracoes.add(btnSairPaginaSeries);
		
		JButton btnFoto = new JButton("foto");
		btnFoto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				pnlConfiguracoes.setVisible(false);
				
			}
		});
		
		btnFoto.setBounds(0, 0, 119, 115);
		pnlConfiguracoes.add(btnFoto);
		btnFoto.setFont(new Font("Tahoma", Font.ITALIC, 20));
		
		JPanel pnlTransparencia1PaginaSeries = new JPanel();
		pnlTransparencia1PaginaSeries.setBounds(10, 220, 1564, 633);
		pnlTransparencia1PaginaSeries.setBackground(new Color(0, 0, 0, 150));
		
		getContentPane().add(pnlTransparencia1PaginaSeries);
		pnlTransparencia1PaginaSeries.setLayout(null);
		
		JButton btnIrParaPgina = new JButton("Ir para P\u00E1gina do Filme (BOT\u00C3O PARA DEMONSTRA\u00C7\u00C3O)");
		btnIrParaPgina.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MudarTelaController mudar = new MudarTelaController();
				
				mudar.IrPaginaDoFilmeGUI();
				
				dispose();
				
			}
		});
		btnIrParaPgina.setBounds(907, 11, 329, 33);
		pnlTransparencia1PaginaSeries.add(btnIrParaPgina);
		
		JButton btnFotoPaginaSeries = new JButton("foto");
		btnFotoPaginaSeries.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				pnlConfiguracoes.setVisible(true);
				
			}
		});
		btnFotoPaginaSeries.setFont(new Font("Tahoma", Font.ITALIC, 20));
		btnFotoPaginaSeries.setBounds(1415, 47, 119, 115);
		getContentPane().add(btnFotoPaginaSeries);
		
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(GPaginaFilmesGUI.class.getResource("/img/filme4.jpg")));
		label.setBounds(0, 0, 1600, 900);
		contentPane.add(label);
	}

}
