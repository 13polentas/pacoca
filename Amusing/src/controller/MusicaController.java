package controller;

import javax.swing.JOptionPane;

import dao.MusicaDAO;
import model.MusicaModel;

public class MusicaController {

	MusicaDAO dao = new MusicaDAO();
	MusicaModel model = new MusicaModel();

	public void Cadastrar(String codMusica, String genero, String titulo, int duracaoMin, int duracaoSeg, String letra, String compositor, String cantor, String lancamento, String linkYouTube,
			int momentoAparicaoMin, int momentoAparicaoSeg){

		if(dao.Cadastrar(model)){
			JOptionPane.showMessageDialog(null,"Cadastrado com sucesso", "",JOptionPane.INFORMATION_MESSAGE);
		}else{
			JOptionPane.showMessageDialog(null,"Erro!", "Problemas ao gravar.",JOptionPane.ERROR_MESSAGE);
		}

	}
	
}