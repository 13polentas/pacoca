package controller;

import java.awt.EventQueue;

import javax.swing.JFrame;

import GUI.GPaginaConfiguracoesGUI;
import GUI.GPaginaDaListaGUI;
import GUI.GPaginaDaMusicaGUI;
import GUI.GPaginaDaSerieGUI;
import GUI.GPaginaDoEpisodioGUI;
import GUI.GPaginaDoFilmeGUI;
import GUI.GPaginaFilmesGUI;
import GUI.GPaginaInicialGUI;
import GUI.GPaginaListasGUI;
import GUI.GPaginaMusicasGUI;
import GUI.GPaginaPesquisasGUI;
import GUI.GPaginaSeriesGUI;
import GUI.PContinuacaoManualGUI;
import GUI.PPaginaAdicionarEpisodioGUI;
import GUI.PPaginaAdicionarListaGUI;
import GUI.PPaginaAdicionarTemporadaGUI;
import GUI.PPaginaCadastroDeFilmeGUI;
import GUI.PPaginaCadastroDeMusicaGUI;
import GUI.PPaginaCadastroDeSeriesGUI;
import GUI.PPaginaDeCadastro;
import GUI.PPaginaEditarEpisodioGUI;
import GUI.PPaginaManualGUI;
import GUI.PPaginaSairGUI;

public class MudarTelaController {


	//ENTRE P�GINAS GRANDES

	public void IrPaginaInicialGUI () {

		EventQueue.invokeLater(new Runnable() {

			public void run() {
				try {
					GPaginaInicialGUI frame = new GPaginaInicialGUI();
					frame.setUndecorated(true);
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	public void IrPaginaMusicasGUI (){

		GPaginaMusicasGUI paginaMusicasGUI = new GPaginaMusicasGUI();

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GPaginaMusicasGUI frame = new GPaginaMusicasGUI();
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					frame.setUndecorated(true);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	public void IrPaginaFilmesGUI (){

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GPaginaFilmesGUI frame = new GPaginaFilmesGUI();
					frame.setUndecorated(true);
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});


	}

	public void IrPaginaSeriesGUI (){

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GPaginaSeriesGUI frame = new GPaginaSeriesGUI();
					frame.setUndecorated(true);
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	public void IrPaginaDaMusicaGUI () {

		EventQueue.invokeLater(new Runnable() {

			public void run() {
				try {
					GPaginaDaMusicaGUI frame = new GPaginaDaMusicaGUI();
					frame.setUndecorated(true);
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	public void IrPaginaDoFilmeGUI () {

		EventQueue.invokeLater(new Runnable() {

			public void run() {
				try {
					GPaginaDoFilmeGUI frame = new GPaginaDoFilmeGUI();
					frame.setUndecorated(true);
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	public void IrPaginaDaSerieGUI () {

		EventQueue.invokeLater(new Runnable() {

			public void run() {
				try {
					GPaginaDaSerieGUI frame = new GPaginaDaSerieGUI();
					frame.setUndecorated(true);
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	public void IrPaginaConfiguracoesGUI () {

		EventQueue.invokeLater(new Runnable() {

			public void run() {
				try {
					GPaginaConfiguracoesGUI frame = new GPaginaConfiguracoesGUI();
					frame.setUndecorated(true);
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	public void IrPaginaDaListaGUI () {

		EventQueue.invokeLater(new Runnable() {

			public void run() {
				try {
					GPaginaDaListaGUI frame = new GPaginaDaListaGUI();
					frame.setUndecorated(true);
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	public void IrPaginaDoEpisodioGUI () {

		EventQueue.invokeLater(new Runnable() {

			public void run() {
				try {
					GPaginaDoEpisodioGUI frame = new GPaginaDoEpisodioGUI();
					frame.setUndecorated(true);
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	public void IrPaginaListasGUI () {

		EventQueue.invokeLater(new Runnable() {

			public void run() {
				try {
					GPaginaListasGUI frame = new GPaginaListasGUI();
					frame.setUndecorated(true);
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	public void IrPaginaPesquisasGUI () {

		EventQueue.invokeLater(new Runnable() {

			public void run() {
				try {
					GPaginaPesquisasGUI frame = new GPaginaPesquisasGUI();
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					frame.setUndecorated(true);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	//ABRIR JANELAS PEQUENAS

	public void AbrirPaginaAdicionarEpisodioGUI () {

		EventQueue.invokeLater(new Runnable() {

			public void run() {
				try {
					PPaginaAdicionarEpisodioGUI frame = new PPaginaAdicionarEpisodioGUI();
					frame.setUndecorated(true);
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	public void AbrirPaginaAdicionarListaGUI () {

		EventQueue.invokeLater(new Runnable() {

			public void run() {
				try {
					PPaginaAdicionarListaGUI frame = new PPaginaAdicionarListaGUI();
					frame.setUndecorated(true);
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	public void AbrirPaginaAdicionarTemporadaGUI () {

		EventQueue.invokeLater(new Runnable() {

			public void run() {
				try {
					PPaginaAdicionarTemporadaGUI frame = new PPaginaAdicionarTemporadaGUI();
					frame.setUndecorated(true);
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void AbrirPaginaCadastroDeFilmeGUI () {

		EventQueue.invokeLater(new Runnable() {

			public void run() {
				try {
					PPaginaCadastroDeFilmeGUI frame = new PPaginaCadastroDeFilmeGUI ();
					frame.setUndecorated(true);
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void AbrirPaginaCadastroDeMusicaGUI () {

		EventQueue.invokeLater(new Runnable() {

			public void run() {
				try {
					PPaginaCadastroDeMusicaGUI frame = new PPaginaCadastroDeMusicaGUI ();
					frame.setUndecorated(true);
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void AbrirPaginaCadastroDeSerieGUI () {

		EventQueue.invokeLater(new Runnable() {

			public void run() {
				try {
					PPaginaCadastroDeSeriesGUI frame = new PPaginaCadastroDeSeriesGUI ();
					frame.setUndecorated(true);
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void AbrirPaginaDeCadastro () {

		EventQueue.invokeLater(new Runnable() {

			public void run() {
				try {
					PPaginaDeCadastro frame = new PPaginaDeCadastro ();
					frame.setUndecorated(true);
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void AbrirPaginaEditarEpisodioGUI () {

		EventQueue.invokeLater(new Runnable() {

			public void run() {
				try {
					PPaginaEditarEpisodioGUI frame = new PPaginaEditarEpisodioGUI ();
					frame.setUndecorated(true);
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void AbrirPaginaManualGUI () {

		EventQueue.invokeLater(new Runnable() {

			public void run() {
				try {
					PPaginaManualGUI frame = new PPaginaManualGUI ();
					frame.setUndecorated(true);
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void AbrirPaginaSairGUI () {

		EventQueue.invokeLater(new Runnable() {

			public void run() {
				try {
					PPaginaSairGUI frame = new PPaginaSairGUI ();
					frame.setUndecorated(true);
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void AbrirPaginaContinuacaoManualGUI () {

		EventQueue.invokeLater(new Runnable() {

			public void run() {
				try {
					PContinuacaoManualGUI frame = new PContinuacaoManualGUI ();
					frame.setUndecorated(true);
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	

}
