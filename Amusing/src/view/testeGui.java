package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class testeGui extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					testeGui frame = new testeGui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public testeGui() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 65, 414, 185);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(135, 51, 172, 106);
		panel.add(panel_1);
		
		JButton btnP = new JButton("p1");
		btnP.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		panel_1.add(btnP);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(153, 51, 156, 99);
		panel.add(panel_2);
		
		JButton btnP_1 = new JButton("p2");
		panel_2.add(btnP_1);
		
		JButton btnMuda = new JButton("muda 1");
		btnMuda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panel_1.hide();
				panel_2.show();
			
			}
		});
		btnMuda.setBounds(152, 33, 89, 23);
		contentPane.add(btnMuda);
		
		JButton btnMuda_1 = new JButton("muda 2");
		btnMuda_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panel_2.hide();
				panel_1.show();
			}
		});
		btnMuda_1.setBounds(268, 33, 89, 23);
		contentPane.add(btnMuda_1);
	}
}
