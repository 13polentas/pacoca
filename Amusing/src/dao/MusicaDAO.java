package dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import controller.FabricaDeConexao;
import model.ModeloMySql;
import model.MusicaModel;


public class MusicaDAO {

	private ModeloMySql modelo = null;

	public MusicaDAO() {
		super();
		modelo = new ModeloMySql();

		modelo.setDatabase("amusing");
		modelo.setServer("localhost");
		modelo.setUser("root");
		modelo.setPassword("ifsuldeminas");
		modelo.setPort("3306");
	}
	
	public boolean Cadastrar(MusicaModel musica){


		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
			return false;
		}


		/* prepara o sql para inserir no banco */

		String sql = "INSERT INTO musica (codMusica, genero, duracaoMin, duracaoSeg, titulo, letra, compositor,"
				+ "cantor, lancamento, linkYouTube, momentoAparicaoMin, momentoAparicaoSeg) VALUES (?,?,?,?,?,?,?,?,?,?,?,?);";

		/* prepara para inserir no banco de dados */

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			/* preenche com os dados */
			statement.setString(1, musica.getCodMusica());
			statement.setString(2, musica.getGenero());
			statement.setInt(3, musica.getDuracaoMin());
			statement.setInt(4, musica.getDuracaoSeg());
			statement.setString(5, musica.getTitulo());
			statement.setString(6, musica.getLetra());
			statement.setString(7, musica.getCompositor());
			statement.setString(8, musica.getCantor());
			statement.setString(9, musica.getLancamento());
			statement.setString(10, musica.getLinkYouTube());
			statement.setInt(11, musica.getMomentoAparicaoMin());
			statement.setInt(12, musica.getMomentoAparicaoSeg());

			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return status;

	}

	public boolean Delete(String codigoMusica){

		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
			return false;
		}


		/* prepara o sql para inserir no banco */

		String sql = "DELETE FROM musica WHERE codigo =?;";


		/* prepara para inserir no banco de dados */

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			/* preenche com os dados */
			statement.setString(1, codigoMusica);



			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return status;

	}


	public ArrayList <MusicaModel> Listar (){

		Connection con = FabricaDeConexao.getConnection(modelo);

		ArrayList <MusicaModel> musicas = new ArrayList <MusicaModel>();


		if (con == null){
			JOptionPane.showMessageDialog (null, "Erro de conex�o ao banco!");
			return null;
		}

		//prepara o sql para selecionar no banco

		String sql = "SELECT * FROM musica;";

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			ResultSet result = statement.executeQuery();

			while (result.next()){
				MusicaModel musica = new MusicaModel();

				musica.setCodMusica(result.getString("C�digo"));
				musica.setGenero(result.getString("G�nero"));
				musica.setDuracaoMin(result.getInt("DuracaoMin"));
				musica.setDuracaoSeg(result.getInt("DuracaoSeg"));
				musica.setTitulo(result.getString("Titulo"));
				musica.setLetra(result.getString("Letra"));
				musica.setCompositor(result.getString("Compositor"));
				musica.setCantor(result.getString("Cantor"));
				musica.setLancamento(result.getString("Lancamento"));
				musica.setLinkYouTube(result.getString("LinkYouTube"));
				musica.setMomentoAparicaoMin(result.getInt("MomentoAparicaoMin"));
				musica.setMomentoAparicaoSeg(result.getInt("MomentoAparicaoSeg"));
				

				musicas.add(musica);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}
		return musicas;


	}

	public MusicaModel Search(String codigoMusica){

		Connection con = FabricaDeConexao.getConnection(modelo);
		MusicaModel musica = new MusicaModel();


		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
			return null;
		}


		/* prepara o sql para inserir no banco */

		String sql = "SELECT * FROM musica WHERE codigo =?;";

		/* prepara para inserir no banco de dados */

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			/* preenche com os dados */
			statement.setString(1, codigoMusica);

			ResultSet result = statement.executeQuery();

			result.next();
			
			musica.setCodMusica(result.getString("C�digo"));
			musica.setGenero(result.getString("G�nero"));
			musica.setDuracaoMin(result.getInt("DuracaoMin"));
			musica.setDuracaoSeg(result.getInt("DuracaoSeg"));
			musica.setTitulo(result.getString("Titulo"));
			musica.setLetra(result.getString("Letra"));
			musica.setCompositor(result.getString("Compositor"));
			musica.setCantor(result.getString("Cantor"));
			musica.setLancamento(result.getString("Lancamento"));
			musica.setLinkYouTube(result.getString("LinkYouTube"));
			musica.setMomentoAparicaoMin(result.getInt("MomentoAparicaoMin"));
			musica.setMomentoAparicaoSeg(result.getInt("MomentoAparicaoSeg"));
			


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return musica;


	}


	public boolean Update(String genero, String duracaoMin, String duracaoSeg, String titulo, String letra,
			String compositor, String cantor, String lancamento, String linkYouTube, int momentoAparicaoMin,
			int momentoAparicaoSeg, String codMusica){

		
		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);


		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
			return false;
		}
		
		String sql = "UPDATE musica SET genero=?, duracaoMin=?, duracaoSeg=?, titulo=?, letra=?, compositor=?, "
				+ "cantor=?, lancamento=?, linkYouTube=?, momentoAparicaoMin=?, momentoAparicaoSeg=? WHERE codigo =?;";
		
		
		try {
			PreparedStatement statement = con.prepareStatement(sql);

			/* preenche com os dados */
			statement.setString(1, genero);
			statement.setInt(2, Integer.parseInt(duracaoMin));
			statement.setInt(3, Integer.parseInt(duracaoSeg));
			statement.setString(4, titulo);
			statement.setString(5, letra);
			statement.setString(6, compositor);
			statement.setString(7, cantor);
			statement.setString(8, lancamento);
			statement.setString(9, linkYouTube);
			statement.setInt(10, momentoAparicaoMin);
			statement.setInt(11, momentoAparicaoSeg);
			statement.setString(12, codMusica);
			

			System.out.println(sql);

			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return status;


	}

}