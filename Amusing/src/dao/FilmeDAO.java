package dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import controller.FabricaDeConexao;
import model.ModeloMySql;
import model.FilmeModel;


public class FilmeDAO {

	private ModeloMySql modelo = null;

	public FilmeDAO() {
		super();
		modelo = new ModeloMySql();

		modelo.setDatabase("amusing");
		modelo.setServer("localhost");
		modelo.setUser("root");
		modelo.setPassword("ifsuldeminas");
		modelo.setPort("3306");
	}
	
	public boolean Cadastrar(FilmeModel filme){


		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
			return false;
		}


		/* prepara o sql para inserir no banco */

		String sql = "INSERT INTO filme (codFilme, titulo, genero, lancamento, atores, diretor, duracaoMin,"
				+ "duracaoSeg, roteirista, listaSoundTrack, estudio) VALUES (?,?,?,?,?,?,?,?,?,?,?);";

		/* prepara para inserir no banco de dados */

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			/* preenche com os dados */
			statement.setString(1, filme.getCodFilme());
			statement.setString(2, filme.getTitulo());
			statement.setString(3, filme.getGenero());
			statement.setString(4, filme.getLancamento());
			statement.setString(5, filme.getAtores());
			statement.setString(6, filme.getDiretor());
			statement.setInt(7, filme.getDuracaoMin());
			statement.setInt(8, filme.getDuracaoSeg());
			statement.setString(9, filme.getRoteirista());
			statement.setString(10, filme.getListaSoundTrack());
			statement.setString(11, filme.getEstudio());


			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return status;

	}

	public boolean Delete(String codigoFilme){

		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
			return false;
		}


		/* prepara o sql para inserir no banco */

		String sql = "DELETE FROM filme WHERE codigo =?;";


		/* prepara para inserir no banco de dados */

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			/* preenche com os dados */
			statement.setString(1, codigoFilme);



			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return status;

	}


	public ArrayList <FilmeModel> Listar (){

		Connection con = FabricaDeConexao.getConnection(modelo);

		ArrayList <FilmeModel> filmes = new ArrayList <FilmeModel>();


		if (con == null){
			JOptionPane.showMessageDialog (null, "Erro de conex�o ao banco!");
			return null;
		}

		//prepara o sql para selecionar no banco

		String sql = "SELECT * FROM filme;";

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			ResultSet result = statement.executeQuery();

			while (result.next()){
				FilmeModel filme = new FilmeModel();

				filme.setCodFilme(result.getString("C�digo"));
				filme.setTitulo(result.getString("Titulo"));
				filme.setGenero(result.getString("Genero"));
				filme.setLancamento(result.getString("Lancamento"));
				filme.setAtores(result.getString("Atores"));
				filme.setDiretor(result.getString("Diretor"));
				filme.setDuracaoMin(result.getInt("DuracaoMin"));
				filme.setDuracaoSeg(result.getInt("DuracaoSeg"));
				filme.setRoteirista(result.getString("Roteirista"));
				filme.setListaSoundTrack(result.getString("ListaSoundTrack"));
				filme.setEstudio(result.getString("Estudio"));

				filmes.add(filme);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}
		return filmes;


	}

	public FilmeModel Search(String codigoFilme){

		Connection con = FabricaDeConexao.getConnection(modelo);
		FilmeModel filme = new FilmeModel();


		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
			return null;
		}


		/* prepara o sql para inserir no banco */

		String sql = "SELECT * FROM filme WHERE codigo =?;";

		/* prepara para inserir no banco de dados */

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			/* preenche com os dados */
			statement.setString(1, codigoFilme);

			ResultSet result = statement.executeQuery();

			result.next();
			
			filme.setCodFilme(result.getString("C�digo"));
			filme.setTitulo(result.getString("Titulo"));
			filme.setGenero(result.getString("Genero"));
			filme.setLancamento(result.getString("Lancamento"));
			filme.setAtores(result.getString("Atores"));
			filme.setDiretor(result.getString("Diretor"));
			filme.setDuracaoMin(result.getInt("DuracaoMin"));
			filme.setDuracaoSeg(result.getInt("DuracaoSeg"));
			filme.setRoteirista(result.getString("Roteirista"));
			filme.setListaSoundTrack(result.getString("ListaSoundTrack"));
			filme.setEstudio(result.getString("Estudio"));


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return filme;


	}


	public boolean Update(String titulo, String genero, String lancamento, String atores, String diretor,
			int duracaoMin, int duracaoSeg, String roteirista, String listaSoundTrack, String estudio, String codFilme){

		
		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);


		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
			return false;
		}
		
		String sql = "UPDATE filme SET titulo=?, genero=?, lancamento=?, atores=?, diretor=?, duracaoMin=?,"
				+ "duracaoSeg=?, roteirista=?, listaSoundTrack=?, estudio=? WHERE codFilme =?;";
		
		
		try {
			PreparedStatement statement = con.prepareStatement(sql);

			/* preenche com os dados */
			statement.setString(1, titulo);
			statement.setString(2, genero);
			statement.setString(3, lancamento);
			statement.setString(4, atores);
			statement.setString(5, diretor);
			statement.setInt(6, duracaoMin);
			statement.setInt(7, duracaoSeg);
			statement.setString(8, roteirista);
			statement.setString(9, listaSoundTrack);
			statement.setString(10, estudio);
			statement.setString(11, codFilme);

			System.out.println(sql);

			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return status;


	}
	
	//teste
	//outro comentario de teste

}