package dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import controller.FabricaDeConexao;
import model.ModeloMySql;
import model.SerieModel;


public class SerieDAO {

	private ModeloMySql modelo = null;

	public SerieDAO() {
		super();
		modelo = new ModeloMySql();

		modelo.setDatabase("amusing");
		modelo.setServer("localhost");
		modelo.setUser("root");
		modelo.setPassword("ifsuldeminas");
		modelo.setPort("3306");
	}
	
	public boolean Cadastrar(SerieModel serie){


		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
			return false;
		}


		/* prepara o sql para inserir no banco */

		String sql = "INSERT INTO serie (codSerie, titulo, genero, atores, diretor, numTemporadas, roteirista) "
				+ "VALUES (?,?,?,?,?,?,?);";

		/* prepara para inserir no banco de dados */

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			/* preenche com os dados */
			statement.setString(1, serie.getCodSerie());
			statement.setString(2, serie.getTitulo());
			statement.setString(3, serie.getGenero());
			statement.setString(4, serie.getAtores());
			statement.setString(5, serie.getDiretor());
			statement.setInt(6, serie.getNumTemporadas());
			statement.setString(7, serie.getRoteirista());

			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return status;

	}

	public boolean Delete(String codigoSerie){

		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
			return false;
		}


		/* prepara o sql para inserir no banco */

		String sql = "DELETE FROM serie WHERE codigo =?;";


		/* prepara para inserir no banco de dados */

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			/* preenche com os dados */
			statement.setString(1, codigoSerie);



			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return status;

	}


	public ArrayList <SerieModel> Listar (){

		Connection con = FabricaDeConexao.getConnection(modelo);

		ArrayList <SerieModel> series = new ArrayList <SerieModel>();


		if (con == null){
			JOptionPane.showMessageDialog (null, "Erro de conex�o ao banco!");
			return null;
		}

		//prepara o sql para selecionar no banco

		String sql = "SELECT * FROM serie;";

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			ResultSet result = statement.executeQuery();

			while (result.next()){
				SerieModel serie = new SerieModel();

				serie.setCodSerie(result.getString("C�digo"));
				serie.setTitulo(result.getString("T�tulo"));
				serie.setGenero(result.getInt("G�nero"));
				serie.setAtores(result.getInt("Atores"));
				serie.setDiretor(result.getString("Diretor"));
				serie.setNumTemporadas(result.getInt("NumTemporadas"));
				serie.setRoteirista(result.getString("Roteirista"));
				serie.setCantor(result.getString("Cantor"));
				serie.setLancamento(result.getString("Lancamento"));
				serie.setLinkYouTube(result.getString("LinkYouTube"));
				serie.setMomentoAparicaoMin(result.getInt("MomentoAparicaoMin"));
				serie.setMomentoAparicaoSeg(result.getInt("MomentoAparicaoSeg"));
				

				series.add(serie);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}
		return series;


	}

	public SerieModel Search(String codigoSerie){

		Connection con = FabricaDeConexao.getConnection(modelo);
		SerieModel serie = new SerieModel();

		

		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
			return null;
		}


		/* prepara o sql para inserir no banco */

		String sql = "SELECT * FROM serie WHERE codigo =?;";

		/* prepara para inserir no banco de dados */

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			/* preenche com os dados */
			statement.setString(1, codigoSerie);

			ResultSet result = statement.executeQuery();

			result.next();
			
			serie.setCodMusica(result.getString("C�digo"));
			serie.setGenero(result.getString("G�nero"));
			serie.setDuracaoMin(result.getInt("DuracaoMin"));
			serie.setDuracaoSeg(result.getInt("DuracaoSeg"));
			serie.setTitulo(result.getString("Titulo"));
			serie.setLetra(result.getString("Letra"));
			serie.setCompositor(result.getString("Compositor"));
			serie.setCantor(result.getString("Cantor"));
			serie.setLancamento(result.getString("Lancamento"));
			serie.setLinkYouTube(result.getString("LinkYouTube"));
			serie.setMomentoAparicaoMin(result.getInt("MomentoAparicaoMin"));
			serie.setMomentoAparicaoSeg(result.getInt("MomentoAparicaoSeg"));
			


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return serie;


	}


	public boolean Update(String genero, String duracaoMin, String duracaoSeg, String titulo, String letra,
			String compositor, String cantor, String lancamento, String linkYouTube, int momentoAparicaoMin,
			int momentoAparicaoSeg, String codMusica){

		
		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);


		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
			return false;
		}
		
		String sql = "UPDATE serie SET genero=?, duracaoMin=?, duracaoSeg=?, titulo=?, letra=?, compositor=?, "
				+ "cantor=?, lancamento=?, linkYouTube=?, momentoAparicaoMin=?, momentoAparicaoSeg=? WHERE codigo =?;";
		
		
		try {
			PreparedStatement statement = con.prepareStatement(sql);

			/* preenche com os dados */
			statement.setString(1, genero);
			statement.setInt(2, Integer.parseInt(duracaoMin));
			statement.setInt(3, Integer.parseInt(duracaoSeg));
			statement.setString(4, titulo);
			statement.setString(5, letra);
			statement.setString(6, compositor);
			statement.setString(7, cantor);
			statement.setString(8, lancamento);
			statement.setString(9, linkYouTube);
			statement.setInt(10, momentoAparicaoMin);
			statement.setInt(11, momentoAparicaoSeg);
			statement.setString(12, codMusica);
			

			System.out.println(sql);

			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return status;


	}

}