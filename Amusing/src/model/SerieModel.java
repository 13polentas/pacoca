package model;

public class SerieModel {
	
	private String codSerie;
	private String titulo;
	private String genero;
	private String atores;
	private String diretor;
	private int numTemporadas;
	private String roteirista;
	
	
	
	public String getCodSerie() {
		return codSerie;
	}
	public void setCodSerie(String codSerie) {
		this.codSerie = codSerie;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	
	
	public String getAtores() {
		return atores;
	}
	public void setAtores(String atores) {
		this.atores = atores;
	}
	public String getDiretor() {
		return diretor;
	}
	public void setDiretor(String diretor) {
		this.diretor = diretor;
	}
	public int getNumTemporadas() {
		return numTemporadas;
	}
	public void setNumTemporadas(int numTemporadas) {
		this.numTemporadas = numTemporadas;
	}
	
	
	public String getRoteirista() {
		return roteirista;
	}
	public void setRoteirista(String roteirista) {
		this.roteirista = roteirista;
	}
	
	
	
	public SerieModel() {
		super();
	}
	public SerieModel(String codSerie, String titulo, String genero,
			String lancamento, String atores, String diretor,
			int numTemporadas, String listaSoundTrack, String episodios,
			int numEpisodios, String roteirista, String estudio) {
		super();
		this.codSerie = codSerie;
		this.titulo = titulo;
		this.genero = genero;
		this.atores = atores;
		this.diretor = diretor;
		this.numTemporadas = numTemporadas;
		this.roteirista = roteirista;
		
	}
	
	
	
	
	
}
