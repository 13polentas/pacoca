package model;

public class PessoaModel {
	
	private String codPessoa;
	private String username;
	private String emai;
	private String senha;
	private String listaFavoritos;
	private String listaSeries;
	private String listaFilmes;
	private String nome;
	private String foto;
	private boolean permissaoAdm;
	
	
	public String getCodPessoa() {
		return codPessoa;
	}
	public void setCodPessoa(String codPessoa) {
		this.codPessoa = codPessoa;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmai() {
		return emai;
	}
	public void setEmai(String emai) {
		this.emai = emai;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getListaFavoritos() {
		return listaFavoritos;
	}
	public void setListaFavoritos(String listaFavoritos) {
		this.listaFavoritos = listaFavoritos;
	}
	public String getListaSeries() {
		return listaSeries;
	}
	public void setListaSeries(String listaSeries) {
		this.listaSeries = listaSeries;
	}
	public String getListaFilmes() {
		return listaFilmes;
	}
	public void setListaFilmes(String listaFilmes) {
		this.listaFilmes = listaFilmes;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public boolean isPermissaoAdm() {
		return permissaoAdm;
	}
	public void setPermissaoAdm(boolean permissaoAdm) {
		this.permissaoAdm = permissaoAdm;
	}
	
	
	public PessoaModel() {
		super();
	}
	public PessoaModel(String codPessoa, String username, String emai,
			String senha, String listaFavoritos, String listaSeries,
			String listaFilmes, String nome, String foto, boolean permissaoAdm) {
		super();
		this.codPessoa = codPessoa;
		this.username = username;
		this.emai = emai;
		this.senha = senha;
		this.listaFavoritos = listaFavoritos;
		this.listaSeries = listaSeries;
		this.listaFilmes = listaFilmes;
		this.nome = nome;
		this.foto = foto;
		this.permissaoAdm = permissaoAdm;
	}
	
	
	
	
	 
	 
	

}
