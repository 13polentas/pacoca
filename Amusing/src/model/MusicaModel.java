package model;

public class MusicaModel {
	private String codMusica;
	private String genero;
	private String titulo;
	private int duracaoMin;
	private int duracaoSeg;
	private String letra;
	private String compositor;
	private String cantor;
	private String lancamento;
	private String linkYouTube;
	private int momentoAparicaoMin;
	private int momentoAparicaoSeg;
	
	
	public MusicaModel() {
		super();
	}


	public MusicaModel(String codMusica, String genero, String titulo,
			int duracaoMin, int duracaoSeg, String letra, String compositor,
			String cantor, String lancamento, String linkYouTube,
			int momentoAparicaoMin, int momentoAparicaoSeg) {
		super();
		this.codMusica = codMusica;
		this.genero = genero;
		this.titulo = titulo;
		this.duracaoMin = duracaoMin;
		this.duracaoSeg = duracaoSeg;
		this.letra = letra;
		this.compositor = compositor;
		this.cantor = cantor;
		this.lancamento = lancamento;
		this.linkYouTube = linkYouTube;
		this.momentoAparicaoMin = momentoAparicaoMin;
		this.momentoAparicaoSeg = momentoAparicaoSeg;
	
	}


	public String getCodMusica() {
		return codMusica;
	}


	public void setCodMusica(String codMusica) {
		this.codMusica = codMusica;
	}


	public String getGenero() {
		return genero;
	}


	public void setGenero(String genero) {
		this.genero = genero;
	}


	public String getTitulo() {
		return titulo;
	}


	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}


	public int getDuracaoMin() {
		return duracaoMin;
	}


	public void setDuracaoMin(int duracaoMin) {
		this.duracaoMin = duracaoMin;
	}


	public int getDuracaoSeg() {
		return duracaoSeg;
	}


	public void setDuracaoSeg(int duracaoSeg) {
		this.duracaoSeg = duracaoSeg;
	}


	public String getLetra() {
		return letra;
	}


	public void setLetra(String letra) {
		this.letra = letra;
	}


	public String getCompositor() {
		return compositor;
	}


	public void setCompositor(String compositor) {
		this.compositor = compositor;
	}


	public String getCantor() {
		return cantor;
	}


	public void setCantor(String cantor) {
		this.cantor = cantor;
	}


	public String getLancamento() {
		return lancamento;
	}


	public void setLancamento(String lancamento) {
		this.lancamento = lancamento;
	}


	public String getLinkYouTube() {
		return linkYouTube;
	}


	public void setLinkYouTube(String linkYouTube) {
		this.linkYouTube = linkYouTube;
	}


	public int getMomentoAparicaoMin() {
		return momentoAparicaoMin;
	}


	public void setMomentoAparicaoMin(int momentoAparicaoMin) {
		this.momentoAparicaoMin = momentoAparicaoMin;
	}


	public int getMomentoAparicaoSeg() {
		return momentoAparicaoSeg;
	}


	public void setMomentoAparicaoSeg(int momentoAparicaoSeg) {
		this.momentoAparicaoSeg = momentoAparicaoSeg;
	}


}