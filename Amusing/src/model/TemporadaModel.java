package model;

public class TemporadaModel {
	private String codTemporada;
	private int numEpisodio;
	private int numTemporadas;
	private String lancamento;
	
	public TemporadaModel() {
		super();
	}

	public TemporadaModel(String codTemporada, int numEpisodio,
			int numTemporadas, String lancamento) {
		super();
		this.codTemporada = codTemporada;
		this.numEpisodio = numEpisodio;
		this.numTemporadas = numTemporadas;
		this.lancamento = lancamento;
	}

	public String getCodTemporada() {
		return codTemporada;
	}

	public void setCodTemporada(String codTemporada) {
		this.codTemporada = codTemporada;
	}

	public int getNumEpisodio() {
		return numEpisodio;
	}

	public void setNumEpisodio(int numEpisodio) {
		this.numEpisodio = numEpisodio;
	}

	public int getNumTemporadas() {
		return numTemporadas;
	}

	public void setNumTemporadas(int numTemporadas) {
		this.numTemporadas = numTemporadas;
	}

	public String getLancamento() {
		return lancamento;
	}

	public void setLancamento(String lancamento) {
		this.lancamento = lancamento;
	}
	
	

}
