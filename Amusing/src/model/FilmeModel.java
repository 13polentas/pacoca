package model;

import java.util.ArrayList;

public class FilmeModel {
	
 private String titulo;
 private String genero;
 private String lancamento;
 private String atores;
 private String diretor;
 private int duracaoMin;
 private String roteirista;
 private String listaSoundTrack;
 private String estudio;
 private String codFilme;
 private int duracaoSeg;
 private String momentoAparicao;
 private ArrayList<MusicaModel> trilhaSonora;

 public FilmeModel() {
	super();
	this.trilhaSonora = new ArrayList<MusicaModel>();
}

public FilmeModel(String titulo, String genero, String lancamento,
		String atores, String diretor, int duracaoMin, String roteirista,
		String listaSoundTrack, String estudio, String codFilme, int duracaoSeg, String momentoAparicao) {
	super();
	this.titulo = titulo;
	this.genero = genero;
	this.lancamento = lancamento;
	this.atores = atores;
	this.diretor = diretor;
	this.duracaoMin = duracaoMin;
	this.roteirista = roteirista;
	this.listaSoundTrack = listaSoundTrack;
	this.estudio = estudio;
	this.codFilme = codFilme;
	this.duracaoSeg = duracaoSeg;
	this.trilhaSonora = new ArrayList<MusicaModel>();
	this.momentoAparicao = momentoAparicao;
}

public String getTitulo() {
	return titulo;
}

public String getMomentoAparicao() {
	return momentoAparicao;
}

public void setMomentoAparicao(String momentoAparicao) {
	this.momentoAparicao = momentoAparicao;
}

public ArrayList<MusicaModel> getTrilhaSonora() {
	return trilhaSonora;
}

public void setTrilhaSonora(ArrayList<MusicaModel> trilhaSonora) {
	this.trilhaSonora = trilhaSonora;
}

public void setTitulo(String titulo) {
	this.titulo = titulo;
}

public String getGenero() {
	return genero;
}

public void setGenero(String genero) {
	this.genero = genero;
}

public String getLancamento() {
	return lancamento;
}

public void setLancamento(String lancamento) {
	this.lancamento = lancamento;
}

public String getAtores() {
	return atores;
}

public void setAtores(String atores) {
	this.atores = atores;
}

public String getDiretor() {
	return diretor;
}

public void setDiretor(String diretor) {
	this.diretor = diretor;
}

public int getDuracaoMin() {
	return duracaoMin;
}

public void setDuracaoMin(int duracaoMin) {
	this.duracaoMin = duracaoMin;
}

public String getRoteirista() {
	return roteirista;
}

public void setRoteirista(String roteirista) {
	this.roteirista = roteirista;
}

public String getListaSoundTrack() {
	return listaSoundTrack;
}

public void setListaSoundTrack(String listaSoundTrack) {
	this.listaSoundTrack = listaSoundTrack;
}

public String getEstudio() {
	return estudio;
}

public void setEstudio(String estudio) {
	this.estudio = estudio;
}

public String getCodFilme() {
	return codFilme;
}

public void setCodFilme(String codFilme) {
	this.codFilme = codFilme;
}

public int getDuracaoSeg() {
	return duracaoSeg;
}

public void setDuracaoSeg(int duracaoSeg) {
	this.duracaoSeg = duracaoSeg;
}
 
 
}
