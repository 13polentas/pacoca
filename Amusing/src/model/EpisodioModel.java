package model;

public class EpisodioModel {
 
	private int numEpisodio;
	private String titulo;
	private int duracaoSeg;
	private String lancamento;
	private String listaSoundTrack;
	private int duracaoMin;
	private String codEpsodio;

	public EpisodioModel() {
		super();
	}

	public EpisodioModel(int numEpisodio, String titulo, int duracaoSeg,
			String lancamento, String listaSoundTrack, int duracaoMin,
			String codEpsodio) {
		super();
		this.numEpisodio = numEpisodio;
		this.titulo = titulo;
		this.duracaoSeg = duracaoSeg;
		this.lancamento = lancamento;
		this.listaSoundTrack = listaSoundTrack;
		this.duracaoMin = duracaoMin;
		this.codEpsodio = codEpsodio;
	}

	public int getNumEpisodio() {
		return numEpisodio;
	}

	public void setNumEpisodio(int numEpisodio) {
		this.numEpisodio = numEpisodio;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getDuracaoSeg() {
		return duracaoSeg;
	}

	public void setDuracaoSeg(int duracaoSeg) {
		this.duracaoSeg = duracaoSeg;
	}

	public String getLancamento() {
		return lancamento;
	}

	public void setLancamento(String lancamento) {
		this.lancamento = lancamento;
	}

	public String getListaSoundTrack() {
		return listaSoundTrack;
	}

	public void setListaSoundTrack(String listaSoundTrack) {
		this.listaSoundTrack = listaSoundTrack;
	}

	public int getDuracaoMin() {
		return duracaoMin;
	}

	public void setDuracaoMin(int duracaoMin) {
		this.duracaoMin = duracaoMin;
	}

	public String getCodEpsodio() {
		return codEpsodio;
	}

	public void setCodEpsodio(String codEpsodio) {
		this.codEpsodio = codEpsodio;
	}
	
	
}
